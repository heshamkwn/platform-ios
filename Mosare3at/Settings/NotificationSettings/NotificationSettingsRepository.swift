//
//  NotificationSettingsRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol NotificationSettingsPresenterDelegate {
    func operationFailed(message: String)
    func getNotificationSettingsSuccess(notificationSettings: NotificationSetting)
}


public class NotificationSettingsRepository {
    var delegate: NotificationSettingsPresenterDelegate!
    
    public func setDelegate(delegate: NotificationSettingsPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getNotificationSettings() {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "notification-types")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let statusJson = json["status"] as! Dictionary<String,AnyObject>
                        let notificationSetting = NotificationSetting.getInstance(dictionary: statusJson)
                        self.delegate.getNotificationSettingsSuccess(notificationSettings: notificationSetting)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}

