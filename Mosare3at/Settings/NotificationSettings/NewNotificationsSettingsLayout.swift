//
//  NewNotificationsSettingsLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Material

public protocol NewNotificationsSettingsLayoutDelegate: BaseLayoutDelegate {
    func goBack()
}

public class NewNotificationsSettingsLayout: BaseLayout {

    var notificationSettingsDelegate: NewNotificationsSettingsLayoutDelegate!
    var topView: TopView = TopView()
    var screenTitle = "notificationsSettings".localized()
    
    init(superview: UIView, delegate: NewNotificationsSettingsLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.notificationSettingsDelegate = delegate
    }
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var settingsTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = true
        tableView.register(NotificationsSettingsCell.self, forCellReuseIdentifier: NotificationsSettingsCell.identifier)
        return tableView
    }()
    
    lazy var noSettingsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "noSettings".localized()
        label.backgroundColor = .white
        label.textAlignment = .center
        label.isHidden = true
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    public func setupViews() {
        self.superview.addSubviews([topView, containerView, settingsTableView])
        
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.containerView.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.bottom.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) * -1)
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
        }
        
        containerView.addSubviews([settingsTableView, noSettingsLabel])
        
        self.settingsTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.bottom.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        self.noSettingsLabel.snp.makeConstraints { (maker) in
            maker.center.equalTo(containerView)
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "notificationsSettings".localized())
        self.topView.backImageView.image = UIImage(named: "close")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.delegate = self
    }
}

extension NewNotificationsSettingsLayout: TopViewDelegate {
    public func goBack() {
        self.notificationSettingsDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}

