//
//  NewNotificationsSettingsVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit

class NewNotificationsSettingsVC: BaseVC {

    var layout: NewNotificationsSettingsLayout!
    var presenter: NotifiationSettingsPresenter!
    var notificationsSettings: NotificationSetting!
    var notificationsSettingsItems: [NotificationSettingsItem]!
    
    public class func buildVC() -> NewNotificationsSettingsVC {
        return NewNotificationsSettingsVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = NewNotificationsSettingsLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        presenter = Injector.provideNotificationsSettingsPresenter()
        presenter.setView(view: self)
        presenter.getNotificationSettings()
        
    }
}

extension NewNotificationsSettingsVC: NewNotificationsSettingsLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        presenter.getNotificationSettings()
    }
}

extension NewNotificationsSettingsVC: NotificationSettingsView {
    func operationFailed(message: String) {
        self.view.makeToast(message, duration: 2, position: .center)
        for view in self.view.subviews {
            if !(view is TopView) {
                view.isHidden = true
            }
        }
        self.layout.showErrorViews()
    }
    
    func getNotificationSettingsSuccess(notificationSettings: NotificationSetting) {
        
        self.notificationsSettings = notificationSettings
        
        notificationsSettingsItems = NotificationSettingsItem.convertNotificationSettingsToNotificationSettingsItems(settings: self.notificationsSettings)
        
        for view in self.view.subviews {
            view.isHidden = false
        }
        self.layout.hideErrorViews()
        
        if notificationsSettingsItems.count > 0 {
            self.layout.noSettingsLabel.isHidden = true
            self.layout.settingsTableView.isHidden = false
            
            self.layout.settingsTableView.dataSource = self
            self.layout.settingsTableView.delegate = self
            self.layout.settingsTableView.reloadData()
        } else {
            self.layout.noSettingsLabel.isHidden = false
            self.layout.settingsTableView.isHidden = true
        }
    }
}

extension NewNotificationsSettingsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsSettingsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NotificationsSettingsCell.identifier, for: indexPath) as! NotificationsSettingsCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.index = indexPath.row
        cell.notificationSettingsItem = self.notificationsSettingsItems.get(at: indexPath.row)
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6)
    }
}

extension NewNotificationsSettingsVC: NotificationsSettingsCellDelegate {
    func notificationStatusChanged(index: Int, status: Bool) {
        
    }
}
