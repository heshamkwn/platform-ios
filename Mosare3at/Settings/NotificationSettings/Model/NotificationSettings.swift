//
//  NotificationSettings.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class NotificationSetting {
    
    public var newCertificate: Bool!
    public var confirmPayment: Bool!
    public var lateConfirm: Bool!
    public var welcomeMail: Bool!
    public var weekSummary: Bool!
    public var interested: Bool!
    public var confirmBooking: Bool!
    public var newPoints: Bool!
    public var congratulation: Bool!
    public var reminder: Bool!
    public var newPassword: Bool!
    public var nickname: Bool!
    public var newBadge: Bool!
    public var deliverableAccepted: Bool!
    public var feedback: Bool!
    public var verifyAccount: Bool!
    public var certificateGeneration: Bool!
    public var deliverableRejected: Bool!
    public var programReminder: Bool!
    public var undoBooking: Bool!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["new_certificate"] = newCertificate
        dictionary["confirm_payment"] = confirmPayment
        dictionary["late_confirm"] = lateConfirm
        dictionary["welcome_mail"] = welcomeMail
        dictionary["week_summary"] = weekSummary
        dictionary["interested"] = interested
        dictionary["confirm_booking"] = confirmBooking
        dictionary["new_points"] = newPoints
        dictionary["congratulation"] = congratulation
        dictionary["Reminder"] = reminder
        dictionary["new_password"] = newPassword
        dictionary["nickname"] = nickname
        dictionary["new_badge"] = newBadge
        dictionary["deliverable_accepted"] = deliverableAccepted
        dictionary["feedback"] = feedback
        dictionary["verify_account"] = verifyAccount
        dictionary["certificate_generation"] = certificateGeneration
        dictionary["deliverable_rejected"] = deliverableRejected
        dictionary["program_reminder"] = programReminder
        dictionary["undo_booking"] = undoBooking
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> NotificationSetting {
        let notificationSetting = NotificationSetting()
        if dictionary.has("new_certificate") {
            notificationSetting.newCertificate = dictionary["new_certificate"] as? Bool
        }
        
        if dictionary.has("confirm_payment") {
            notificationSetting.confirmPayment = dictionary["confirm_payment"] as? Bool
        }
        
        if dictionary.has("late_confirm") {
            notificationSetting.lateConfirm = dictionary["late_confirm"] as? Bool
        }
        
        if dictionary.has("welcome_mail") {
            notificationSetting.welcomeMail = dictionary["welcome_mail"] as? Bool
        }
        
        if dictionary.has("week_summary") {
            notificationSetting.weekSummary = dictionary["week_summary"] as? Bool
        }
        
        if dictionary.has("interested") {
            notificationSetting.interested = dictionary["interested"] as? Bool
        }
        
        if dictionary.has("confirm_booking") {
            notificationSetting.confirmBooking = dictionary["confirm_booking"] as? Bool
        }
        
        if dictionary.has("new_points") {
            notificationSetting.newPoints = dictionary["new_points"] as? Bool
        }
        
        if dictionary.has("congratulation") {
            notificationSetting.congratulation = dictionary["congratulation"] as? Bool
        }
        
        if dictionary.has("Reminder") {
            notificationSetting.reminder = dictionary["Reminder"] as? Bool
        }
        
        if dictionary.has("new_password") {
            notificationSetting.newPassword = dictionary["new_password"] as? Bool
        }
        
        if dictionary.has("nickname") {
            notificationSetting.nickname = dictionary["nickname"] as? Bool
        }
        
        if dictionary.has("new_badge") {
            notificationSetting.newBadge = dictionary["new_badge"] as? Bool
        }
        
        if dictionary.has("deliverable_accepted") {
            notificationSetting.deliverableAccepted = dictionary["deliverable_accepted"] as? Bool
        }
        
        if dictionary.has("feedback") {
            notificationSetting.feedback = dictionary["feedback"] as? Bool
        }
        
        if dictionary.has("verify_account") {
            notificationSetting.verifyAccount = dictionary["verify_account"] as? Bool
        }
        
        if dictionary.has("certificate_generation") {
            notificationSetting.certificateGeneration = dictionary["certificate_generation"] as? Bool
        }
        
        if dictionary.has("deliverable_rejected") {
            notificationSetting.deliverableRejected = dictionary["deliverable_rejected"] as? Bool
        }
        
        if dictionary.has("program_reminder") {
            notificationSetting.programReminder = dictionary["program_reminder"] as? Bool
        }
        
        if dictionary.has("undo_booking") {
            notificationSetting.undoBooking = dictionary["undo_booking"] as? Bool
        }
        
        return notificationSetting
    }
}
