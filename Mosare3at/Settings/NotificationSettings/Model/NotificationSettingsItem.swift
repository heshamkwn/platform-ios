//
//  NotificationSettingsItem.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class NotificationSettingsItem {

    public var name: String!
    public var status: Bool!
    
    init() {
        
    }
    
    init(name: String, status: Bool) {
        self.name = name
        self.status = status
    }
    
    public class func convertNotificationSettingsToNotificationSettingsItems(settings: NotificationSetting) -> [NotificationSettingsItem] {
        var items = [NotificationSettingsItem]()
        if let newCertificate = settings.newCertificate {
            items.append(NotificationSettingsItem(name: "gotCert".localized(), status: newCertificate))
        }
        
        if let confirmPayment = settings.confirmPayment {
            items.append(NotificationSettingsItem(name: "confirmPayment".localized(), status: confirmPayment))
        }
        
        if let lateConfirm = settings.lateConfirm {
            items.append(NotificationSettingsItem(name: "lateConfirm".localized(), status: lateConfirm))
        }
        
        if let welcomeMail = settings.welcomeMail {
            items.append(NotificationSettingsItem(name: "welcomeMail".localized(), status: welcomeMail))
        }
        
        if let weekSummary = settings.weekSummary {
            items.append(NotificationSettingsItem(name: "weekSummary".localized(), status: weekSummary))
        }
        
        if let interested = settings.interested {
            items.append(NotificationSettingsItem(name: "interested".localized(), status: interested))
        }
        
        if let confirmBooking = settings.confirmBooking {
            items.append(NotificationSettingsItem(name: "confirmBooking".localized(), status: confirmBooking))
        }
        
        if let newPoints = settings.newPoints {
            items.append(NotificationSettingsItem(name: "newPoints".localized(), status: newPoints))
        }
        
        if let congratulation = settings.congratulation {
            items.append(NotificationSettingsItem(name: "congratulation".localized(), status: congratulation))
        }
        
        if let reminder = settings.reminder {
            items.append(NotificationSettingsItem(name: "reminder".localized(), status: reminder))
        }
        
        if let newPassword = settings.newPassword {
            items.append(NotificationSettingsItem(name: "newPassword".localized(), status: newPassword))
        }
        
        if let nickname = settings.nickname {
            items.append(NotificationSettingsItem(name: "nickname".localized(), status: nickname))
        }
        
        if let newBadge = settings.newBadge {
            items.append(NotificationSettingsItem(name: "newBadge".localized(), status: newBadge))
        }
        
        if let deliverableAccepted = settings.deliverableAccepted {
            items.append(NotificationSettingsItem(name: "deliverableAccepted".localized(), status: deliverableAccepted))
        }
        
        if let feedback = settings.feedback {
            items.append(NotificationSettingsItem(name: "feedback".localized(), status: feedback))
        }
        
        if let verifyAccount = settings.verifyAccount {
            items.append(NotificationSettingsItem(name: "verifyAccount".localized(), status: verifyAccount))
        }
        
        if let certificateGeneration = settings.certificateGeneration {
            items.append(NotificationSettingsItem(name: "certificateGeneration".localized(), status: certificateGeneration))
        }
        
        if let deliverableRejected = settings.deliverableRejected {
            items.append(NotificationSettingsItem(name: "deliverableRejected".localized(), status: deliverableRejected))
        }
        
        if let programReminder = settings.programReminder {
            items.append(NotificationSettingsItem(name: "programReminder".localized(), status: programReminder))
        }
        
        if let undoBooking = settings.undoBooking {
            items.append(NotificationSettingsItem(name: "undoBooking".localized(), status: undoBooking))
        }
        
        return items
    }
}
