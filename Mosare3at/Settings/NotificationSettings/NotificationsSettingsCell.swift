//
//  NotificationsSettingsCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift
import Material

public protocol NotificationsSettingsCellDelegate {
    func notificationStatusChanged(index: Int, status: Bool)
}

class NotificationsSettingsCell: UITableViewCell {

    public static let identifier = "NotificationsSettingsCell"
    var superView: UIView!
    
    var index: Int!
    var delegate: NotificationsSettingsCellDelegate!
    var notificationSettingsItem: NotificationSettingsItem!
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var settingsSwitch: Switch = {
        let gSwitch = Switch(state: .on, style: .light, size: .medium)
        gSwitch.buttonOffColor = UIColor.AppColors.gray
        gSwitch.trackOffColor = UIColor.AppColors.gray.withAlphaComponent(0.5)
        gSwitch.buttonOnColor = UIColor.AppColors.darkRed
        gSwitch.trackOnColor = UIColor.AppColors.darkRed.withAlphaComponent(0.5)
        return gSwitch
        
    }()
    
    lazy var horizontalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        return view
    }()
    
    public func setupViews() {
        let views = [titleLabel, settingsSwitch, horizontalView]
        
        superView = self.contentView
        
        superView.addSubviews(views)
        settingsSwitch.delegate = self
        
        settingsSwitch.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3) * -1)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(settingsSwitch.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        horizontalView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(titleLabel)
            maker.top.equalTo(titleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(1)
        }
    }
    
    public func populateData() {
        titleLabel.text = notificationSettingsItem.name
        if notificationSettingsItem.status {
            settingsSwitch.setSwitchState(state: .on)
        } else {
            settingsSwitch.setSwitchState(state: .off)
        }
    }
}

extension NotificationsSettingsCell: SwitchDelegate {
    func switchDidChangeState(control: Switch, state: SwitchState) {
        switch state {
        case .on:
            self.delegate.notificationStatusChanged(index: self.index, status: true)
            break
            
        case .off:
            self.delegate.notificationStatusChanged(index: self.index, status: false)
            break
        default:
            break
        }
    }
}
