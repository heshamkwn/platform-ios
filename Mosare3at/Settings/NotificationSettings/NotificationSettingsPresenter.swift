//
//  NotificationSettingsPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/5/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public protocol NotificationSettingsView: class {
    func operationFailed(message: String)
    func getNotificationSettingsSuccess(notificationSettings: NotificationSetting)
}

public class NotifiationSettingsPresenter {
    
    fileprivate weak var notificationSettingsView : NotificationSettingsView?
    fileprivate let notificationSettingsRepository : NotificationSettingsRepository
    
    init(repository: NotificationSettingsRepository) {
        self.notificationSettingsRepository = repository
        self.notificationSettingsRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : NotificationSettingsView) {
        notificationSettingsView = view
    }
    
}

extension NotifiationSettingsPresenter {
    public func getNotificationSettings() {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.notificationSettingsRepository.getNotificationSettings()
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func updateNotificationSettings() {
    
    }
}

extension NotifiationSettingsPresenter: NotificationSettingsPresenterDelegate {
    public func operationFailed(message: String) {
        UiHelpers.hideLoader()
        self.notificationSettingsView?.operationFailed(message: message)
    }
    
    public func getNotificationSettingsSuccess(notificationSettings: NotificationSetting) {
        UiHelpers.hideLoader()
        self.notificationSettingsView?.getNotificationSettingsSuccess(notificationSettings: notificationSettings)
    }
}
