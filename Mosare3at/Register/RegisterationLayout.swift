//
//  RegisterationLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/30/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol RegisterationLayoutDelegate : BaseLayoutDelegate {
    func goBack()
}

public class RegisterationLayout: BaseLayout {
    var topView: TopView = TopView()
    
    var registerDelegate: RegisterationLayoutDelegate!
    
    init(superview: UIView, delegate: RegisterationLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.registerDelegate = delegate
    }
    
    lazy var titleLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .center
        label.text = "register".localized()
        label.textColor = .black
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var registerTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = true
        tableView.register(RegisterationCell.self, forCellReuseIdentifier: RegisterationCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [topView, titleLabel, registerTableView]
        
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.leading.trailing.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
        
        self.registerTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.leading.trailing.bottom.equalTo(superview)
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "")
        self.topView.screenTitleLabel.isHidden = true
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")?.withRenderingMode(.alwaysTemplate)
        self.topView.backImageView.tintColor = .black
        self.topView.backgroundColor = .white
        self.topView.delegate = self
    }
}

extension RegisterationLayout: TopViewDelegate {
    public func goBack() {
        self.registerDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
