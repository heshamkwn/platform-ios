//
//  RegisterationPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/1/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import Firebase

public protocol RegisterationView : class {
    func registerSuccess()
    func operationFailed(errorMessage: String)
}

public class RegisterationPresenter {
    fileprivate weak var registerationView : RegisterationView?
    fileprivate let registerationRepository : RegisterationRepository
    
    init(repository: RegisterationRepository) {
        self.registerationRepository = repository
        self.registerationRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : RegisterationView) {
        registerationView = view
    }
}

extension RegisterationPresenter {
    public func register(parameters: [String:String]) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.registerationRepository.register(parameters: parameters)
        } else {
            self.operationFailed(errorMessage: "noInternetConnection".localized())
        }
    }
}

extension RegisterationPresenter: RegisterationPresenterDelegate {
    public func registerSuccess() {
        UiHelpers.hideLoader()
        self.registerationView?.registerSuccess()
    }
    
    public func operationFailed(errorMessage: String) {
        UiHelpers.hideLoader()
        self.registerationView?.operationFailed(errorMessage: errorMessage)
    }
}
