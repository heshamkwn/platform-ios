//
//  RegistrationVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/30/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

class RegistrationVC: BaseVC {
    
    var layout: RegisterationLayout!
    
    var presenter: RegisterationPresenter!
    
    var isAcceptTermsChecked: Bool = false
    
    public class func buildVC() -> RegistrationVC {
        return RegistrationVC()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = RegisterationLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        layout.registerTableView.dataSource = self
        layout.registerTableView.delegate = self
        layout.registerTableView.reloadData()
        
        presenter = Injector.provideRegisterationPresenter()
        presenter.setView(view: self)
    }

}

extension RegistrationVC: RegisterationView {
    func registerSuccess() {
        self.navigationController?.viewControllers.removeAll()
        self.navigationController?.pushViewController(LoginVC.buildVC(), animated: false)
    }
    
    func operationFailed(errorMessage: String) {
        self.view.makeToast(errorMessage)
    }
}

extension RegistrationVC: RegisterationLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension RegistrationVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RegisterationCell = self.layout.registerTableView.dequeueReusableCell(withIdentifier: RegisterationCell.identifier, for: indexPath) as! RegisterationCell
        cell.selectionStyle = .none
        cell.setupViews()
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 120)
    }
}

extension RegistrationVC: RegisterationCellDelegate {
    func checkAcceptTerms() {
        isAcceptTermsChecked = true
    }
    
    func register(isValid: Bool, message: String?, parameters: [String : String]?) {
        if isValid {
            self.presenter.register(parameters: parameters!)
        } else {
            self.view.makeToast(message!)
        }
    }
}
