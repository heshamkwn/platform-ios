//
//  RegisterationCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/30/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Material

public protocol RegisterationCellDelegate: class {
    func checkAcceptTerms()
    func register(isValid: Bool, message: String?, parameters: [String : String]?)
}

class RegisterationCell: UITableViewCell {

    var delegate: RegisterationCellDelegate!
    var superView: UIView!
    public static let identifier = "RegisterationCell"
    
    lazy var firstNameField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "firstName".localized())
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.returnKeyType = UIReturnKeyType.next
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var lastNameField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "familyName".localized())
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.returnKeyType = UIReturnKeyType.next
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var mobileNumberField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "mobileNumber".localized())
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.keyboardType = .phonePad
        field.returnKeyType = UIReturnKeyType.next
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var emailField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "email".localized())
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.returnKeyType = UIReturnKeyType.next
        field.autocorrectionType = .no
        field.keyboardType = .emailAddress
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var passwordField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "password".localized())
        field.isSecureTextEntry = true
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var visibilityToggleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.passwordField.isSecureTextEntry = !self.passwordField.isSecureTextEntry
            if self.passwordField.isSecureTextEntry {
                imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
            } else {
                imageView.image = UIImage(named: "eye")!.withRenderingMode(.alwaysTemplate)
            }
            imageView.tintColor = UIColor.AppColors.gray
            
        })
        return imageView
    }()
    
    lazy var acceptTermsHintLabel: UILabel = {
        let label = UILabel()
        label.text = "acceptTerms".localized()
        label.textColor = .black
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 12)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    lazy var acceptTermsTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "acceptTermsTitle".localized()
        label.textColor = .black
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 12)
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        return label
    }()
    
    lazy var checkBoxImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "not_checked")
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            if imageView.image == UIImage(named: "not_checked") {
                self.delegate.checkAcceptTerms()
                imageView.image = UIImage(named: "checked")
            }
        })
        return imageView
    }()
    
    lazy var registerButton: RaisedButton = {
        let button = RaisedButton(title: "register".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.green
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 18)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            if (self.firstNameField.text?.isEmpty)! || (self.lastNameField.text?.isEmpty)! || (self.mobileNumberField.text?.isEmpty)! || (self.emailField.text?.isEmpty)! || (self.passwordField.text?.isEmpty)! {
                self.delegate.register(isValid: false, message: "emptyFieldsError".localized(), parameters: nil)
            } else if !(self.mobileNumberField.text?.isNumber())! {
                self.delegate.register(isValid: false, message: "notValidPhone".localized(), parameters: nil)
            } else if !(self.emailField.text?.isEmail)! {
                self.delegate.register(isValid: false, message: "notValidEmail".localized(), parameters: nil)
            } else if self.checkBoxImageView.image == UIImage(named: "not_checked") {
                self.delegate.register(isValid: false, message: "checkTermsFirst".localized(), parameters: nil)
            } else {
                let parameters: [String:String] = ["firstname" : self.firstNameField.text!, "lastname" : self.lastNameField.text!, "mobile" : self.mobileNumberField.text!, "email" : self.emailField.text!, "password" : self.passwordField.text!]
                self.delegate.register(isValid: true, message: nil, parameters: parameters)
            }
           
        }
        return button
    }()
    
    public func setupViews() {
        let views = [firstNameField, lastNameField, mobileNumberField, emailField, passwordField, visibilityToggleImageView, acceptTermsHintLabel, acceptTermsTitleLabel, checkBoxImageView, registerButton]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        firstNameField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        lastNameField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(firstNameField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        mobileNumberField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(lastNameField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        emailField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(mobileNumberField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        passwordField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(emailField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        self.visibilityToggleImageView.snp.makeConstraints { maker in
            maker.trailing.equalTo(passwordField.snp.trailing)
            maker.top.equalTo(passwordField)
            maker.bottom.equalTo(passwordField).offset(-4)
            maker.width.equalTo(20)
        }
        
        acceptTermsHintLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(passwordField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo("acceptTerms".localized().height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: acceptTermsHintLabel.font))
        }
        
        acceptTermsTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo("acceptTermsTitle".localized().widthOfString(usingFont: acceptTermsTitleLabel.font))
            maker.top.equalTo(acceptTermsHintLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        checkBoxImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(acceptTermsTitleLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
             maker.top.equalTo(acceptTermsHintLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        registerButton.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.top.equalTo(checkBoxImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
    }
}
