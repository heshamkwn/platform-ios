//
//  RegisterationRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/1/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import Localize_Swift
import SwiftyUserDefaults

public protocol RegisterationPresenterDelegate {
    func registerSuccess()
    func operationFailed(errorMessage: String)
}

public class RegisterationRepository {
    var delegate: RegisterationPresenterDelegate!
    
    public func setDelegate(delegate: RegisterationPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func register(parameters: [String:String]) {
        let headers = ["Content-Type" : "application/json"]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "users")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if response.response?.statusCode == 200 || response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.registerSuccess()
                } else {
                    self.delegate.operationFailed(errorMessage: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(errorMessage: "somethingWentWrong".localized())
            }
        }
    }
}
