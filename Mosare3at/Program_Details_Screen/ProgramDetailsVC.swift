//
//  ProgramDetailsVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import AVKit

class ProgramDetailsVC: BaseVC {
    
    var presenter: ProgramDetailsPresenter!

    var layout: ProgramDetailsLayout!
    
    var programDetails: ProgramDetails!
        
    public class func buildVC() -> ProgramDetailsVC {
        return ProgramDetailsVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = ProgramDetailsLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        presenter = Injector.provideProgramDetailsPresenter()
        presenter.setView(view: self)
        
        let programId = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").get(at: Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").count - 1)
        
        presenter.getProgramDetails(programId: Int(programId!)!)
    }
}

extension ProgramDetailsVC: ProgramDetailsView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getProgramDetailsSuccess(programDetails: ProgramDetails) {
        self.programDetails = programDetails
        
        layout.programDetailsTableView.dataSource = self
        layout.programDetailsTableView.delegate = self
        layout.programDetailsTableView.reloadData()
    }
}

extension ProgramDetailsVC: ProgramDetailsLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func share(image: UIImage) {
        UiHelpers.shareImage(sharableImage: image, sourceView: self.view, vc: self)
    }
    
    func retry() {
        
    }
}

extension ProgramDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProgramDetailsCell.identifier, for: indexPath) as! ProgramDetailsCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.programDetails = programDetails
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.cellForRow(at: indexPath) as? ProgramDetailsCell
        
        if let _ = cell {
            var height:CGFloat = 0.0
            
            switch cell?.selection {
            case 0:
                height = cell?.mainTabView.getTabHeight() ?? 0
                break
                
            case 1:
                height = cell?.forWhoTabView.getTabHeight() ?? 0
                break
                
            case 2:
                height = cell?.methodologyTabView.getTabHeight() ?? 0
                break
                
            case 3:
                height = cell?.questionTabView.getTabHeight() ?? 0
                break
                
            default:
                break
            }
            
            return height + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55)
        } else {
            var programSummary = ""
            for data in programDetails.data {
                if data.kind == "textBlock" && data.title == "عن البرنامج" {
                    programSummary = data.html.get(at: 0)!
                    programSummary = programSummary.byConvertingHTMLToPlainText()
                }
            }
            
            return  UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + programSummary.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: AppFont.font(type: .Bold, size: 16), lineBreakMode: .byWordWrapping) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65)
        }
    }
}

extension ProgramDetailsVC: ProgramDetailsCellDelegate {
    func playVideo() {
        let videoURL = URL(string: programDetails.video)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func refreshBelowView(selection: Int) {
        self.layout.programDetailsTableView.beginUpdates()
        self.layout.programDetailsTableView.endUpdates()
    }
    
    func openProjectDetails(index: Int) {
        // get id from request id not from the id
        
        let projectId = (programDetails.projects.get(at: index)?.requestId)!.components(separatedBy: "/")[(programDetails.projects.get(at: index)?.requestId)!.components(separatedBy: "/").count - 1]
        
        navigator.navigateToProjectDetails(projectId: Int(projectId)!)
    }
}
