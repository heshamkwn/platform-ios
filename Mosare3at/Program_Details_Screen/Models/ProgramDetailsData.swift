//
//  ProgramDetailsData.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class ProgramDetailsData {
    var kind: String!
    var title: String!
    var html: [String]!
    var audience: [String]!
    var data: String!
    var dataImageBox: [ImageBoxData]!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["kind"] = kind
        dictionary["title"] = title
        dictionary["html"] = html
        dictionary["audience"] = audience
        
        if let _ = data {
            dictionary["data"] = data
        } else if let _ = dataImageBox {
            var dataImageBoxDicArray = [Dictionary<String, Any>]()
            
            for imageBox in dataImageBox {
                dataImageBoxDicArray.append(imageBox.convertToDictionary())
            }
            dictionary["data"] = dataImageBoxDicArray
        }
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> ProgramDetailsData {
        
        let data = ProgramDetailsData()
        data.kind =  dictionary["kind"] as? String
        data.title =  dictionary["title"] as? String
        data.html =  dictionary["html"] as? [String]
        data.audience =  dictionary["audience"] as? [String]
        
        if let dataImageBoxDicArray = dictionary["data"], dataImageBoxDicArray is [Dictionary<String, Any>] {
            var imageBoxDataArray: [ImageBoxData] = []
            for dic in dataImageBoxDicArray as! [Dictionary<String, Any>] {
                imageBoxDataArray.append(ImageBoxData.getInstance(dictionary: dic))
            }
            data.dataImageBox = imageBoxDataArray
        } else if let dataString = dictionary["data"], dataString is String {
            data.data = dataString as? String
        }
        return data
    }
}
