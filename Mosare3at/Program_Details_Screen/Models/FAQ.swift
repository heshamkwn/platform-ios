//
//  FAQ.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class FAQ {
    var requestId: String!
    var question: String!
    var answer: String!
    var weight: Int!
    
    var isExpanded: Bool = false // for internal usage
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["@id"] = requestId
        dictionary["question"] = question
        dictionary["answer"] = answer
        dictionary["weight"] = weight
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> FAQ {
        
        let faq = FAQ()
        faq.requestId =  dictionary["@id"] as? String
        faq.question =  dictionary["question"] as? String
        faq.answer =  dictionary["answer"] as? String
        faq.weight =  dictionary["weight"] as? Int
        return faq
    }
}
