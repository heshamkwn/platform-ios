//
//  ImageBoxData.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class ImageBoxData {
    var image: String!
    var title: String!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["image"] = image
         dictionary["title"] = title
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> ImageBoxData {
        
        let imageBoxData = ImageBoxData()
        imageBoxData.image =  dictionary["image"] as? String
        imageBoxData.title =  dictionary["title"] as? String
        return imageBoxData
    }
}
