//
//  Guest.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class Guest {
    var requestId: String!
    var prefix: String!
    var name: String!
    var description: String!
    var image: String!
    var weight: Int!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["image"] = image
        dictionary["@id"] = requestId
        dictionary["prefix"] = prefix
        dictionary["name"] = name
        dictionary["description"] = description
        dictionary["weight"] = weight
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Guest {
        
        let guest = Guest()
        guest.image =  dictionary["image"] as? String
        guest.requestId =  dictionary["@id"] as? String
        guest.prefix =  dictionary["prefix"] as? String
        guest.name =  dictionary["name"] as? String
        guest.description =  dictionary["description"] as? String
        guest.weight =  dictionary["weight"] as? Int
        return guest
    }
}
