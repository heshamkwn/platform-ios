//
//  ProgramDetails.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation


public class ProgramDetails {
    var id: Int!
    var title: String!
    var summary: String!
    var image: String!
    var bgImage: String!
    var catalogImage: String!
    var languages: [String]!
    var startDate: String!
    var endDate: String!
    var expirationBookingDate: String!
    var cancelSubscriptionDate: String!
    var duration: String!
    var price: Int!
    var salePrice: Int!
    var booked: Int!
    var weight: Int!
    var total: Int!
    var projects: [Project]!
    var faqs: [FAQ]!
    var guests: [Guest]!
    var batch: Int!
    var featured: Bool!
    var video: String!
    var data: [ProgramDetailsData]!
    var methodology: [ProgramDetailsMethodology]!
    var approved: Bool!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["id"] = "id"
        dictionary["title"] = title
        dictionary["summary"] = summary
        dictionary["image"] = image
        dictionary["bgImage"] = bgImage
        dictionary["catalogImage"] = catalogImage
        dictionary["language"] = languages
        dictionary["startDate"] = startDate
        dictionary["endDate"] = endDate
        dictionary["expirationBookingDate"] = expirationBookingDate
        dictionary["cancelSubscriptionDate"] = cancelSubscriptionDate
        dictionary["duration"] = duration
        dictionary["price"] = price
        dictionary["title"] = title
        dictionary["salePrice"] = salePrice
        dictionary["booked"] = booked
        dictionary["weight"] = weight
        dictionary["total"] = total
        
        if let _ = projects {
            var projectsDicArray = [Dictionary<String, Any>]()
            
            for project in projects {
                projectsDicArray.append(project.convertToDictionary())
            }
            dictionary["projects"] = projectsDicArray
        }
        
        if let _ = faqs {
            var faqsDicArray = [Dictionary<String, Any>]()
            
            for faq in faqs {
                faqsDicArray.append(faq.convertToDictionary())
            }
            dictionary["faqs"] = faqsDicArray
        }
        
        if let _ = guests {
            var guestsDicArray = [Dictionary<String, Any>]()
            
            for guest in guests {
                guestsDicArray.append(guest.convertToDictionary())
            }
            dictionary["guests"] = guestsDicArray
        }
        
        dictionary["batch"] = batch
        dictionary["featured"] = featured
        dictionary["video"] = video
        
        if let _ = data {
            var dataDicArray = [Dictionary<String, Any>]()
            
            for dataObj in data {
                dataDicArray.append(dataObj.convertToDictionary())
            }
            dictionary["data"] = dataDicArray
        }
        
        if let _ = methodology {
            var methodologyDicArray = [Dictionary<String, Any>]()
            
            for methodologyObj in methodology {
                methodologyDicArray.append(methodologyObj.convertToDictionary())
            }
            dictionary["methodology"] = methodologyDicArray
        }
        
        dictionary["approved"] = approved
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> ProgramDetails {
        
        let programDetails = ProgramDetails()
        programDetails.id =  dictionary["id"] as? Int
        programDetails.title =  dictionary["title"] as? String
        programDetails.summary =  dictionary["summary"] as? String
        programDetails.image =  dictionary["image"] as? String
        programDetails.bgImage =  dictionary["bgImage"] as? String
        programDetails.catalogImage =  dictionary["catalogImage"] as? String
        programDetails.languages =  dictionary["language"] as? [String]
        programDetails.startDate =  dictionary["startDate"] as? String
        programDetails.endDate =  dictionary["endDate"] as? String
        programDetails.title =  dictionary["title"] as? String
        programDetails.expirationBookingDate =  dictionary["expirationBookingDate"] as? String
        programDetails.cancelSubscriptionDate =  dictionary["cancelSubscriptionDate"] as? String
        programDetails.duration =  dictionary["duration"] as? String
        programDetails.price =  dictionary["price"] as? Int
        programDetails.salePrice =  dictionary["salePrice"] as? Int
        programDetails.booked =  dictionary["booked"] as? Int
        programDetails.weight =  dictionary["weight"] as? Int
        programDetails.total =  dictionary["html"] as? Int
        
        if let projectsDicArray = dictionary["projects"], projectsDicArray is [Dictionary<String, Any>] {
            var projectsArray: [Project] = []
            for dic in projectsDicArray as! [Dictionary<String, Any>] {
                projectsArray.append(Project.getInstance(dictionary: dic))
            }
            programDetails.projects = projectsArray
        }
        
        if let faqsDicArray = dictionary["faqs"], faqsDicArray is [Dictionary<String, Any>] {
            var faqsArray: [FAQ] = []
            for dic in faqsDicArray as! [Dictionary<String, Any>] {
                faqsArray.append(FAQ.getInstance(dictionary: dic))
            }
            programDetails.faqs = faqsArray
        }
        
        if let guestsDicArray = dictionary["guests"], guestsDicArray is [Dictionary<String, Any>] {
            var guestsArray: [Guest] = []
            for dic in guestsDicArray as! [Dictionary<String, Any>] {
                guestsArray.append(Guest.getInstance(dictionary: dic))
            }
            programDetails.guests = guestsArray
        }
        
        programDetails.batch =  dictionary["batch"] as? Int
        programDetails.featured =  dictionary["featured"] as? Bool
        programDetails.video =  dictionary["video"] as? String
        
        if let dataDicArray = dictionary["data"], dataDicArray is [Dictionary<String, Any>] {
            var dataArray: [ProgramDetailsData] = []
            for dic in dataDicArray as! [Dictionary<String, Any>] {
                dataArray.append(ProgramDetailsData.getInstance(dictionary: dic))
            }
            programDetails.data = dataArray
        }
        
        if let methodologyDicArray = dictionary["methodology"], methodologyDicArray is [Dictionary<String, Any>] {
            var methodologyArray: [ProgramDetailsMethodology] = []
            for dic in methodologyDicArray as! [Dictionary<String, Any>] {
                methodologyArray.append(ProgramDetailsMethodology.getInstance(dictionary: dic))
            }
            programDetails.methodology = methodologyArray
        }
        
        programDetails.approved =  dictionary["approved"] as? Bool
        return programDetails
    }
}
