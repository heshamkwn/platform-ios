//
//  ProgramDetailsMethodology.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class ProgramDetailsMethodology {
    var kind: String!
    var image: String!
    var title: String!
    var description: String!
    var list: [String]!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["kind"] = kind
        dictionary["title"] = title
        dictionary["image"] = image
        dictionary["description"] = description
        dictionary["list"] = list
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> ProgramDetailsMethodology {
        
        let methodology = ProgramDetailsMethodology()
        methodology.image =  dictionary["image"] as? String
        methodology.title =  dictionary["title"] as? String
        methodology.kind =  dictionary["kind"] as? String
        methodology.description =  dictionary["description"] as? String
        methodology.list =  dictionary["list"] as? [String]
        return methodology
    }
}
