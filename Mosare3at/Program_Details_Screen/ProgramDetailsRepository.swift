//
//  ProgramDetailsRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol ProgramDetailsPresenterDelegate: class {
    func operationFailed(message: String)
    func getProgramDetailsSuccess(programDetails: ProgramDetails)
}

public class ProgramDetailsRepository {
    var delegate: ProgramDetailsPresenterDelegate!
    
    public func setDelegate(delegate: ProgramDetailsPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getProgramDetails(programId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        //"Content-Type" : "application/json",
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "programs/\(programId)/")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let programDetails = ProgramDetails.getInstance(dictionary: json)
                        Defaults[.currentProgramStartDate] = programDetails.startDate
                        self.delegate.getProgramDetailsSuccess(programDetails: programDetails)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
