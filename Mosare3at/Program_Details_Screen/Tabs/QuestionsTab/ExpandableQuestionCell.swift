//
//  ExpandableQuestionCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/3/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol ExpandableQuestionCellDelegate: class {
    func expandCell(index: Int, isExpanded: Bool)
}

class ExpandableQuestionCell: UITableViewCell {
    
    static let identifier = "ExpandableQuestionCell"
    var superView: UIView!
    
    var question: FAQ!
    var delegate: ExpandableQuestionCellDelegate!

    var index: Int!
    
   lazy var mainContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.isUserInteractionEnabled = true
        view.addTapGesture { action in
//            print(self.question.isExpanded)
//            self.delegate.expandCell(index: self.index, isExpanded: self.answerLabel.isHidden)
        }
        return view
    }()
    
    lazy var questionContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        view.isUserInteractionEnabled = true
        return view
    }()
    
    lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.isUserInteractionEnabled = false
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "solid_down_arrow")
        return imageView
    }()
    
    lazy var questionLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 14)
        label.isUserInteractionEnabled = false
        return label
    }()
    
    lazy var answerLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
//        label.isHidden = true
        label.font = AppFont.font(type: .Regular, size: 14)
        label.isUserInteractionEnabled = false
        return label
    }()
    
    public func setupViews() {
        let views = [mainContainerView, questionContainerView, arrowImageView, questionLabel, answerLabel]
        
        superView = self.contentView
        
        superView.addSubviews(views)
        self.questionContainerView.addSubviews([arrowImageView, questionLabel])
        
        questionContainerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        arrowImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(questionContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(questionContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 7))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        questionLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(arrowImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 0.5))
            maker.trailing.equalTo(questionContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(questionContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
        }
        
        answerLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(questionContainerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(question.answer.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), font: answerLabel.font, lineBreakMode: .byWordWrapping))
        }
        
        mainContainerView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(superView)
        }
        
        superView.bringSubviewToFront(mainContainerView)
    }
    
    public func populateData() {
//        if question.isExpanded {
//            answerLabel.isHidden = false
//            arrowImageView.image = UIImage(named: "solid_down_arrow")
//        } else {
//            answerLabel.isHidden = true
//            arrowImageView.image = UIImage(named: "solid_arrow")
//        }
        
        questionLabel.text = question.question
        answerLabel.text = question.answer
    }
    
}
