//
//  MainTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/3/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Localize_Swift

public protocol MainTabViewDelegate: class {
    func openProjectDetails(index: Int)
}

public class MainTabView: UIView {
    
    var programSummary: String!
    var projects: [Project]!
    var guests: [Guest]!
    
    var delegate: MainTabViewDelegate!
    
    public class func getInstance(programSummary: String, projects: [Project], guests: [Guest], delegate: MainTabViewDelegate) -> MainTabView {
        let view: MainTabView = MainTabView()
        view.programSummary = programSummary
        view.projects = projects
        view.guests = guests
        view.delegate = delegate
        return view
    }
    
    lazy var programSummaryView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var programSummaryTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "programSummary".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    lazy var programSummaryValueLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var projectsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var projectsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "programProjects".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    var projectsCollectionView: UICollectionView = UICollectionView(frame: CGRect(),collectionViewLayout: UICollectionViewFlowLayout())
    
    lazy var guestsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var guestsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "guests".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    var guestsCollectionView: UICollectionView = UICollectionView(frame: CGRect(),collectionViewLayout: UICollectionViewFlowLayout())
    
    public func setupViews() {
        let views = [programSummaryView, programSummaryTitleLabel, programSummaryValueLabel, projectsView, projectsLabel, projectsCollectionView, guestsView, guestsLabel, guestsCollectionView]
        
        self.backgroundColor = .white
        
        self.addSubviews(views)
        
        self.programSummaryView.addSubviews([programSummaryTitleLabel, programSummaryValueLabel])
        
        self.programSummaryView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
             maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
             maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + programSummary.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: programSummaryValueLabel.font, lineBreakMode: .byWordWrapping))
        }
        
        self.programSummaryTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(programSummaryView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(programSummaryView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(programSummaryView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.programSummaryValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(programSummaryView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(programSummaryView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(programSummaryTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(programSummary.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: programSummaryValueLabel.font, lineBreakMode: .byWordWrapping))
        }
        
        projectsView.addSubviews([projectsLabel, projectsCollectionView])
        
        projectsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(programSummaryView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55))
        }
        
        self.projectsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.projectsCollectionView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(projectsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 48))
        }
        
        guestsView.addSubviews([guestsLabel, guestsCollectionView])
        
        guestsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(projectsView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65))
        }
        
        self.guestsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.guestsCollectionView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(guestsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 58))
        }
        
    }
    
    public func populateData() {
        programSummaryValueLabel.text = programSummary
//        projects = projects.reversed()
        
        projectsCollectionView.backgroundColor = .clear
        projectsCollectionView.dataSource = self
        projectsCollectionView.delegate = self
        projectsCollectionView.register(ProjectCell.self, forCellWithReuseIdentifier: ProjectCell.identifier)
        projectsCollectionView.showsHorizontalScrollIndicator = false
        projectsCollectionView.isScrollEnabled = true
        projectsCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        projectsCollectionView.contentSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 100), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 48))
        if let layout = projectsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 45), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 45))
        }
        
        projectsCollectionView.reloadData()
        
        guestsCollectionView.backgroundColor = .clear
        guestsCollectionView.dataSource = self
        guestsCollectionView.delegate = self
        guestsCollectionView.register(GuestCell.self, forCellWithReuseIdentifier: GuestCell.identifier)
        guestsCollectionView.showsHorizontalScrollIndicator = false
        guestsCollectionView.isScrollEnabled = true
        guestsCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        guestsCollectionView.contentSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 100), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 58))
        if let layout = guestsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 35), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55))
        }
        
        guestsCollectionView.reloadData()
    }
    
    public func getTabHeight() -> CGFloat {
        var height: CGFloat = 0.0
        
        height = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + self.programSummary.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: programSummaryValueLabel.font, lineBreakMode: .byWordWrapping) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65)
        
        return height
    }
}

extension MainTabView: UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == projectsCollectionView {
            return projects.count
        } else {
            return guests.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == projectsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectCell.identifier, for: indexPath) as! ProjectCell
            cell.index = projects.count - 1 - indexPath.row
            cell.project = projects.get(at: projects.count - 1 - indexPath.row)!
            cell.delegate = self
            cell.setupViews()
            cell.populateData()
            cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GuestCell.identifier, for: indexPath) as! GuestCell
            cell.guest = guests.get(at: indexPath.row)!
            cell.setupViews()
            cell.populateData()
            cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            return cell
        }
    }
}

extension MainTabView: ProjectCellDelegate {
    public func openProjectDetails(index: Int) {
        self.delegate.openProjectDetails(index: index)
    }
}
