//
//  ProjectCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/3/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol ProjectCellDelegate: class {
    func openProjectDetails(index: Int)
}

class ProjectCell: UICollectionViewCell {
    
    public static let identifier = "ProjectCell"
    var superView: UIView!
    
    var project: Project!
    var index: Int!
    var delegate: ProjectCellDelegate!
    
    lazy var mainContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        view.addTapGesture(action: { (_) in
            self.delegate.openProjectDetails(index: self.index)
        })
        return view
    }()
    
    lazy var projectIndexLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 15)
        return label
    }()
    
    lazy var projectNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var projectPhotoImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 8
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var dateImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "date_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var dateView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var durationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "alarm_white")
        return imageView
    }()
    
    lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var projectDescLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    public func setupViews() {
        let views = [mainContainerView, projectIndexLabel, projectNameLabel, projectPhotoImageview, dateImageView, dateLabel, dateView, durationImageView, durationLabel, projectDescLabel]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        mainContainerView.addSubviews([projectIndexLabel, projectNameLabel, projectPhotoImageview, dateImageView, dateLabel, dateView, durationImageView, durationLabel, projectDescLabel])
        
        mainContainerView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(superView)
        }
        
        projectPhotoImageview.snp.makeConstraints { (maker) in
            maker.leading.trailing.top.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 38))
        }
        
        projectIndexLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(mainContainerView)
            maker.top.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        projectNameLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(mainContainerView)
            maker.top.equalTo(projectIndexLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        dateImageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.leading.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 8))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 4))
        }
        
        dateLabel.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.leading.equalTo(dateImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
        }
        
        dateView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(superView.snp.centerX)
             maker.bottom.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(1)
        }
        
        durationImageView.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.leading.equalTo(dateView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 8))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        durationLabel.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(projectPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.leading.equalTo(durationImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 13))
        }
        
        projectDescLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(projectPhotoImageview.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        mainContainerView.bringSubviewToFront(projectIndexLabel)
        mainContainerView.bringSubviewToFront(projectNameLabel)
        
    }
    
    public func populateData() {
        projectIndexLabel.text = UiHelpers.getIndexName(index: self.index)
        projectNameLabel.text = project.title
        dateLabel.text = project.startDate.components(separatedBy: "T")[0]
        durationLabel.text = project.duration
        projectDescLabel.text = project.summary
        projectPhotoImageview.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + project.image)!)
    }
    
}
