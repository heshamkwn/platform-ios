//
//  GuestCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/3/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

class GuestCell: UICollectionViewCell {
    
    public static let identifier = "GuestCell"
    var superView: UIView!
    
    var guest: Guest!
    
    lazy var guestImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 8
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var guestNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var guestDescLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    public func setupViews() {
        let views = [guestImageView, guestNameLabel, guestDescLabel]
        
        self.superView = self.contentView
        self.superView.addSubviews(views)
        
        guestImageView.snp.makeConstraints { (maker) in
            maker.leading.trailing.top.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 40))
        }
        
        guestNameLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(guestImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        guestDescLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(guestNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
        }
    }
    
    public func populateData() {
        guestImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + guest.image)!)
        guestNameLabel.text = guest.prefix + " / " + guest.name
        guestDescLabel.text = guest.description
    }
}
