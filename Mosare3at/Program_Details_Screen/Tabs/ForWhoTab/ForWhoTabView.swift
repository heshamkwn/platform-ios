//
//  ForWhoTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/3/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Localize_Swift

public class ForWhoTabView: UIView {
    
    var targetAudience: String!
    
    public class func getInstance(targetAudience: String) -> ForWhoTabView {
        let view: ForWhoTabView = ForWhoTabView()
        view.targetAudience = targetAudience
        return view
    }
    
    lazy var targetAudienceView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var targetAudienceTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "targetAudience".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    lazy var targetAudienceValueLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    public func setupViews() {
        let views = [targetAudienceView, targetAudienceTitleLabel, targetAudienceValueLabel]
        
        self.backgroundColor = .white
        
        self.addSubviews(views)
        
        self.targetAudienceView.addSubviews([targetAudienceTitleLabel, targetAudienceValueLabel])
        
        self.targetAudienceView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + targetAudience.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: targetAudienceValueLabel.font, lineBreakMode: .byWordWrapping))
        }
        
        self.targetAudienceTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(targetAudienceView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(targetAudienceView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(targetAudienceView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.targetAudienceValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(targetAudienceView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(targetAudienceView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(targetAudienceTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(targetAudience.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: targetAudienceValueLabel.font, lineBreakMode: .byWordWrapping))
        }
    }
    
    public func populateData() {
        targetAudienceValueLabel.text = targetAudience
    }
    
    public func getTabHeight() -> CGFloat {
        var height: CGFloat = 0.0
        
        height = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + targetAudience.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: targetAudienceValueLabel.font, lineBreakMode: .byWordWrapping)
        
        return height
    }
}
