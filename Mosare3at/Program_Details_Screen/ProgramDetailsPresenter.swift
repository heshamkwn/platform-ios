//
//  ProgramDetailsPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation


public protocol ProgramDetailsView: class {
    func operationFailed(message: String)
    func getProgramDetailsSuccess(programDetails: ProgramDetails)
}

public class ProgramDetailsPresenter {
    fileprivate weak var programDetailsView : ProgramDetailsView?
    fileprivate let programDetailsRepository : ProgramDetailsRepository
    
    init(repository: ProgramDetailsRepository) {
        self.programDetailsRepository = repository
        self.programDetailsRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : ProgramDetailsView) {
        programDetailsView = view
    }
}

extension ProgramDetailsPresenter {
    public func getProgramDetails(programId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.programDetailsRepository.getProgramDetails(programId: programId)
        } else {
            self.programDetailsView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension ProgramDetailsPresenter: ProgramDetailsPresenterDelegate {
    public func operationFailed(message: String) {
        self.programDetailsView?.operationFailed(message: message)
    }
    
    public func getProgramDetailsSuccess(programDetails: ProgramDetails) {
        self.programDetailsView?.getProgramDetailsSuccess(programDetails: programDetails)
    }
}
