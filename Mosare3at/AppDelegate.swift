//
//  AppDelegate.swift
//  Mosare3at
//
//  Created by Hesham Donia on 9/30/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import Localize_Swift
import Material
import Firebase
import UserNotifications
import AlamofireNetworkActivityIndicator
import AVFoundation
import Alamofire
import DropDown

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    public static var instance: AppDelegate!
    public var unreadNotificationsNumber: Int = 0

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Localize.setCurrentLanguage("ar")

        AppDelegate.instance = self
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.2
        NetworkActivityIndicatorManager.shared.completionDelay = 0.5
        DropDown.startListeningToKeyboard()
        FirebaseApp.configure()
        setUpNotification(application)
        if UiHelpers.isInternetAvailable() && Defaults[.isLoggedIn] != nil && Defaults[.isLoggedIn]! {
            getNotSeenNotifications(url: CommonConstants.BASE_URL + "notifications?seen=false&flag=mob&both")
            getBadges()
        }
        startApplication()
        
//        if Defaults[.isWeekSummary] == nil {
//            Defaults[.isWeekSummary] = true
//        }
//
//        if Defaults[.isNewPoints] == nil {
//            Defaults[.isNewPoints] = true
//        }
//
//        if Defaults[.isNewBadges] == nil {
//            Defaults[.isNewBadges] = true
//        }
//
//        if Defaults[.isProjectStart] == nil {
//            Defaults[.isProjectStart] = true
//        }
//
//        if Defaults[.isDeadLine] == nil {
//            Defaults[.isDeadLine] = true
//        }
//
//        if Defaults[.isDeliverableAccepted] == nil {
//            Defaults[.isDeliverableAccepted] = true
//        }
//
//        if Defaults[.isDeliverableRefused] == nil {
//            Defaults[.isDeliverableRefused] = true
//        }
//
//        if Defaults[.isTeamDataUpdated] == nil {
//            Defaults[.isTeamDataUpdated] = true
//        }
//
//        if Defaults[.isGotCertificate] == nil {
//            Defaults[.isGotCertificate] = true
//        }
//
//        if Defaults[.isNewVideo] == nil {
//            Defaults[.isNewVideo] = true
//        }
//
//        if Defaults[.isNewComment] == nil {
//            Defaults[.isNewComment] = true
//        }
        
        return true
    }
    
    func getNotSeenNotifications(url: String) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        Alamofire.request(URL(string: url)!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let jsonArray = json["hydra:member"] as? [Dictionary<String,AnyObject>]
                        self.unreadNotificationsNumber = self.unreadNotificationsNumber + (jsonArray?.count)!
                        if let hydraView = json["hydra:view"] as? Dictionary<String,AnyObject> {
                            if hydraView.has("hydra:next") {
                                self.getNotSeenNotifications(url: hydraView["hydra:next"] as! String)
                            } else {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: CommonConstants.NOTIFICATIONS_UPDATED), object: nil)
                            }
                        }
                    }
                }
            }
        }
    }
    
    public func getBadges() {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "badges")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let jsonArray = json["hydra:member"] as? [Dictionary<String,AnyObject>]
                        var badges = [Badge]()
                        for dic in jsonArray! {
                            let badge = Badge.getInstance(dictionary: dic)
                            badges.append(badge)
                        }
                        Singleton.getInstance().badges = badges
                    }
                }
            }
        }
    }

    public func getProgramDetails(programId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]

        Alamofire.request(URL(string: CommonConstants.BASE_URL + "programs/\(programId)/")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let programDetails = ProgramDetails.getInstance(dictionary: json)
                        Defaults[.currentProgramStartDate] = programDetails.startDate
                        self.startApplication()
                    }
                }
            }
        }
    }
    
    /**
     This method called to setup receiving remote notifications from Firebase.
     - Parameter application: The application instance.
     */
    func setUpNotification(_ application: UIApplication) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        if let isLoggedIn = Defaults[.isLoggedIn], isLoggedIn {
            if let token = Messaging.messaging().fcmToken {
                sendFCMTokenToServer(fcmToken: token)
            }
        }
    }
    
    public class func logout(view: UIView) {
        
        let headers = ["Content-Type" : "application/x-www-form-urlencoded", "X-AUTH-TOKEN" : Defaults[.token]!]
        let parameters = ["device-token" : Defaults[.fcmToken]!]
        
        UiHelpers.showLoader()
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "logout")!, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    print(json)
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        Defaults.removeAll()
                        AppDelegate.instance.startApplication()
                    } else {
                        view.makeToast("somethingWentWrong".localized())
                    }
                }
            }
        }
        
        
    }
    
    /**
     This method set the allignment of all used design components according to current language and set the root viewController according to loggedIn or not.
     */
    func startApplication() {
        
        Localize.setCurrentLanguage("ar")
        
        switch Localize.currentLanguage() {
        case "ar":
            
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            FlatButton.appearance().semanticContentAttribute = .forceRightToLeft
            FlatButton.appearance().contentHorizontalAlignment = .right
            RaisedButton.appearance().semanticContentAttribute = .forceRightToLeft
            RaisedButton.appearance().contentHorizontalAlignment = .right
            Button.appearance().contentHorizontalAlignment = .right
            Button.appearance().semanticContentAttribute = .forceRightToLeft
            UIButton.appearance().semanticContentAttribute = .forceRightToLeft
            UIImageView.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
            ErrorTextField.appearance().semanticContentAttribute = .forceRightToLeft
            DropDown.appearance().semanticContentAttribute = .forceRightToLeft
            TextField.appearance().semanticContentAttribute = .forceRightToLeft
            Switch.appearance().semanticContentAttribute = .forceRightToLeft
            UITableViewCell.appearance().semanticContentAttribute = .forceRightToLeft
            UICollectionViewCell.appearance().semanticContentAttribute = .forceRightToLeft
            UISearchBar.appearance().semanticContentAttribute = .forceRightToLeft
            UIProgressView.appearance().semanticContentAttribute = .forceRightToLeft
            

            break
            
        case "en":
            Switch.appearance().semanticContentAttribute = .forceLeftToRight
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UILabel.appearance().semanticContentAttribute = .forceLeftToRight
            FlatButton.appearance().semanticContentAttribute = .forceLeftToRight
            FlatButton.appearance().contentHorizontalAlignment = .left
            RaisedButton.appearance().contentHorizontalAlignment = .left
            RaisedButton.appearance().semanticContentAttribute = .forceLeftToRight
            Button.appearance().contentHorizontalAlignment = .left
            Button.appearance().semanticContentAttribute = .forceLeftToRight
            UIButton.appearance().semanticContentAttribute = .forceLeftToRight
            UIImageView.appearance().semanticContentAttribute = .forceLeftToRight
            UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
            ErrorTextField.appearance().semanticContentAttribute = .forceLeftToRight
            DropDown.appearance().semanticContentAttribute = .forceLeftToRight
            TextField.appearance().semanticContentAttribute = .forceLeftToRight
            UITableViewCell.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionViewCell.appearance().semanticContentAttribute = .forceLeftToRight
            UISearchBar.appearance().semanticContentAttribute = .forceLeftToRight
            UIProgressView.appearance().semanticContentAttribute = .forceLeftToRight
            break
            
        default:
            break
        }
        
        // setting the font and text color for tab bar items
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.AppColors.darkGray, NSAttributedString.Key.font: AppFont.font(type: .Bold, size: 12)], for: .normal)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.AppColors.primaryColor, NSAttributedString.Key.font: AppFont.font(type: .Bold, size: 12)], for: .selected)
        
        let navigationController = UINavigationController()
        
        navigationController.navigationBar.setStyle(style: .solid, tintColor: UIColor.white, forgroundColor: .black)

        if let loggedIn = Defaults[.isLoggedIn], loggedIn {
            if let userDic = Defaults[.user] {
                let user = User.getInstance(dictionary: userDic)
                if Defaults[.currentProgramStartDate] != nil {
                    let finalCurrentProgramStartDate = Defaults[.currentProgramStartDate]?.components(separatedBy: "+").get(at: 0)
                    if AppDelegate.isCurrentProgramStarted(programStartDate: finalCurrentProgramStartDate!) {
                        if !user.takeTutorial {
                            self.window?.rootViewController = navigationController
                            navigationController.pushViewController(TutorialPagerVC.buildVC(), animated: false)
                        } else {
                            navigationController.navigationBar.isHidden = true
                            self.window?.rootViewController = navigationController
                            navigationController.pushViewController(MainScreenVC.buildVC(), animated: false)
                        }
                    } else {
                        navigationController.navigationBar.isHidden = true
                        self.window?.rootViewController = navigationController
                        navigationController.pushViewController(WaitingVC.buildVC(), animated: false)
                    }
                } else {
                    navigationController.navigationBar.isHidden = true
                    self.window?.rootViewController = navigationController
                    navigationController.pushViewController(MainScreenVC.buildVC(), animated: false)
//                    getSubscriptions(userId: user.id, token: user.token)
                    let programID = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").get(at: Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").count - 1)
                    getProgramDetails(programId: Int(programID!)!)
                    
                }

            } else {
                self.window?.rootViewController = navigationController
                navigationController.pushViewController(LoginVC.buildVC(), animated: false)
            }
        } else {
            self.window?.rootViewController = navigationController
            navigationController.pushViewController(LoginVC.buildVC(), animated: false)
        }
        
        window?.makeKeyAndVisible()
    }
    
    /**
     This method is called to play a sound when receive notificaion in forground.
     */
    func playSound() {
        // create a sound ID, in this case its the tweet sound.
        let systemSoundID: SystemSoundID = 1007
        
        // to play sound
        AudioServicesPlaySystemSound(systemSoundID)
    }
    
   public static func isCurrentProgramStarted(programStartDate: String) -> Bool {
        let currentDate = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //Your date format
        let programDate = dateFormatter.date(from: programStartDate)
        
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: currentDate, to: programDate!)
        
        
        let years = components.year ?? -1
        let months = components.month ?? -1
        let days = components.day ?? -1
        let hours = components.hour ?? -1
        let minutes = components.minute ?? -1
        let seconds = components.second ?? -1
        
        if years == 0 {
            if months == 0 {
                if days == 0 {
                    if hours == 0 {
                        if minutes == 0 {
                            if seconds == 0 {
                                return true
                            } else if seconds > 0 {
                                return false
                            } else {
                                return true
                            }
                        } else if minutes > 0 {
                            return false
                        } else {
                            return true
                        }
                    } else if hours > 0 {
                        return false
                    } else {
                        return true
                    }
                } else if days > 0 {
                    return false
                } else {
                    return true
                }
            } else if months > 0 {
                return false
            } else {
                return true
            }
        } else if years > 0 {
            return false
        } else {
            return true
        }
    }
    
    /**
     This method is called to send the user's current FCM Token to be saved on server.
     */
    func sendFCMTokenToServer(fcmToken: String) {
        if Defaults[.isLoggedIn] != nil && Defaults[.isLoggedIn]! {
            let user: User = User.getInstance(dictionary: Defaults[.user]!)
            let loginPresenter = Injector.provideLoginPresenter()
            
            if let currentFCMToken = Defaults[.fcmToken] {
                loginPresenter.updateFCMToken(id: user.id, oldToken: currentFCMToken, newToken: fcmToken)
            } else {
                loginPresenter.registerFCMToken(id: user.id, token: fcmToken)
            }
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CommonConstants.backgroundStatus), object: nil)
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: CommonConstants.forgroundStatus), object: nil)
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // forground
    // Receive displayed notifications for iOS 10 devices.
    /**
     This method is called when receive remote notification and the app in the forground state.
     - Parameter center: Instance of notification center.
     - Parameter notification: The notification object that contains the needed data.
     - Parameter completionHandler: What will happen after that function called.
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
        playSound()
        AppDelegate.instance.unreadNotificationsNumber = AppDelegate.instance.unreadNotificationsNumber  + 1
        
        NotificationCenter.default.post(name: NSNotification.Name(CommonConstants.NOTIFICATIONS_UPDATED), object: nil)
    }
    
    // de elly sha3`ala m3 l background notifications
    /**
     This method is called when receive remote notification and click on it and the app in the background state.
     - Parameter center: Instance of notification center.
     - Parameter notification: The notification object that contains the needed data.
     - Parameter completionHandler: What will happen after that function called.
     */
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo
        AppDelegate.instance.unreadNotificationsNumber = AppDelegate.instance.unreadNotificationsNumber  + 1
        
        
        var navController: UINavigationController?
        
        var currentVC: UIViewController?
        
        if window?.rootViewController is UINavigationController {
            navController = window?.rootViewController as? UINavigationController
            currentVC = navController?.visibleViewController
        } else {
            navController = UINavigationController()
            let mainScreenVC = MainScreenVC.buildVC()
            mainScreenVC.selectedIndex = 0
            navController?.viewControllers = [mainScreenVC]
            currentVC = navController?.viewControllers[0]
        }
        
        if userInfo.has("user_id") {
            if let userId = userInfo["user_id"] as? String {
                if let currentUserId = User.getInstance(dictionary: Defaults[.user]!).id {
                    if userId == String(currentUserId) {
                        let notificationType = userInfo["notification_type"] as! String
                        switch notificationType {
                            
                        case CommonConstants.weekSummaryNotificationType: // go to week summary screen (New Screen)
                            let notificationData = NotificationData.getInstance(dictionary: userInfo["data"] as! Dictionary<String, Any>)
                            let weekSummaryVC = WeekSummaryVC.buildVC(id: notificationData.objectId)
                            currentVC?.pushVC(weekSummaryVC)
                            break
                            
                        case CommonConstants.newPointsNotificationType: // go to leaderboard screen (tab 2)
                            if currentVC is MainScreenVC {
                                (currentVC as? MainScreenVC)?.selectedIndex = 1
                            } else {
                                let mainScreenVC = MainScreenVC.buildVC()
                                mainScreenVC.selectedIndex = 1
                                currentVC?.presentVC(mainScreenVC)
                            }
                            break
                            
                        case CommonConstants.newBadgeNotificationType: // go to all badges screen
                            let allBadgesVC = AllBadgesVC.buildVC(badges: Singleton.getInstance().badges)
                            currentVC?.pushVC(allBadgesVC)
                            break
                            
                        case CommonConstants.programReminderNotificationType: // go to home screen on first tab
                            if currentVC is MainScreenVC {
                                (currentVC as? MainScreenVC)?.selectedIndex = 0
                            } else {
                                let mainScreenVC = MainScreenVC.buildVC()
                                mainScreenVC.selectedIndex = 0
                                currentVC?.presentVC(mainScreenVC)
                            }
                            break
                            
                        case CommonConstants.deliverableReminderNotificationType, CommonConstants.deliverableAcceptedNotificationType, CommonConstants.deliverableRejectedNotificationType: // go to deliverable details screen
                            
                            let deliverableDetailsVC = DeliverableDetailsVC.buildVC(deliverableId: Int(userInfo["object_id"] as! String)!)
                            currentVC?.pushVC(deliverableDetailsVC)
                            break
                            
                        case CommonConstants.newCertificateNotificationType: // go to achievements screen
                            let achivementsVC = AchievementsVC.buildVC(isFromNotifications: true)
                            currentVC?.pushVC(achivementsVC)
                            break
                            
                        case CommonConstants.updatingTeamNotificationType: // go to team screen tab 3
                            if currentVC is MainScreenVC {
                                (currentVC as? MainScreenVC)?.selectedIndex = 2
                            } else {
                                let mainScreenVC = MainScreenVC.buildVC()
                                mainScreenVC.selectedIndex = 2
                                currentVC?.presentVC(mainScreenVC)
                            }
                            break
                            
                        case CommonConstants.sharingVideoNotificationType: // go to videos screen
                            let videosVC = VideosVC.buildVC()
                            currentVC?.pushVC(videosVC)
                            break
                            
                        case CommonConstants.feedbackNotificationType: // go to feedbacks screen
                            let weekDeliverableId = Int(userInfo["weekDeliverable_id"] as! String)
                            let feedbackVC = FeedbackVC.buildVC(weekDeliverableId: weekDeliverableId!)
                            currentVC?.pushVC(feedbackVC)
                            break
                            
                        default:
                            break
                        }
                    }
                }
            }
        }
    }
}

extension AppDelegate : MessagingDelegate {
    
    /**
     This method is called when FCM token is refreshed by Firebase and refresh the server with the new Token.
     - Parameter messaging: Instance of Messaging.
     - Parameter fcmToken: The new FCM token.
     */
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        sendFCMTokenToServer(fcmToken: fcmToken)
    }
    
    /**
     This method is called when receive new message from Fireabse.
     - Parameter messaging: Instance of Messaging.
     - Parameter remoteMessage: The new message object contains the received data.
     */
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
        
    }
}


