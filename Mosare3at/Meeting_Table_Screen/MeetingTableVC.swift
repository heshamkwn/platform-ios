//
//  MeetingTableVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/24/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit

class MeetingTableVC: BaseVC {
    
    var descriptions: [Description]!
    
    public class func buildVC(describtion: [Description]) -> MeetingTableVC {
        let vc = MeetingTableVC()
        vc.descriptions = describtion
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
