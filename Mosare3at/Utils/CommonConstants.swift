//
//  CommonConstants.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
class CommonConstants {
    public static let backgroundStatus = "background"
    public static let forgroundStatus = "forground"
    public static let BASE_URL = "http://test.accelerate.om:8080/"
    public static let IMAGES_BASE_URL = "http://test.accelerate.om:8080/media/download/"
    public static let DELIVERABLE_ACCEPTED = "accepted"
    public static let DELIVERABLE_DELIVERED = "delivered"
    public static let DELIVERABLE_NONE = "none"
    public static let DELIVERABLE_DELAYED = "delayed"
    public static let DELIVERABLE_REJECTED = "rejected"
    public static let NOTIFICATIONS_UPDATED = "Notifications_Updated"
    public static let SIDE_MENU_PROGRESS_UPDATED = "SIDE_MENU_PROGRESS_UPDATED"
    public static let ASCENDING = "ASCE"
    public static let DESCENDING = "DESC"
    public static let VIDEO_MEDIA_TYPE = "فيديو"
    public static let REPORT_MEDIA_TYPE = "تقرير"
    public static let VISUAL_SHOW_MEDIA_TYPE = "عرض مرئي"
    public static let PLAN_SHOW_MEDIA_TYPE =  "خطة"
    public static let MAP_SHOW_MEDIA_TYPE = "خارطة"
    public static let PROJECT_REPORT_SHOW_MEDIA_TYPE = "تقرير المشروع"
    public static let MEETING_MEDIA_TYPE = "اجتماع"
    public static let AGREEMENT_MEDIA_TYPE = "ميثاق"
    
    public static let weekSummaryNotificationType = "week_summary"
    public static let newPointsNotificationType = "new_points"
    public static let newBadgeNotificationType = "new_badge"
    public static let programReminderNotificationType = "program_reminder"
    public static let deliverableReminderNotificationType = "deliverable_reminder"
    public static let newCertificateNotificationType = "new_certificate"
    public static let updatingTeamNotificationType = "updating_team"
    public static let sharingVideoNotificationType = "sharing_video"
    public static let deliverableAcceptedNotificationType = "deliverable_accepted"
    public static let deliverableRejectedNotificationType = "deliverable_rejected"
    public static let feedbackNotificationType = "feedback"
    
    public static let TA_ROLE = "ROLE_TA"
    public static let USER_ROLE = "ROLE_USER"
    
}
