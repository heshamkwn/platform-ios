//
//  Injector.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/2/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class Injector {
    public class func provideLoginPresenter() -> LoginPresenter {
        return LoginPresenter(repository: Injector.provideLoginRepository())
    }
    
    public class func provideLoginRepository() -> LoginRepository {
        return LoginRepository()
    }
    
    public class func provideForgetPasswordPresenter() -> ForgetPasswordPresenter {
        return ForgetPasswordPresenter(repository: Injector.provideForgetPasswordRepository())
    }
    
    public class func provideForgetPasswordRepository() -> ForgetPasswordRepository {
        return ForgetPasswordRepository()
    }
    
    public class func provideAvatarPresenter() -> AvatarPresenter {
        return AvatarPresenter(repository: Injector.provideAvatarRepository())
    }
    
    public class func provideAvatarRepository() -> AvatarRepository {
        return AvatarRepository()
    }
    
    public class func provideProgramRepository() -> ProgramRepository{
        return ProgramRepository()
    }
    
    public class func provideProgramPresenter() -> ProgramPresenter {
        return ProgramPresenter(repository: Injector.provideProgramRepository())
    }
    
    public class func provideProjectPagerPresenter() -> ProjectPagerPresenter {
        return ProjectPagerPresenter(repository: Injector.provideProjectPageRepository())
    }
    
    public class func provideProjectPageRepository() -> ProjectPageRepository {
        return ProjectPageRepository()
    }
    
    public class func provideWeekDetailsPresenter() -> WeekDetailsPresenter {
        return WeekDetailsPresenter(repository: Injector.provideWeekDetailsRepository())
    }
    
    public class func provideWeekDetailsRepository() -> WeekDetailsRepository {
        return WeekDetailsRepository()
    }
    
    public class func provideWeekVisionRepository() -> WeekVisionRepository {
        return WeekVisionRepository()
    }
    
    public class func provideWeekVisionPresenter() -> WeekVisionPresenter {
        return WeekVisionPresenter(repository: Injector.provideWeekVisionRepository())
    }
    
    public class func provideMilestonesRepository() -> MilestonesRepository {
        return MilestonesRepository()
    }
    
    public class func provideMilestonesPresenter() -> MilestonePresenter {
        return MilestonePresenter(repository: Injector.provideMilestonesRepository())
    }
    
    public class func provideNotificationsRepository() -> NotificationsRepository {
        return NotificationsRepository()
    }
    
    public class func provideNotificationsPresenter() -> NotificationsPresenter {
        return NotificationsPresenter(repository: Injector.provideNotificationsRepository())
    }
    
    public class func provideTeamRatingRepository() -> TeamRatingRepository {
        return TeamRatingRepository()
    }
    
    public class func provideTeamRatingPresenter() -> TeamRatingPresenter {
        return TeamRatingPresenter(repository: Injector.provideTeamRatingRepository())
    }
    
    public class func provideVideosRepository() -> VideosRepository {
        return VideosRepository()
    }
    
    public class func provideVideosPresenter() -> VideosPresenter {
        return VideosPresenter(repository: Injector.provideVideosRepository())
    }
    
    public class func provideTutorialRepository() -> TutorialRepository {
        return TutorialRepository()
    }
    
    public class func provideTutorialPresenter() -> TutorialPresenter {
        return TutorialPresenter(repository: Injector.provideTutorialRepository())
    }
    
    public class func provideDashboardRepository() -> DashboardRepository {
        return DashboardRepository()
    }
    
    public class func provideDashboardPresenter() -> DashboardPresenter {
        return DashboardPresenter(repository: Injector.provideDashboardRepository())
    }
    
    public class func provideMyProfileRepository() -> MyProfileRepository {
        return MyProfileRepository()
    }
    
    public class func provideMyProfilePresenter() -> MyProfilePresenter {
        return MyProfilePresenter(repository: Injector.provideMyProfileRepository())
    }
    
    public class func provideMemberDetailsRepository() -> MemberDetailsRepository {
        return MemberDetailsRepository()
    }
    
    public class func provideMemberDetailsPresenter() -> MemberDetailsPresenter {
        return MemberDetailsPresenter(repository: Injector.provideMemberDetailsRepository())
    }
    
    public class func provideProgramDetailsRepository() -> ProgramDetailsRepository {
        return ProgramDetailsRepository()
    }
    
    public class func provideProgramDetailsPresenter() -> ProgramDetailsPresenter {
        return ProgramDetailsPresenter(repository: Injector.provideProgramDetailsRepository())
    }
    
    public class func provideProjectDetailsRepository() -> ProjectDetailsRepository {
        return ProjectDetailsRepository()
    }
    
    public class func provideProjectDetailsPresenter() -> ProjectDetailsPresenter {
        return ProjectDetailsPresenter(repository: Injector.provideProjectDetailsRepository())
    }
    
    public class func provideTeamRepository() -> TeamRepository {
        return TeamRepository()
    }
    
    public class func provideTeamPresenter() -> TeamPresenter {
        return TeamPresenter(repository: Injector.provideTeamRepository())
    }
    
    public class func provideEditProfileRepository() -> EditProfileRepository {
        return EditProfileRepository()
    }
    
    public class func provideEditProfilePresenter() -> EditProfilePresenter {
        return EditProfilePresenter(repository: Injector.provideEditProfileRepository())
    }
    
    public class func provideDeliverableGradesRepository() -> DeliverableGradesRepository {
        return DeliverableGradesRepository()
    }
    
    public class func provideDeliverableGradesPresenter() -> DeliverableGradesPresenter {
        return DeliverableGradesPresenter(repository: Injector.provideDeliverableGradesRepository())
    }
    
    public class func provideFeedbackRepository() -> FeedbackRepository {
        return FeedbackRepository()
    }
    
    public class func provideFeedbackPresenter() -> FeedbackPresenter {
        return FeedbackPresenter(repository: provideFeedbackRepository())
    }
    
    public class func provideScheduleRepository() -> ScheduleRepository {
        return ScheduleRepository()
    }
    
    public class func provideSchedulePresenter() -> SchedulePresenter {
        return SchedulePresenter(repository: Injector.provideScheduleRepository())
    }
    
    public class func provideRegisterationRepository() -> RegisterationRepository {
        return RegisterationRepository()
    }
    
    public class func provideRegisterationPresenter() -> RegisterationPresenter {
        return RegisterationPresenter(repository: Injector.provideRegisterationRepository())
    }
    
    public class func provideDeliverableDetailsRepository() -> DeliverableDetailsRepository {
        return DeliverableDetailsRepository()
    }
    
    public class func provideDeliverableDetailsPresenter() -> DeliverableDetailsPresenter {
        return DeliverableDetailsPresenter(repository: Injector.provideDeliverableDetailsRepository())
    }
    
    public class func provideWeekSummaryRepository() -> WeekSummaryRepository {
        return WeekSummaryRepository()
    }
    
    public class func provideWeekSummaryPresenter() -> WeekSummaryPresenter {
        return WeekSummaryPresenter(repository: Injector.provideWeekSummaryRepository())
    }
    
    public class func provideNotificationsSettingsRepository() -> NotificationSettingsRepository {
        return NotificationSettingsRepository()
    }
    
    public class func provideNotificationsSettingsPresenter() -> NotifiationSettingsPresenter {
        return NotifiationSettingsPresenter(repository: Injector.provideNotificationsSettingsRepository())
    }
}
