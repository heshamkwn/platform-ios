//
//  Navigator.swift
//  Mosare3at
//
//  Created by Hesham Donia on 9/30/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public class Navigator {
    
    var navigationController: UINavigationController!
    
    public init(navController: UINavigationController) {
        self.navigationController = navController
    }
    
    public func navigateToLogin() {
        let vc = LoginVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToForgetPassword() {
        let vc = ForgetPasswordVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToTutorial() {
        let vc = TutorialPagerVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToChooseAvatar() {
        let vc = ChooseAvatarVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToJoinSuccess() {
        let vc = JoinSuccessVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToMainScreen() {
        let vc = MainScreenVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func naviagateToWaitingScreen() {
        let vc = WaitingVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToWeekDetailsScreen(screenTitle: String, weekTitle: String, week: Week, project: Project, isWorkingOn: Bool) {
        let vc = WeekDetailsVC.buildVC(screenTitle: screenTitle, weekTitle: weekTitle, week: week, project: project, isWorkingOn: isWorkingOn)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToWeekVisionScreen(weekMaterial: WeekMaterial, project: Project, week: Week) {
        let vc = WeekVisionVC.buildVC(weekMaterial: weekMaterial, project: project, week: week)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToMilestonesScreen(weekMaterial: WeekMaterial, project: Project, week: Week, clickedMilestone: Milestone, currentMilestoneIndex: Int) {
        let vc = MilestonesVC.buildVC(weekMaterial: weekMaterial, project: project, week: week, currentMilestone: clickedMilestone, currentMilestoneIndex: currentMilestoneIndex)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToDeliverableDetailsScreen(week: Week, deliverable: Deliverable) {
        let vc = DeliverableDetailsVC.buildVC(week: week, deliverable: deliverable)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToDeliverableDetailsScreen(deliverableId: Int) {
        let vc = DeliverableDetailsVC.buildVC(deliverableId: deliverableId)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToNotifications() {
        let vc = NotificationsVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToTeamRating(week: Week) {
        let vc = TeamRatingVC.buildVC(week: week)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToGameMethodology() {
        let vc = GameMethodologyVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToSettings() {
        let vc = SettingsVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToNotificationsSettings() {
        let vc = NewNotificationsSettingsVC.buildVC()//NotificationsSettingsVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToTerms() {
        let vc = TermsVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToVideos() {
        let vc = VideosVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToVideos(isFromProfile: Bool) {
        let vc = VideosVC.buildVC()
        vc.isFromProfile = isFromProfile
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToVideos(memberId: Int) {
        let vc = VideosVC.buildVC()
        vc.memberId = memberId
        vc.isFromProfile = true
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToMyProfile() {
        let vc = MyProfileVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToRegisteredPrograms(programs: [RegisteredProgram]) {
        let vc = RegisteredProgramsVC.buildVC(programs: programs)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToMemberDetails(member: TeamMember, isTeamMate: Bool) {
        let vc = MemberDetailsVC.buildVC(member: member, isTeamMate: isTeamMate)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToTeamDetails(team: Team) {
        let vc = TeamDetailsVC.buildVC(team: team)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToProgramDetails() {
        let vc = ProgramDetailsVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToProjectDetails(projectId: Int) {
        let vc = ProjectDetailsVC.buildVC(projectId: projectId)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToEditProfile() {
        let vc = EditProfileVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToEditPassword() {
        let vc = EditPasswordVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToEditMobile() {
        let vc = EditMobileNumberVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToDeliverablesGradesVC() {
        let vc = DeliverablesGradesVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToAchievements(achievements: [Achievement]) {
        let vc = AchievementsVC.buildVC(achivements: achievements)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToFeedbacks(weekDeliverable: WeekDeliverable) {
        let vc = FeedbackVC.buildVC(weekDeliverable: weekDeliverable)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToEditFeedback(feedback: Feedback, delegate: EditFeedbackDelegate) {
        let vc = EditFeedbackVC.buildVC(feedback: feedback, delegate: delegate)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToEditTeamInfo(teamPicUrl: String, teamId: Int) {
        let vc = EditTeamVC.buildVC(teamPicUrl: teamPicUrl, teamId: teamId)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToRegisterationVC() {
        let vc = RegistrationVC.buildVC()
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToMeetingVC(describtion: [Description]) {
        let vc = MeetingVC.buildVC(describtion: describtion)
        self.navigationController.pushViewController(vc, animated: true)
    }
    
    public func navigateToMeetingTableVC(describtion: [Description]) {
        let vc = MeetingTableVC.buildVC(describtion: describtion)
        self.navigationController.pushViewController(vc, animated: true)
    }
}
