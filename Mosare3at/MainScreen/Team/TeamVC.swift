//
//  TeamVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/14/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Localize_Swift
import SideMenu
import SwiftyUserDefaults
import AVKit
import EzPopup

class TeamVC: BaseVC, UISideMenuNavigationControllerDelegate {
    
    var layout: TeamLayout!
    var sideMenuVC: SideMenuVC!
    
    var presenter: TeamPresenter!
    var team: Team!
    var videos: [Video]!
    
    var project: Project!
    var weekDeliverables: [WeekDeliverable]!
    var selectedWeekIndex: Int!
    
    var isTeamMemberCalled: Bool = false
    
    var weekDeliverablesSecondTab: [WeekDeliverable]!
    var currentWeek: Week?
    static func buildVC() -> TeamVC {
        return TeamVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout = TeamLayout(superview: self.view, teamLayoutDelegate: self)
        layout.setupViews()
        if AppDelegate.instance.unreadNotificationsNumber > 0 {
            self.layout.topView.notificationsNumberLabel.isHidden = false
            self.layout.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: CommonConstants.NOTIFICATIONS_UPDATED),
                                               object: self,
                                               queue: OperationQueue.main,
                                               using: notificationsUpdated(noti:))
        
        if let dic = Defaults[.subscription] {
            currentWeek = Subscribtion.getInstance(dictionary: dic).week
        }
        
        presenter = Injector.provideTeamPresenter()
        presenter.setView(view: self)
        
        
    }
    
    
    
    func notificationsUpdated(noti: Notification) {
        self.layout.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuVC = UiHelpers.setupSideMenu(delegate: self, viewToPresent: self.layout.topView.backImageView, viewToEdge: self.view, sideMenuCellDelegate: self, sideMenuHeaderDelegate: self)
        
        let teamId = Defaults[.teamId]!
        presenter.getTeam(teamId: Int(teamId.components(separatedBy: "/")[teamId.components(separatedBy: "/").count - 1])!)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension TeamVC: SideMenuHeaderDelegate {
    func headerClicked() {
        sideMenuVC.closeSideMenu()
        self.navigator = Navigator(navController: self.navigationController!)
        self.navigator.navigateToMyProfile()
    }
}

extension TeamVC: SideMenuCellDelegate {
    func sideMenuItemSelected(index: Int) {
        sideMenuVC.closeSideMenu()
        self.navigator = Navigator(navController: self.navigationController!)
        switch index {
        case 0:
            self.navigator.navigateToMyProfile()
            break
            
        case 1:
            self.navigator.navigateToVideos()
            break
            
        case 2:
            self.navigator.navigateToGameMethodology()
            break
            
        case 3:
            self.navigator.navigateToTerms()
            break
            
        case 4:
            self.navigator.navigateToSettings()
            break
            
        default:
            break
        }
         print("team :: item \(index) clicked")
    }
}

extension TeamVC : TeamLayoutDelegate {
    func retry() {
        let teamId = Defaults[.teamId]!
        presenter.getTeam(teamId: Int(teamId.components(separatedBy: "/")[teamId.components(separatedBy: "/").count - 1])!)
    }
    
    func openSideMenu() {
        if Localize.currentLanguage() == "en" {
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        } else {
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    
    func goToNotificationsScreen() {
        self.navigator = Navigator(navController: self.navigationController!)
        self.navigator.navigateToNotifications()
    }
}

extension TeamVC: TeamView {
    
    
    func operationFailed(message: String) {
        self.view.makeToast(message, duration: 2, position: .center)
        
        for view in self.view.subviews {
            if !(view is TopView) {
                view.isHidden = true
            }
            
        }
        self.layout.showErrorViews()
    }
    
    func getTeamSuccess(team: Team) {
        self.team = team
        presenter.getAllTeam()
        
    }
    
    func getAllTeamsSuccess(teams: [Team]) {
        var count = 0
        for team in teams {
            if team.id == self.team.id {
                self.team.rank = count
                break
            }
            count = count + 1
        }
        
        presenter.getAllMembers()
    }
    
    func getAllMembersSuccess(members: [TeamMember]) {
        
        for count1 in 0...team.teamMembers.count {
            for count2 in 0...members.count {
                if team.teamMembers.get(at: count1)?.member.id == members.get(at: count2)?.member.id {
                    team.teamMembers.get(at: count1)?.rank = count2 + 1
                    break
                }
            }
        }
        
        presenter.getVideos(programId: nil, projectId: team.project.id, teamId: team.id, order: CommonConstants.ASCENDING, isFromProfile: nil)
    }
    
    func getVideosSuccess(videos: [Video]) {
        self.videos = videos
        presenter.getProject(projectId: Subscribtion.getInstance(dictionary: Defaults[.subscription]!).project!.id)        
    }
    
    func getProjectSuccess(project: Project) {
        self.project = project
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        for count in 0...self.project.weeks.count {
            if self.project.weeks.get(at: count)?.requestId == currentWeek?.requestId {
                self.selectedWeekIndex = count
                currentWeek?.endDate = self.project.weeks.get(at: count)?.endDate
                break
            }
        }
        
        let selectedWeek = project.weeks.get(at: self.selectedWeekIndex)!
        
        let teamId = Int(Defaults[.teamId]!.components(separatedBy: "/").get(at: Defaults[.teamId]!.components(separatedBy: "/").count - 1)!)!
        
        presenter.getWeekDeliverables(parameters: ["team":"\(teamId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
        
    }
    
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable]) {
        if let _ = self.weekDeliverables {
            self.weekDeliverables.append(contentsOf: weekDeliverables)
        } else {
            self.weekDeliverables = weekDeliverables
        }
        
        if !isTeamMemberCalled {
            
            weekDeliverablesSecondTab = weekDeliverables
            
            let selectedWeek = project.weeks.get(at: self.selectedWeekIndex)!
            let user = User.getInstance(dictionary: Defaults[.user]!)
             let teamMemberId = Int(Defaults[.teamMemberId]!.components(separatedBy: "/").get(at: Defaults[.teamMemberId]!.components(separatedBy: "/").count - 1)!)!
            
            presenter.getWeekDeliverables(parameters: ["teamMember":"\(teamMemberId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            
            isTeamMemberCalled = true
        } else {
            self.weekDeliverables = self.weekDeliverables.sorted(by: { $0.feedbackCounter > $1.feedbackCounter })
            
            self.layout.teamTableView.dataSource = self
            self.layout.teamTableView.delegate = self
            self.layout.teamTableView.reloadData()
            
            self.layout.teamTableView.beginUpdates()
            self.layout.teamTableView.endUpdates()
            
            for view in self.view.subviews {
                if !(view is TopView) {
                    view.isHidden = false
                }
                
            }
            self.layout.hideErrorViews()
        }
    }
    
    func getVideoLinkSuccess(videoLink: String, index: Int) {
        self.videos.get(at: index)?.videoLink = videoLink
        let videoURL = URL(string: videoLink)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
}

extension TeamVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = layout.teamTableView.dequeueReusableCell(withIdentifier: TeamCell.identifier, for: indexPath) as! TeamCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.team = team
        cell.videos = videos
        cell.project = project
        cell.weekDeliverables = weekDeliverables
        cell.weekDeliverablesSecondTab = weekDeliverablesSecondTab
        cell.selectedWeekIndex = selectedWeekIndex
        cell.currentWeek = currentWeek
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.cellForRow(at: indexPath) as? TeamCell
        
        if let _ = cell {
            var height:CGFloat = 0.0
            
            switch cell?.selection {
            case 0:
                height = cell?.teamTabView.getTabHeight() ?? 0
                break
                
            case 1:
                height = cell?.deliverablesTabView.getTabHeight() ?? 0
                break
                
            case 2:
                height = cell?.videosTabView.getTabHeight() ?? 0
                break
                
            case 3:
                cell?.weekDeliverables = weekDeliverables
                height = cell?.deliverablesGradesTabView.getTabHeight() ?? 0
                break
                
            default:
                break
            }
            
            return height + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 59)
        } else {
            
            return  UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 57) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) * CGFloat(team.teamMembers.count) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2)
        }
    }
}

extension TeamVC: TeamCellDelegate {
    func openDeliverableDetails(weekDeliverable: WeekDeliverable) {
        self.navigator.navigateToDeliverableDetailsScreen(week: weekDeliverable.week, deliverable: weekDeliverable.deliverable)
    }
    
    func openEditTeamInfo() {
        print("go to edit team info")
        self.navigator.navigateToEditTeamInfo(teamPicUrl: self.team.image, teamId: self.team.id)
    }
    
    func openMyProfile() {
        self.navigator.navigateToMyProfile()
    }
    
    func openMemberDetails(index: Int) {
        self.navigator.navigateToMemberDetails(member: self.team.teamMembers.get(at: index)!, isTeamMate: true)
    }
    
    func playVideo(index: Int) {
        if let link = self.videos.get(at: index)?.videoLink, !link.isEmpty {
            self.getVideoLinkSuccess(videoLink: link, index: index)
        } else {
            self.presenter.getVideoLinks(videoId: (self.videos.get(at: index)?.id)!, index: index)
        }
    }
    
    func refreshBelowView(selection: Int) {
        self.layout.teamTableView.beginUpdates()
        self.layout.teamTableView.endUpdates()
    }
    
    func showFilters() {
        print("show filters")
        let vc = DeliverablesGradesTabViewFilterVC.buildVC(weeks: project.weeks, delegate: self)
        vc.weeks = project.weeks
        vc.selectedWeekIndex = self.selectedWeekIndex
        let popupVC = PopupViewController(contentController: vc, popupWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), popupHeight: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 25))
        present(popupVC, animated: true)
    }
    
    func openFeedback(index: Int) {
        navigator.navigateToFeedbacks(weekDeliverable: self.weekDeliverables.get(at: index)!)
    }
}

extension TeamVC: DeliverableGradesTabViewFilterDelegate {
    func applyFilters(selectedWeekIndex: Int) {
        isTeamMemberCalled = false
        self.selectedWeekIndex = selectedWeekIndex
        self.weekDeliverables.removeAll()
        let selectedWeek = project.weeks.get(at: self.selectedWeekIndex)!
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        let teamMemberId = Int(Defaults[.teamMemberId]!.components(separatedBy: "/").get(at: Defaults[.teamMemberId]!.components(separatedBy: "/").count - 1)!)!
        
        presenter.getWeekDeliverables(parameters: ["teamMember":"\(teamMemberId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
        
//        refreshBelowView(selection: 0)
    }
    
    func updateTopTitle(title: String) {
//        refreshBelowView(selection: 0)
    }
}
