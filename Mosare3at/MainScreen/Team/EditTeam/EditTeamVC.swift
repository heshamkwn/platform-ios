//
//  EditTeamVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/24/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import AVKit

class EditTeamVC: BaseVC {

    var layout: EditTeamLayout!
    var presenter: TeamPresenter!
    
    var teamPicUrl: String!
    var teamId: Int!
    
    var uploadedImageUrl: String!
    var imagePicker = UIImagePickerController()
    
    public class func buildVC(teamPicUrl: String, teamId: Int) -> EditTeamVC {
        let vc = EditTeamVC()
        vc.teamPicUrl = teamPicUrl
        vc.teamId = teamId
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = EditTeamLayout(superview: self.view, editTeamLayoutDelegate: self)
        layout.setupViews()
        
        layout.teamPicUrl = teamPicUrl
        layout.teamNameField.delegate = self
        
        layout.populateData()
        
        presenter = Injector.provideTeamPresenter()
        presenter.setEditTeamView(view: self)
    }
    
    func chooseImageFromGallery() {
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .photoLibrary;
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    //access denied
                }
            })
        }
    }

}

extension EditTeamVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        picker.dismiss(animated: true, completion: { () -> Void in
            self.layout.teamPicImageview.image = image
            self.presenter.uploadImageToServer(image: self.layout.teamPicImageview.image!)
        })
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: { () -> Void in
            print("cancelled")
        })
    }
}

extension EditTeamVC: EditTeamView {
    func imageUploadedSuccess(imageName: String) {
        self.uploadedImageUrl = imageName
    }
    
    public func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    public func updateAvatarSuccess(message: String) {
        self.view.makeToast(message, duration: 2) {
            self.goBack()
        }
    }
    
    public func updateAvatarFailed(message: String) {
        self.view.makeToast(message)
    }
}

extension EditTeamVC: EditTeamLayoutDelegate {
    func chooseImage() {
        chooseImageFromGallery()
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateTeamData() {
        if (layout.teamNameField.text?.isEmpty)! {
            self.view.makeToast("emptyFieldsError".localized())
        } else if uploadedImageUrl == nil {
            presenter.uploadImageToServer(image: layout.teamPicImageview.image!)
        } else {
            presenter.updateTeamData(teamName: layout.teamNameField.text!, teamImageUrl: uploadedImageUrl, teamId: teamId)
        }
    }
    
    func retry() {
        
    }
}

extension EditTeamVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 20
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if newString.length <= maxLength {
            layout.numberOfCharactersLabel.text = "\(newString.length)/20"
        }
        
        return newString.length <= maxLength
    }
}
