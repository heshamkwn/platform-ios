//
//  EditTeamLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/24/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Material

public protocol EditTeamLayoutDelegate: BaseLayoutDelegate {
    func goBack()
    func updateTeamData()
    func chooseImage()
}

public class EditTeamLayout: BaseLayout {
    
    var editTeamLayoutDelegate: EditTeamLayoutDelegate!
    
    var topView: TopView = TopView()
    
    var teamPicUrl: String!
    
    init(superview: UIView, editTeamLayoutDelegate: EditTeamLayoutDelegate) {
        super.init(superview: superview, delegate: editTeamLayoutDelegate)
        self.editTeamLayoutDelegate = editTeamLayoutDelegate
    }
    
    lazy var teamPicImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var changeImageLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.darkRed
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "+"
        label.clipsToBounds = true
        label.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)/2
        label.addTapGesture(action: { (_) in
            self.editTeamLayoutDelegate.chooseImage()
        })
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var teamNameField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "enterEmail".localized())
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.returnKeyType = UIReturnKeyType.next
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var numberOfCharactersLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.AppColors.gray
        label.textAlignment = .center
        label.text = "0/20"
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var alertContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.layer.cornerRadius = 8
        view.addBorder(width: 2, color: .gray)
        return view
    }()
    
    lazy var alertImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "ic_alert")
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) / 2
        return imageView
    }()
    
    lazy var alertLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = .gray
        label.textAlignment = .right
        label.text = "teamMemberNotify".localized()
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var saveButton: RaisedButton = {
        let button = RaisedButton(title: "save".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.green
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 18)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.editTeamLayoutDelegate.updateTeamData()
        }
        return button
    }()
    
    public func setupViews() {
        let views = [topView, teamPicImageview, changeImageLabel, teamNameField, numberOfCharactersLabel, alertContainerView, alertLabel, alertImageview, saveButton]
        
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        teamPicImageview.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.leading.trailing.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 40))
        }
        
        changeImageLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 46.5))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 60))
        }
        
        teamNameField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(changeImageLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        numberOfCharactersLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(teamNameField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo("20/20".widthOfString(usingFont: numberOfCharactersLabel.font))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        alertContainerView.snp.makeConstraints { (maker) in
            maker.top.equalTo(numberOfCharactersLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12))
        }
        
        alertContainerView.addSubviews([alertImageview, alertLabel])
        
        alertImageview.snp.makeConstraints { (maker) in
            maker.leading.equalTo(alertContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.top.equalTo(alertContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
        
        alertLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(alertImageview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(alertContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
             maker.top.equalTo(alertContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
        
        saveButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(alertContainerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
        }
        
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "editTeamInfo".localized())
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")
        self.topView.logoImageView.isHidden = true
        self.topView.screenTitleLabel.isHidden = false
        self.topView.delegate = self
    }
    
    public func populateData() {
        teamPicImageview.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + teamPicUrl)!)
    }
}

extension EditTeamLayout: TopViewDelegate {
    public func goBack() {
        self.editTeamLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
