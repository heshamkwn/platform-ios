//
//  TeamCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Material

public protocol TeamCellDelegate: class {
    func playVideo(index: Int)
    func refreshBelowView(selection: Int)
    func openMyProfile()
    func openMemberDetails(index: Int)
    func openEditTeamInfo()
    func showFilters()
    func openFeedback(index: Int)
    func openDeliverableDetails(weekDeliverable: WeekDeliverable)
}

class TeamCell: UITableViewCell {

    static let identifier = "TeamCell"
    var superView: UIView!
    var selection: Int = 0
    var team: Team!
    var videos: [Video]!
    var delegate: TeamCellDelegate!
    
    var weekDeliverables: [WeekDeliverable]!
    
    var currentWeek: Week!
    
    var weekDeliverablesSecondTab: [WeekDeliverable]!
    
    var project: Project!
    var selectedWeekIndex: Int = 0
    
    lazy var mainTeamProfilePicView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "#000000")?.withAlphaComponent(0.5)
        return view
    }()
    
    lazy var mainTeamProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var editImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "edit_ic")
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            self.delegate.openEditTeamInfo()
        })
        return imageView
    }()
    
    lazy var teamPicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        return imageView
    }()
    
    lazy var teamNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var projectNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var myTeamLabel: UILabel = {
        let label = UILabel()
        label.text = "myTeam".localized()
        label.textColor = .white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var pointsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.alphaBlack
        view.clipsToBounds = true
        view.addBorder(width: 2, color: UIColor.AppColors.darkRed)
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)/2
        return view
    }()
    
    lazy var pointsTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "point".localized()
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var pointsValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var rankingView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.alphaBlack
        view.clipsToBounds = true
        view.addBorder(width: 2, color: UIColor.AppColors.darkRed)
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)/2
        return view
    }()
    
    lazy var rankingTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "rank".localized()
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var rankingValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var teamButton: RaisedButton = {
        let button = RaisedButton(title: "team".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.teamLine.isHidden = false
            self.deliverablesLine.isHidden = true
            self.videosLine.isHidden = true
            self.deliverablesGradesLine.isHidden = true
            self.selection = 0
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 0)
            
        }
        return button
    }()
    
    lazy var teamLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = false
        return view
    }()
    
    lazy var deliverablesButton: RaisedButton = {
        let button = RaisedButton(title: "outputs".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.isUserInteractionEnabled = true
        button.addTapGesture { recognizer in
            self.teamLine.isHidden = true
            self.deliverablesLine.isHidden = false
            self.videosLine.isHidden = true
            self.deliverablesGradesLine.isHidden = true
            self.selection = 1
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 1)
            
        }
        return button
    }()
    
    lazy var deliverablesLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = true
        return view
    }()
    
    lazy var videosButton: RaisedButton = {
        let button = RaisedButton(title: "videos".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.isUserInteractionEnabled = true
        button.addTapGesture { recognizer in
            self.teamLine.isHidden = true
            self.deliverablesLine.isHidden = true
            self.videosLine.isHidden = false
            self.deliverablesGradesLine.isHidden = true
            self.selection = 2
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 2)
        }
        return button
    }()
    
    lazy var videosLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = true
        return view
    }()
    
    lazy var deliverablesGradesButton: RaisedButton = {
        let button = RaisedButton(title: "deliverablesGrades".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.isUserInteractionEnabled = true
        button.addTapGesture { recognizer in
            self.teamLine.isHidden = true
            self.deliverablesLine.isHidden = true
            self.videosLine.isHidden = true
            self.deliverablesGradesLine.isHidden = false
            self.selection = 3
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 3)
            
        }
        return button
    }()
    
    lazy var deliverablesGradesLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = true
        return view
    }()
    
    var teamTabView: TeamTabView!
    var deliverablesTabView: DeliverablesTabView!
    var videosTabView: VideosTabView!
    var deliverablesGradesTabView: DeliverablesGradesTabView!
    
    public func setupViews() {
        
        if teamTabView == nil {
            
            teamTabView = TeamTabView.getInstance(team: team, delegate: self)
            teamTabView.setupViews()
            teamTabView.populateData()
            
            deliverablesTabView = DeliverablesTabView.getInstance(weekDeliverables: weekDeliverablesSecondTab)
            deliverablesTabView.currentWeek = currentWeek
            deliverablesTabView.delegate = self
            deliverablesTabView.setupViews()
            deliverablesTabView.populateData()
            deliverablesTabView.isHidden = true
            
            videosTabView = VideosTabView.getInstance(videos: videos, delegate: self)
            videosTabView.setupViews()
            videosTabView.populateData()
            videosTabView.isHidden = true
            
            deliverablesGradesTabView = DeliverablesGradesTabView.getInstance(project: project, weekDeliverables: weekDeliverables)
            deliverablesGradesTabView.project = project
            deliverablesGradesTabView.weekDeliverables = weekDeliverables
            deliverablesGradesTabView.delegate = self
            deliverablesGradesTabView.selectedWeekIndex = selectedWeekIndex
            deliverablesGradesTabView.setupViews()
            deliverablesGradesTabView.populateData()
            deliverablesGradesTabView.isHidden = true
        } else {
            
            deliverablesGradesTabView.weekDeliverables.removeAll()
            deliverablesGradesTabView.weekDeliverables = weekDeliverables
            deliverablesGradesTabView.selectedWeekIndex = selectedWeekIndex
            deliverablesGradesTabView.setupViews()
            deliverablesGradesTabView.populateData()
        }
        
        let views = [mainTeamProfilePicView, editImageView, mainTeamProfilePicImageView, teamPicImageView, teamNameLabel, projectNameLabel, myTeamLabel, pointsView, pointsTitleLabel, pointsValueLabel, rankingView, rankingTitleLabel, rankingValueLabel, teamButton, teamLine, deliverablesLine, deliverablesButton, videosButton, videosLine, deliverablesGradesButton, deliverablesGradesLine, teamTabView, deliverablesTabView, videosTabView, deliverablesGradesTabView]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views as! [UIView])
        
        mainTeamProfilePicImageView.snp.makeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 50))
        }
        
        mainTeamProfilePicView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(mainTeamProfilePicImageView)
        }
        
        editImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(mainTeamProfilePicImageView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(mainTeamProfilePicImageView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        teamPicImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 20))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 42))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        teamNameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(teamPicImageView)
            maker.leading.equalTo(teamPicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 70))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        projectNameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(teamNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.trailing.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        myTeamLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(projectNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.trailing.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        pointsView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView.snp.centerX).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(myTeamLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
        }
        
        pointsView.addSubviews([pointsTitleLabel, pointsValueLabel])
        
        pointsValueLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(pointsView)
            maker.top.equalTo(pointsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        pointsTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(pointsView)
            maker.top.equalTo(pointsValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        rankingView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView.snp.centerX).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(myTeamLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
        }
        
        rankingView.addSubviews([rankingTitleLabel, rankingValueLabel])
        
        rankingValueLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(rankingView)
            maker.top.equalTo(rankingView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        rankingTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(rankingView)
            maker.top.equalTo(rankingValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        teamButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(mainTeamProfilePicImageView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(superView)
        }
        
        teamLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(teamButton)
            maker.bottom.equalTo(teamButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        deliverablesButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(mainTeamProfilePicImageView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(teamButton.snp.trailing)
        }
        
        deliverablesLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(deliverablesButton)
            maker.bottom.equalTo(deliverablesButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        videosButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(mainTeamProfilePicImageView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(deliverablesButton.snp.trailing)
        }
        
        videosLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(videosButton)
            maker.bottom.equalTo(videosButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        deliverablesGradesButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(mainTeamProfilePicImageView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(videosButton.snp.trailing)
        }
        
        deliverablesGradesLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(deliverablesGradesButton)
            maker.bottom.equalTo(deliverablesGradesButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        teamTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(teamButton.snp.bottom)
            maker.height.equalTo(teamTabView.getTabHeight())
        }
        
        deliverablesTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(teamButton.snp.bottom)
            maker.height.equalTo(deliverablesTabView.getTabHeight())
        }
        
        videosTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(teamButton.snp.bottom)
            maker.height.equalTo(videosTabView.getTabHeight())
        }
        
        deliverablesGradesTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(teamButton.snp.bottom)
            maker.height.equalTo(deliverablesGradesTabView.getTabHeight())
        }
        
        
        
        superView.bringSubviewToFront(mainTeamProfilePicView)
        superView.bringSubviewToFront(teamPicImageView)
        superView.bringSubviewToFront(teamNameLabel)
        superView.bringSubviewToFront(projectNameLabel)
        superView.bringSubviewToFront(myTeamLabel)
        superView.bringSubviewToFront(pointsView)
        superView.bringSubviewToFront(pointsTitleLabel)
        superView.bringSubviewToFront(pointsValueLabel)
        superView.bringSubviewToFront(rankingView)
        superView.bringSubviewToFront(rankingTitleLabel)
        superView.bringSubviewToFront(rankingValueLabel)
        superView.bringSubviewToFront(editImageView)
        
        superView.bringSubviewToFront(teamLine)
        superView.bringSubviewToFront(deliverablesLine)
        superView.bringSubviewToFront(videosLine)
        superView.bringSubviewToFront(deliverablesGradesLine)
        
        superView.bringSubviewToFront(teamTabView)
        superView.bringSubviewToFront(deliverablesTabView)
        superView.bringSubviewToFront(videosTabView)
        superView.bringSubviewToFront(deliverablesGradesTabView)
    }
    
    public func populateData() {
        self.mainTeamProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + team.image)!)
        self.projectNameLabel.text = team.project.title
        self.teamNameLabel.text = team.name
        if let rank = team.rank {
            self.rankingValueLabel.text = "\(rank)"
        } else {
           self.rankingValueLabel.text = ""
        }
        
        if let points = team.points {
            self.pointsValueLabel.text = "\(points)"
        } else {
            self.pointsValueLabel.text = ""
        }
        
        
    }
    
    fileprivate func changeBelowViewResource() {
        
        switch self.selection {
        case 0:
            teamTabView.isHidden = false
            deliverablesTabView.isHidden = true
            videosTabView.isHidden = true
            deliverablesGradesTabView.isHidden = true
            superView.bringSubviewToFront(teamTabView)
            break
            
        case 1:
            teamTabView.isHidden = true
            deliverablesTabView.isHidden = false
            videosTabView.isHidden = true
            deliverablesGradesTabView.isHidden = true
            superView.bringSubviewToFront(deliverablesTabView)
            break
            
        case 2:
            teamTabView.isHidden = true
            deliverablesTabView.isHidden = true
            videosTabView.isHidden = false
            deliverablesGradesTabView.isHidden = true
            superView.bringSubviewToFront(videosTabView)
            break
            
        case 3:
            teamTabView.isHidden = true
            deliverablesTabView.isHidden = true
            videosTabView.isHidden = true
            deliverablesGradesTabView.isHidden = false
            superView.bringSubviewToFront(deliverablesGradesTabView)
            break
            
        default:
            break
        }
    }
}

extension TeamCell: TeamTabViewDelegate {
    func openMyProfile() {
        self.delegate.openMyProfile()
    }
    
    func openMemberDetails(index: Int) {
        self.delegate.openMemberDetails(index: index)
    }
}

extension TeamCell: VideosTabViewDelegate {
    func playVideo(index: Int) {
        self.delegate.playVideo(index: index)
    }
}

extension TeamCell: DeliverablesGradesTabViewDelegate {
    func showFilters() {
        self.delegate.showFilters()
    }
    
    func openFeedback(index: Int) {
        self.delegate.openFeedback(index: index)
    }
}

extension TeamCell: DeliverablesTabViewDelegate {
    func openDeliverableDetails(weekDeliverable: WeekDeliverable) {
        self.delegate.openDeliverableDetails(weekDeliverable: weekDeliverable)
    }
}


