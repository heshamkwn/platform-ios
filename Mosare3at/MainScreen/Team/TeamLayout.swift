//
//  ProgramLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/14/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import UIView_draggable

public protocol TeamLayoutDelegate: BaseLayoutDelegate {
    func openSideMenu()
    func goToNotificationsScreen()
}

public class TeamLayout : BaseLayout {
    
    public var teamLayoutDelegate: TeamLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, teamLayoutDelegate: TeamLayoutDelegate) {
        super.init(superview: superview, delegate: teamLayoutDelegate)
        self.teamLayoutDelegate = teamLayoutDelegate
    }
    
    lazy var teamTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = true
        tableView.register(TeamCell.self, forCellReuseIdentifier: TeamCell.identifier)
        return tableView
    }()
    
    lazy var slackImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.image = UIImage(named: "slack_ic")
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3)/2
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            UiHelpers.openSlack()
        })
        return imageView
    }()
    
    lazy var slackView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        view.clipsToBounds = true
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)/2
        view.enableDragging()
        return view
    }()
    
    public func setupViews() {
        
        let views = [topView, teamTableView, slackImageview, slackView]
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.teamTableView.snp.makeConstraints { maker in
            maker.leading.trailing.bottom.equalTo(superview)
            maker.top.equalTo(self.topView.snp.bottom)
        }
        
        self.slackView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.bottom.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10) * -1)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        self.slackImageview.snp.makeConstraints { (maker) in
            maker.center.equalTo(slackView)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        superview.bringSubviewToFront(slackView)
        superview.bringSubviewToFront(slackImageview)
        
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "")
        self.topView.backImageView.image = UIImage(named: "side_menu")
        self.topView.logoImageView.isHidden = false
        self.topView.screenTitleLabel.isHidden = true
        self.topView.delegate = self
        self.topView.notificationsImageView.isHidden = false
        if AppDelegate.instance.unreadNotificationsNumber > 0 {
            self.topView.notificationsNumberLabel.isHidden = false
            self.topView.notificationsNumberLabel.layer.masksToBounds = true
            self.topView.notificationsNumberLabel.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1)
            self.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
        }
        self.topView.backImageView.addTapGesture(action: nil)
        self.topView.backImageView.addTapGesture { (_) in
            self.teamLayoutDelegate.openSideMenu()
        }
    }
}

extension TeamLayout : TopViewDelegate {
    public func goBack() {
        // open side menu here
    }
    
    public func goToNotifications() {
        // go to notifications screen
        self.teamLayoutDelegate.goToNotificationsScreen()
    }
}
