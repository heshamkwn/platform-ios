//
//  File.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol TeamView : class {
    func operationFailed(message: String)
    func getTeamSuccess(team: Team)
    func getAllTeamsSuccess(teams: [Team])
    func getAllMembersSuccess(members: [TeamMember])
    func getVideosSuccess(videos: [Video])
    func getVideoLinkSuccess(videoLink: String, index: Int)
    func getProjectSuccess(project: Project)
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable])
}

public protocol EditTeamView: class {
    func operationFailed(message: String)
    func updateAvatarSuccess(message: String)
    func updateAvatarFailed(message: String)
    func imageUploadedSuccess(imageName: String) 
}

public class TeamPresenter {
    fileprivate weak var teamView : TeamView?
    fileprivate let teamRepository : TeamRepository
    fileprivate weak var editTeamView : EditTeamView?
    
    init(repository: TeamRepository) {
        self.teamRepository = repository
        self.teamRepository.setDelegate(delegate: self)
        self.teamRepository.setEditTeamDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : TeamView) {
        teamView = view
    }
    
    func setEditTeamView(view: EditTeamView) {
        editTeamView = view
    }
}

extension TeamPresenter {
    public func getTeam(teamId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getTeam(teamId: teamId)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getAllTeam() {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getAllTeams(page: nil)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getAllMembers() {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getAllMembers(page: nil)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getVideos(programId: Int?, projectId: Int?, teamId: Int?, order: String, isFromProfile: Bool?) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getVideos(programId: programId, projectId: projectId, teamId: teamId, order: order, isFromProfile: isFromProfile)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getVideoLinks(videoId: Int, index: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getVideoLinks(videoId: videoId, index: index)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getProject(projectId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getProject(projectId: projectId)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getWeekDeliverables(parameters: [String:String], token: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.getWeekDeliverables(token: token, parameters: parameters, page: nil)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func updateTeamData(teamName: String, teamImageUrl: String, teamId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.updateTeamData(imageName: teamImageUrl, teamName: teamName, teamId: teamId)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func uploadImageToServer(image: UIImage) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.teamRepository.uploadTeamImageToServer(imageData: image.jpeg(.lowest)!)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension TeamPresenter: TeamPresenterDelegate {
    public func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable]) {
        self.teamView?.getWeekDeliverableSuccess(weekDeliverables: weekDeliverables)
    }
    
    public func getProjectSuccess(project: Project) {
        self.teamView?.getProjectSuccess(project: project)
    }
    
    public func operationFailed(message: String) {
        UiHelpers.hideLoader()
        self.teamView?.operationFailed(message: message)
    }
    
    public func getTeamSuccess(team: Team) {
        UiHelpers.hideLoader()
        self.teamView?.getTeamSuccess(team: team)
    }
    
    public func getAllTeamsSuccess(teams: [Team]) {
        UiHelpers.hideLoader()
        self.teamView?.getAllTeamsSuccess(teams: teams)
    }
    
    public func getAllMembersSuccess(members: [TeamMember]) {
        UiHelpers.hideLoader()
        self.teamView?.getAllMembersSuccess(members: members)
    }
    
    public func getVideosSuccess(videos: [Video]) {
        UiHelpers.hideLoader()
        self.teamView?.getVideosSuccess(videos: videos)
    }
    
    public func getVideoLinkSuccess(videoLink: String, index: Int) {
        UiHelpers.hideLoader()
        self.teamView?.getVideoLinkSuccess(videoLink: videoLink, index: index)
    }
}

extension TeamPresenter: EditTeamPresenterDelegate {
    public func imageUploadedSuccess(imageName: String) {
        UiHelpers.hideLoader()
        self.editTeamView?.imageUploadedSuccess(imageName: imageName)
    }
    
    public func updateTeamSuccess(message: String) {
        UiHelpers.hideLoader()
        self.editTeamView?.updateAvatarSuccess(message: message)
    }
    
    public func updateAvatarFailed(message: String) {
        UiHelpers.hideLoader()
        self.editTeamView?.updateAvatarFailed(message: message)
    }
}
