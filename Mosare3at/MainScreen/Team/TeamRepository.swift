//
//  TeamRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol TeamPresenterDelegate {
    func operationFailed(message: String)
    func getTeamSuccess(team: Team)
    func getAllTeamsSuccess(teams: [Team])
    func getAllMembersSuccess(members: [TeamMember])
    func getVideosSuccess(videos: [Video])
    func getVideoLinkSuccess(videoLink: String, index: Int)
    func getProjectSuccess(project: Project)
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable])
}

public protocol EditTeamPresenterDelegate {
    func operationFailed(message: String)
    func updateAvatarFailed(message: String)
    func updateTeamSuccess(message: String)
    func imageUploadedSuccess(imageName: String)
}

public class TeamRepository {
    //getSubscribtions
    var delegate: TeamPresenterDelegate!
     var allTeams = [Team]()
    var allMembers = [TeamMember]()
    var weekDeliverables = [WeekDeliverable]()
    
    var editTeamDelegate: EditTeamPresenterDelegate!
    
    public func setDelegate(delegate: TeamPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func setEditTeamDelegate(delegate: EditTeamPresenterDelegate) {
        editTeamDelegate = delegate
    }

    func getTeam(teamId: Int) {
        let token = User.getInstance(dictionary: Defaults[.user]!).token
        let headers = ["X-AUTH-TOKEN" : token!]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "teams/\(teamId)")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let team = Team.getInstance(dictionary: json)
                        self.delegate.getTeamSuccess(team: team)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getAllTeams(page: String?) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        let projectId = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).project.requestId.components(separatedBy: "/")[Subscribtion.getInstance(dictionary: Defaults[.subscription]!).project.requestId.components(separatedBy: "/").count - 1]
        
        var parameters = ["project": projectId]
        
        if let _ = page {
            parameters["page"] = page
        }
        
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "teams")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let membersJsonArray = json["hydra:member"] as? [Dictionary<String,AnyObject>]
                        
                        for dic in membersJsonArray! {
                            let team = Team.getInstance(dictionary: dic)
                            self.allTeams.append(team)
                        }
                        
                        let hydraView = json["hydra:view"] as? Dictionary<String,AnyObject>
                        if let next = hydraView!["hydra:next"] as? String {
                            let page = next.components(separatedBy: "=")[next.components(separatedBy: "=").count - 1]
                            self.getAllTeams(page: page)
                        } else {
                            self.delegate.getAllTeamsSuccess(teams: self.allTeams)
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getAllMembers(page: String?) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        let programId = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/")[Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").count - 1]
        
        var parameters = ["program": programId, "status" : "active"] as [String : Any]
        if let _ = page {
            parameters["page"] = page
        }
        
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "program_points")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        
                        let membersJsonArray = json["hydra:member"] as? [Dictionary<String,AnyObject>]
                        
                        for dic in membersJsonArray! {
                            let member = TeamMember.getInstance(dictionary: dic)
                            self.allMembers.append(member)
                        }
                        
                        let hydraView = json["hydra:view"] as? Dictionary<String,AnyObject>
                        if let next = hydraView!["hydra:next"] as? String {
                            let page = next.components(separatedBy: "=")[next.components(separatedBy: "=").count - 1]
                            self.getAllMembers(page: page)
                        } else {
                            self.delegate.getAllMembersSuccess(members: self.allMembers)
                        }
                        
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getVideos(programId: Int?, projectId: Int?, teamId: Int?, order: String, isFromProfile: Bool?) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        let userId = User.getInstance(dictionary: Defaults[.user]!).id!
        
        var parameters = ["order" : order] as [String : Any]
        
        var defaultUrl = "aws/get-videos-by-filter"
        var method: HTTPMethod = .post
        var parameterEncoding: ParameterEncoding = JSONEncoding.default
        
        
        if let _ = isFromProfile {
            defaultUrl = "aws/get-user-videos/\(userId)/\(programId!)"
            method = .get
            parameterEncoding = URLEncoding.default
            
        } else {
            if let _ = programId {
                parameters["program"] =  programId
            }
            
            if let _ = projectId {
                parameters["project"] =  projectId
            }
            
            if let _ = teamId {
                parameters["team"] =  teamId
            }
        }
        
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + defaultUrl)!, method: method, parameters: parameters, encoding: parameterEncoding, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                
                if let _ = isFromProfile {
                    if let json = response.result.value as? Dictionary<String,AnyObject> {
                        if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                            var videos: [Video] = [Video]()
                            let jsonArray = json["videos"] as? [Dictionary<String,AnyObject>]
                            
                            for dic in jsonArray! {
                                let video = Video()
                                video.id = dic["id"] as? Int
                                let uploadDateDic = dic["uploadedDate"] as? Dictionary<String,AnyObject>
                                video.uploadDate = uploadDateDic!["date"] as? String
                                if let thumbnailsDic = dic["thumbnail"], thumbnailsDic is [Dictionary<String, Any>] {
                                    var thumbnails: [Thumbnail] = []
                                    for dic in thumbnailsDic as! [Dictionary<String, Any>] {
                                        thumbnails.append(Thumbnail.getInstance(dictionary: dic))
                                    }
                                    video.thumbnails =  thumbnails
                                }
                                video.description = dic["description"] as? String
                                if let ownerDic = dic["owner"], ownerDic is  Dictionary<String, Any> {
                                    video.owner =  User.getInstance(dictionary: ownerDic as! Dictionary<String, Any>)
                                }
                                videos.append(video)
                            }
                            self.delegate.getVideosSuccess(videos: videos)
                            
                        } else {
                            self.delegate.operationFailed(message: "somethingWentWrong".localized())
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    if let json = response.result.value as? [[Dictionary<String,AnyObject>]] {
                        if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                            
                            let jsonArray = json[0]
                            var videos: [Video] = [Video]()
                            for dic in jsonArray {
                                let video = Video()
                                video.id = dic["id"] as? Int
                                let uploadDateDic = dic["uploadedDate"] as? Dictionary<String,AnyObject>
                                video.uploadDate = uploadDateDic!["date"] as? String
                                if let thumbnailsDic = dic["thumbnail"], thumbnailsDic is [Dictionary<String, Any>] {
                                    var thumbnails: [Thumbnail] = []
                                    for dic in thumbnailsDic as! [Dictionary<String, Any>] {
                                        thumbnails.append(Thumbnail.getInstance(dictionary: dic))
                                    }
                                    video.thumbnails =  thumbnails
                                }
                                video.description = dic["description"] as? String
                                if let ownerDic = dic["owner"], ownerDic is  Dictionary<String, Any> {
                                    video.owner =  User.getInstance(dictionary: ownerDic as! Dictionary<String, Any>)
                                }
                                videos.append(video)
                            }
                            self.delegate.getVideosSuccess(videos: videos)
                        } else {
                            self.delegate.operationFailed(message: "somethingWentWrong".localized())
                        }
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getVideoLinks(videoId: Int, index: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        let params = ["video":videoId]
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "aws/download")!, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let jsonArray = json["results"] as? [Dictionary<String,AnyObject>]
                        let dic = jsonArray![1]
                        
                        self.delegate.getVideoLinkSuccess(videoLink: dic["360"] as! String, index: index)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getProject(projectId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        //"Content-Type" : "application/json",
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "projects/\(projectId)")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let project = Project.getInstance(dictionary: json)
                        self.delegate.getProjectSuccess(project: project)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    func getWeekDeliverables(token: String, parameters: [String : String], page: String?) {
        let headers = ["X-AUTH-TOKEN" : token]
        var myParams = parameters
        if let _ = page {
            myParams["page"] = page
        } else {
            self.weekDeliverables.removeAll()
        }
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_deliverables")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let weekDeliverableResponse = WeekDeliverableResponse.getInstance(dictionary: json)
                        for weekDeliverable in weekDeliverableResponse.hydraMember {
                            self.weekDeliverables.append(weekDeliverable)
                        }
                        let hydraView = json["hydra:view"] as? Dictionary<String,String>
                        if let next = hydraView!["hydra:next"] as? String {
                            let page = next.components(separatedBy: "=")[next.components(separatedBy: "=").count - 1]
                            self.getWeekDeliverables(token: token, parameters: myParams, page: page)
                        } else {
                            self.delegate.getWeekDeliverableSuccess(weekDeliverables: self.weekDeliverables)
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func updateTeamData(imageName: String, teamName: String, teamId: Int) {
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        let headers = ["Content-Type" : "application/json", "X-AUTH-TOKEN" : "\(user.token!)"]
        let parameters = ["name" : teamName, "image" : imageName]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "teams/\(teamId)")!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
//                    self.editTeamDelegate.updateAvatarSuccess(message: "updateAvatarSuccess".localized())
                    // update the user's profile image using the uploaded one
                    self.editTeamDelegate.updateTeamSuccess(message: "teamUpdatedSuccess".localized())
                } else {
                    self.editTeamDelegate.updateAvatarFailed(message: "editTeamFailed".localized())
                }
            } else {
                self.editTeamDelegate.updateAvatarFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func uploadTeamImageToServer(imageData: Data) {
        let fileName = "image.png"
        let mimeType = "image/png"
        UiHelpers.showLoader()
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(imageData, withName: "file", fileName: fileName, mimeType: mimeType)
        }, to:"\(CommonConstants.BASE_URL)media/upload", method:.post, headers : nil, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let dictionary = response.value as? Dictionary<String, Any> {
                        let name = dictionary["name"] as! String
                        //self.updateTeamData(imageName: name, teamName: teamName, teamId: teamId)
                        self.editTeamDelegate.imageUploadedSuccess(imageName: name)
                    } else {
                        self.editTeamDelegate.updateAvatarFailed(message: "uploadImageError".localized()) //
                    }
                }
            case .failure(let encodingError):
                self.editTeamDelegate.updateAvatarFailed(message: encodingError as! String)
            }
        })
    }
}
