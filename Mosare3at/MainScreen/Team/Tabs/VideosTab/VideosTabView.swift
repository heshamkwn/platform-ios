//
//  VideosTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol VideosTabViewDelegate: class {
    func playVideo(index: Int)
}

class VideosTabView: UIView {

    
    var delegate: VideosTabViewDelegate!
    
    var videos: [Video]!
    
    public class func getInstance(videos: [Video], delegate: VideosTabViewDelegate) -> VideosTabView {
        let view = VideosTabView()
        view.delegate = delegate
        view.videos = videos
        return view
    }
    
    lazy var videosTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.register(VideoCell.self, forCellReuseIdentifier: VideoCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [videosTableView]
        
        self.addSubviews(views)
        
        self.videosTableView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(calculateTableViewHeight())
        }
    }
    
    public func populateData() {
        videosTableView.dataSource = self
        videosTableView.delegate = self
        videosTableView.reloadData()
    }
    
    fileprivate func calculateTableViewHeight() -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65) * CGFloat(videos.count)
    }
    
    public func getTabHeight() -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65) * CGFloat(videos.count) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2)
    }

}

extension VideosTabView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:VideoCell = self.videosTableView.dequeueReusableCell(withIdentifier: VideoCell.identifier, for: indexPath) as! VideoCell
        cell.selectionStyle = .none
        cell.setupViews()
        cell.video = self.videos.get(at: indexPath.row)!
        cell.delegate = self
        cell.populateData()
        cell.index = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65)
    }
}

extension VideosTabView: VideoCellDelegate {
    func playVideo(index: Int) {
        self.delegate.playVideo(index: index)
    }
}
