//
//  TeamMemberCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwipeCellKit

public protocol TeamMemberCellDelegate: class {
    func openMyProfile()
    func openMemberDetails(index: Int)
}

class TeamMemberCell: SwipeTableViewCell {

    static let identifier = "TeamMemberCell"
    var superView: UIView!
    
    
    var myDelegate: TeamMemberCellDelegate!
    var index: Int!
    var member: TeamMember!
    var teacherAssistant: TeacherAssistant!
    var isMe: Bool!
    
    lazy var userProfileImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)/2
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var userPositionLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var swipToConnectLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.gray
        label.textAlignment = .left
        label.text = "swipToConnect".localized()
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    public func setupViews() {
        let views = [userProfileImageview, userNameLabel, userPositionLabel, swipToConnectLabel]
        
        self.superView = self.contentView
        self.superView.addSubviews(views)
        
        self.userProfileImageview.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.centerY.equalTo(superView)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
        }
        
        self.userNameLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(userProfileImageview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            if let _ = teacherAssistant {
                let name = teacherAssistant.firstname + " " + teacherAssistant.lastname
                
                maker.width.equalTo(name.widthOfString(usingFont: userNameLabel.font))
            } else {
                let name = member.member.firstname + " " + member.member.lastname
                
                maker.width.equalTo(name.widthOfString(usingFont: userNameLabel.font))
            }
            
        }
        
        self.userPositionLabel.snp.remakeConstraints { (maker) in
            maker.top.equalTo(userNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(userProfileImageview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40))
        }
        
        self.swipToConnectLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.centerY.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40))
        }
    }
    
    public func populateData() {
        if let _ = teacherAssistant {
            userProfileImageview.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + teacherAssistant.profilePic)!, placeholderImage: UIImage(named: "no_badge"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: false, completion: nil)
            userNameLabel.text = teacherAssistant.firstname + " " + teacherAssistant.lastname
            userPositionLabel.text = "teacherAssistant".localized()
        } else {
            userProfileImageview.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + member.member.profilePic)!, placeholderImage: UIImage(named: "no_badge"), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: false, completion: nil)
            userNameLabel.text = member.member.firstname + " " + member.member.lastname
            userPositionLabel.text = member.nickname
        }
        
        if let _ = isMe, isMe {
            swipToConnectLabel.isHidden = true
            self.superView.backgroundColor = UIColor.AppColors.gray
            self.superView.addTapGesture { (_) in
                self.myDelegate.openMyProfile()
            }
        } else {
            swipToConnectLabel.isHidden = false
            self.superView.backgroundColor = UIColor.clear
            self.superView.addTapGesture { (_) in
                if let _ = self.index {
                    self.myDelegate.openMemberDetails(index: self.index)
                }
            }
        }
    }
}
