//
//  TeamTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SwipeCellKit

public protocol TeamTabViewDelegate: class {
    func openMyProfile()
    func openMemberDetails(index: Int)
}

class TeamTabView: UIView {

    var team: Team!
    let user = User.getInstance(dictionary: Defaults[.user]!)
    var delegate: TeamTabViewDelegate!
    
    
    public class func getInstance(team: Team, delegate: TeamTabViewDelegate) -> TeamTabView {
        let view: TeamTabView = TeamTabView()
        view.team = team
        view.delegate = delegate
        return view
    }
    
    lazy var membersTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.isScrollEnabled = false
        tableView.register(TeamMemberCell.self, forCellReuseIdentifier: TeamMemberCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [membersTableView]
        
        self.addSubviews(views)
        
        self.membersTableView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(calculateTableViewHeight())
        }
    }
    
    public func populateData() {
        membersTableView.dataSource = self
        membersTableView.delegate = self
        membersTableView.reloadData()
    }
    
    fileprivate func calculateTableViewHeight() -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) * CGFloat(team.teamMembers.count)
    }
    
    public func getTabHeight() -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) * CGFloat(team.teamMembers.count) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2)
    }
}

extension TeamTabView: UITableViewDataSource, UITableViewDelegate, SwipeTableViewCellDelegate {
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .left else { return nil }
        
        let contactAction = SwipeAction(style: .default, title: "openSlack".localized()) { action, indexPath in
            UiHelpers.openSlack()
        }
        // customize the action appearance
        contactAction.image = nil//UIImage(named: "slack_ic_2")//slack_ic
        
        return [contactAction]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return team.teamMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeamMemberCell = self.membersTableView.dequeueReusableCell(withIdentifier: TeamMemberCell.identifier, for: indexPath) as! TeamMemberCell
        
        if indexPath.row == 0 {
            cell.teacherAssistant = team.teacherAssistant
        } else {
            cell.member = team.teamMembers.get(at: indexPath.row - 1)!
            cell.index = indexPath.row - 1
            cell.isMe = team.teamMembers.get(at: indexPath.row - 1)!.member.id == user.id
        }
        
        cell.delegate = self
        cell.myDelegate = self
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12)
    }
}

extension TeamTabView: TeamMemberCellDelegate {
    func openMyProfile() {
        self.delegate.openMyProfile()
    }
    
    func openMemberDetails(index: Int) {
        self.delegate.openMemberDetails(index: index)
    }
}
