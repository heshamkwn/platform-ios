//
//  DeliverablesGradesTopCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/23/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol DeliverablesGradesTopCellDelegate: class {
    func showFilters()
}

class DeliverablesGradesTopCell: UITableViewCell {

    var superView: UIView!
    public static let identifier = "DeliverablesGradesTopCell"

    var delegate: DeliverablesGradesTopCellDelegate!
    
    var weekTitle: String!
    var weekDate: String!
    
    lazy var weekTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    lazy var weekDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = .gray
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var filterImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "filter_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.darkRed
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.delegate.showFilters()
        })
        return imageView
    }()
    
    lazy var filterView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.addTapGesture(action: { (recognizer) in
            self.delegate.showFilters()
        })
        return view
    }()
    
    public func setupViews() {
        let views = [weekTitleLabel, weekDateLabel, filterImageView, filterView]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        self.filterImageView.snp.makeConstraints { maker in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3) * -1)
            
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        filterView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(filterImageView)
        }
        
        weekTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(filterImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        weekDateLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(weekTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(filterImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
    }
    
    public func populateData() {
        weekTitleLabel.text = weekTitle
        weekDateLabel.text = weekDate
    }
}
