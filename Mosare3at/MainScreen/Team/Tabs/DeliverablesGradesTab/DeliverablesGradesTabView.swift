//
//  DeliverablesGradesTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

public protocol DeliverablesGradesTabViewDelegate: class {
    func showFilters()
    func openFeedback(index: Int)
}

class DeliverablesGradesTabView: UIView {
    
    var delegate: DeliverablesGradesTabViewDelegate!
    var weekDeliverables :[WeekDeliverable]!
    var project: Project!
    var selectedWeekIndex: Int!

    public class func getInstance(project: Project, weekDeliverables :[WeekDeliverable]) -> DeliverablesGradesTabView {
        let view = DeliverablesGradesTabView()
        view.project = project
        view.weekDeliverables = weekDeliverables
        return DeliverablesGradesTabView()
    }
    
    lazy var deliverablesGradesTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.isScrollEnabled = false
        tableView.register(DeliverablesGradesTopCell.self, forCellReuseIdentifier: DeliverablesGradesTopCell.identifier)
        tableView.register(DeliverablesGradesCell.self, forCellReuseIdentifier: DeliverablesGradesCell.identifier)
        return tableView
    }()

    public func setupViews() {
        let views = [deliverablesGradesTableView]
        
        self.addSubviews(views)
        
        self.deliverablesGradesTableView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(calculateTableViewHeight())
        }
    }
    
    public func populateData() {
        deliverablesGradesTableView.dataSource = self
        deliverablesGradesTableView.delegate = self
        deliverablesGradesTableView.reloadData()
    }
    
    fileprivate func calculateTableViewHeight() -> CGFloat {
        
        var height: CGFloat = 0.0
        
        for weekDeliverable in weekDeliverables {
            height = height + weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: AppFont.font(type: .Bold, size: 16)) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
        }
        
        return height + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
    }
    
    public func getTabHeight() -> CGFloat {
        
        return calculateTableViewHeight() + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
    }
    
    func getWeekTitle(startDate: String, endDate: String) -> String{
        let finalStartDate = startDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (startDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 1)
        
        let finalEndDate = endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 1)
        
        let month = endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 2)
        
        let monthString = UiHelpers.getMonthName(monthNumber: Int(month!)!)
        
        return "\(finalStartDate!) - \(finalEndDate!) \(monthString)"
    }
}

extension DeliverablesGradesTabView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDeliverables.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell: DeliverablesGradesTopCell = deliverablesGradesTableView.dequeueReusableCell(withIdentifier: DeliverablesGradesTopCell.identifier, for: indexPath) as! DeliverablesGradesTopCell
            cell.selectionStyle = .none
            cell.delegate = self
            cell.weekTitle = "week".localized() + " " + UiHelpers.getIndexName(index: selectedWeekIndex)
            cell.weekDate = getWeekTitle(startDate: (project.weeks.get(at: selectedWeekIndex)?.startDate)!, endDate: (project.weeks.get(at: selectedWeekIndex)?.endDate)!)
            cell.setupViews()
            cell.populateData()
            return cell
        } else {
            let cell: DeliverablesGradesCell = deliverablesGradesTableView.dequeueReusableCell(withIdentifier: DeliverablesGradesCell.identifier, for: indexPath) as! DeliverablesGradesCell
            cell.selectionStyle = .none
            cell.weekDeliverable = self.weekDeliverables.get(at: indexPath.row - 1)!
            cell.index = indexPath.row - 1
            cell.delegate = self
            cell.setupViews()
            cell.populateData()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12)
        } else {
            let weekDeliverable = weekDeliverables.get(at: indexPath.row - 1)!
            return weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: AppFont.font(type: .Bold, size: 16)) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
        }
    }
}

extension DeliverablesGradesTabView: DeliverablesGradesCellDelegate {
    func goToDeliverableDetails(index: Int) {
        
    }
    
    func openFeedback(index: Int) {
        self.delegate.openFeedback(index: index)
    }
}

extension DeliverablesGradesTabView: DeliverablesGradesTopCellDelegate {
    func showFilters() {
        self.delegate.showFilters()
    }
}
