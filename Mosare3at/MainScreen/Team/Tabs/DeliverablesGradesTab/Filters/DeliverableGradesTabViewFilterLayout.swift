//
//  DeliverablesGradesTabViewFilterLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/24/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import DropDown
import UIKit
import Material

public protocol DeliverableGradesTabViewFilterLayoutDelegate: BaseLayoutDelegate {
    func applyFilters()
    func cancel()
    func setupWeeksDropDown()
}

public class DeliverableGradesTabViewFilterLayout: BaseLayout {
    var deliverableGradesTabViewDelegate: DeliverableGradesTabViewFilterLayoutDelegate!
    
    init(superview: UIView, delegate: DeliverableGradesTabViewFilterLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.deliverableGradesTabViewDelegate = delegate
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "filter".localized()
        label.font = AppFont.font(type: .Bold, size: 24)
        return label
    }()
    
    lazy var weeksLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.text = "time".localized()
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var weeksTextField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "")
        field.returnKeyType = UIReturnKeyType.next
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.textColor = UIColor.AppColors.gray
        field.isEnabled = false
        field.textAlignment = .center
        return field
    }()
    
    lazy var weekView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.addTapGesture(action: { (recognizer) in
            self.deliverableGradesTabViewDelegate.setupWeeksDropDown()
        })
        return view
    }()
    
    lazy var weekArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "down_arrow")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var applyLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "apply".localized()
        label.addTapGesture(action: { (_) in
            self.deliverableGradesTabViewDelegate.applyFilters()
        })
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var cancelLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "cancel".localized()
        label.font = AppFont.font(type: .Bold, size: 14)
        label.addTapGesture(action: { (_) in
            self.deliverableGradesTabViewDelegate.cancel()
        })
        return label
    }()
    
    lazy var verticalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()
    
    lazy var horizontalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()

     public func setupViews() {
            let views = [titleLabel, weekView, weeksLabel, weeksTextField, weekArrowImageView, applyLabel, cancelLabel, verticalView, horizontalView]
        
        self.superview.addSubviews(views)
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.centerX.equalTo(superview)
        }
        
        weeksLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(titleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        weekView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(titleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        weekArrowImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(weeksLabel).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 0.5))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        weeksTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(weeksLabel)
            maker.leading.equalTo(weeksLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(weekArrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        
        
        weekView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(weeksTextField)
        }
        
        horizontalView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.height.equalTo(2)
            maker.top.equalTo(weeksLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        applyLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.top.equalTo(horizontalView.snp.bottom)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49))
        }
        
        verticalView.snp.makeConstraints { (maker) in
            maker.top.equalTo(horizontalView.snp.bottom)
            maker.leading.equalTo(applyLabel.snp.trailing)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(2)
        }
        
        cancelLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(verticalView.snp.trailing)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.top.equalTo(horizontalView.snp.bottom)
            maker.trailing.equalTo(superview)
        }
    }
    
    public func populateData(weekText: String) {
        weeksTextField.text = weekText
    }
}
