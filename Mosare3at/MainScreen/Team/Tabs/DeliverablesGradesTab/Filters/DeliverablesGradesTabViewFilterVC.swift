//
//  DeliverablesGradesTabViewFilterVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/24/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import DropDown
import SwiftyUserDefaults

public protocol DeliverableGradesTabViewFilterDelegate: class {
    func applyFilters(selectedWeekIndex: Int)
    func updateTopTitle(title: String)
}

class DeliverablesGradesTabViewFilterVC: BaseVC {

    var delegate: DeliverableGradesTabViewFilterDelegate!
    
    var weeks: [Week]!
    var selectedWeekIndex: Int!
    
    var layout: DeliverableGradesTabViewFilterLayout!
    
    public static func buildVC(weeks: [Week], delegate: DeliverableGradesTabViewFilterDelegate) -> DeliverablesGradesTabViewFilterVC {
        let vc = DeliverablesGradesTabViewFilterVC()
        vc.weeks = weeks
        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = DeliverableGradesTabViewFilterLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        layout.populateData(weekText: weeks.get(at: selectedWeekIndex)!.title)
    }

}

extension DeliverablesGradesTabViewFilterVC: DeliverableGradesTabViewFilterLayoutDelegate {
    func retry() {
        
    }
    
    func applyFilters() {
        self.dismiss(animated: true) {
            self.delegate.applyFilters(selectedWeekIndex: self.selectedWeekIndex)
        }
    }
    
    func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupWeeksDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.weeksTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.weeksTextField.bounds.height)
        
        var dataSource = [String]()
        for week in weeks {
            dataSource.append(week.title)
        }
        dropDown.dataSource = dataSource
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedWeekIndex = index
            self.layout.weeksTextField.text = self.weeks.get(at: self.selectedWeekIndex)?.title
            
            let startDate = self.weeks.get(at: self.selectedWeekIndex)!.startDate
            let endDate = self.weeks.get(at: self.selectedWeekIndex)!.endDate
            
            self.populateTitle(startDate: startDate!, endDate: endDate!)
            
            
        }
        dropDown.show()
    }
    
    func populateTitle(startDate: String, endDate: String) {
        let finalStartDate = startDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (startDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 1)
        
        let finalEndDate = endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 1)
        
        let month = endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 2)
        
        let monthString = UiHelpers.getMonthName(monthNumber: Int(month!)!)
        
        self.delegate.updateTopTitle(title: "\("week".localized()) \(UiHelpers.getIndexName(index: self.weeks.get(at: selectedWeekIndex)!.weight)) - \(finalStartDate!) - \(finalEndDate!) \(monthString)")
    }
}
