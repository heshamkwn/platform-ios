//
//  DeliverablesTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/9/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Material
import SwiftyUserDefaults

public protocol DeliverablesTabViewDelegate: class {
    func openDeliverableDetails(weekDeliverable: WeekDeliverable)
}

class DeliverablesTabView: UIView {
    
    
    var weekDeliverables: [WeekDeliverable]!
    var currentWeek: Week!
    
    var delegate: DeliverablesTabViewDelegate!
    
    public class func getInstance(weekDeliverables: [WeekDeliverable]) -> DeliverablesTabView {
        let view = DeliverablesTabView()
        view.weekDeliverables = weekDeliverables
        return view
    }
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var deliverablesLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .right
        label.text = "teamOutputs".localized()
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var progressBar: UIProgressView = {
        
        let progressBar = UIProgressView()
        progressBar.tintColor = UIColor.AppColors.darkRed
        return progressBar
        
    }()
    
    lazy var deliverablesPercentTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .right
        label.text = "deliverablesPercentThisWeek".localized()
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    lazy var deliverablesPercentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .left
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var deadLineLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.darkGray
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    lazy var weekDeliverablesView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var weekDeliverablesTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.isScrollEnabled = false
        tableView.register(DeliverableCell.self, forCellReuseIdentifier: DeliverableCell.identifier)
        return tableView
    }()
    
    lazy var acceptedStatusLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.gray
        label.textAlignment = .right
        label.text = "accepted".localized()
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var acceptedStatusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "accepted_status")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var deliveredStatusLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.gray
        label.textAlignment = .right
        label.text = "delivered".localized()
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var deliveredStatusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "delivered_status_icon")
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var notDeliveredStatusLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.gray
        label.textAlignment = .right
        label.text = "notDelivered".localized()
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var notDeliveredStatusImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "not_delivered_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.contentMode = .scaleToFill
        return imageView
    }()

    public func setupViews() {
        let views = [containerView, deliverablesLabel, progressBar, deliverablesPercentTitleLabel, deliverablesPercentLabel, deadLineLabel, weekDeliverablesView, weekDeliverablesTableView, acceptedStatusLabel, acceptedStatusImageView, deliveredStatusLabel, deliveredStatusImageView, notDeliveredStatusLabel, notDeliveredStatusImageView]

        self.addSubviews(views)
        
        containerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 33))
        }
        
        containerView.addSubviews([deliverablesLabel, progressBar, deliverablesPercentTitleLabel, deliverablesPercentLabel, deadLineLabel])
        
        deliverablesLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        progressBar.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(deliverablesLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(4)
        }
        
        deliverablesPercentTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(progressBar.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 60))
        }
        
        deliverablesPercentLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(progressBar.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
        }
        
        deadLineLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(deliverablesPercentLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        weekDeliverablesView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(containerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 4) + (UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10) * CGFloat(weekDeliverables.count)))
        }
        
        weekDeliverablesView.addSubview(weekDeliverablesTableView)
        
        weekDeliverablesTableView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(weekDeliverablesView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(weekDeliverablesView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(weekDeliverablesView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10) * CGFloat(weekDeliverables.count))
        }
        
        acceptedStatusImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weekDeliverablesView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        acceptedStatusLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(acceptedStatusImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weekDeliverablesView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 26))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        deliveredStatusImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(acceptedStatusLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weekDeliverablesView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        deliveredStatusLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(deliveredStatusImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weekDeliverablesView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 26))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        notDeliveredStatusImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(deliveredStatusLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weekDeliverablesView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        notDeliveredStatusLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(notDeliveredStatusImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weekDeliverablesView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 26))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
    }
    
    public func populateData() {
        if Singleton.getInstance().sideMenuDoneTasksCount != nil && Singleton.getInstance().sideMenuDoneTasksCount > 0 && Singleton.getInstance().sideMenuTotalTasksCount != nil && Singleton.getInstance().sideMenuTotalTasksCount > 0 {
            
            progressBar.setProgress(Float(Singleton.getInstance().sideMenuDoneTasksCount) / Float(Singleton.getInstance().sideMenuTotalTasksCount), animated: true)
            
            deliverablesPercentLabel.text = "\(Int(Float(Singleton.getInstance().sideMenuDoneTasksCount) / Float(Singleton.getInstance().sideMenuTotalTasksCount) * 100)) %"
        }
        
        let endDate = currentWeek?.endDate
        let date = (endDate?.components(separatedBy: "T")[0])!
        let day = Int(date.components(separatedBy: "-")[2])!
        let monthName = UiHelpers.getMonthName(monthNumber: Int(date.components(separatedBy: "-")[1])!)
        let hours = (endDate?.components(separatedBy: "T")[1].components(separatedBy: ":")[0])!
        let minutes = (endDate?.components(separatedBy: "T")[1].components(separatedBy: ":")[1])!
        let time = "\(hours):\(minutes)"
        
        deadLineLabel.text = "\("deadline".localized()):  \(day) \(monthName) \(time)"
        
        weekDeliverablesTableView.dataSource = self
        weekDeliverablesTableView.delegate = self
        weekDeliverablesTableView.reloadData()
    }
    
    public func getTabHeight() -> CGFloat {
        
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 32) + (UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10) * CGFloat(weekDeliverables.count)) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 16)
    }
}

extension DeliverablesTabView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDeliverables.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DeliverableCell = weekDeliverablesTableView.dequeueReusableCell(withIdentifier: DeliverableCell.identifier, for: indexPath) as! DeliverableCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.index = indexPath.row
        cell.weekDeliverable = self.weekDeliverables.get(at: indexPath.row)!
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
    }
}

extension DeliverablesTabView: DeliverableCellDelegate {
    func openWeekDeliverableDetails(index: Int) {
        self.delegate.openDeliverableDetails(weekDeliverable: self.weekDeliverables.get(at: index)!)
    }
}
