//
//  DeliverableCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/26/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol DeliverableCellDelegate: class {
    func openWeekDeliverableDetails(index: Int)
}

class DeliverableCell: UITableViewCell {

    var superView: UIView!
    public static let identifier = "DeliverableCell"
    
    public var weekDeliverable: WeekDeliverable!
    var index: Int!
    var delegate: DeliverableCellDelegate!
    
    lazy var statusImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var weekDeliverableTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var arrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "left_arrow")
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    public func setupViews() {
        let views = [statusImageview, weekDeliverableTitleLabel, arrowImageView]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        self.superView.addTapGesture { (_) in
            self.delegate.openWeekDeliverableDetails(index: self.index)
        }
        
        statusImageview.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.centerY.equalTo(superView)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.arrowImageView.snp.remakeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.centerY.equalTo(superView)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        weekDeliverableTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(statusImageview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(arrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.centerY.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            
        }
    }
    
    public func populateData() {
        switch weekDeliverable.status {
        case "none", "rejected":
            statusImageview.image = UIImage(named: "not_delivered_icon")
            break
            
        case "delivered":
            statusImageview.image = UIImage(named: "delivered_status_icon")
            break
            
        default:
            statusImageview.image = UIImage(named: "accepted_status")?.withRenderingMode(.alwaysTemplate)
            statusImageview.tintColor = UIColor.AppColors.gray
            break
        }
        
        weekDeliverableTitleLabel.text = weekDeliverable.deliverable.title
    }

}
