//
//  ProgramLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/14/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol ScheduleLayoutDelegate: BaseLayoutDelegate {
    func openSideMenu()
    func goToNotificationsScreen()
}

public class ScheduleLayout : BaseLayout {
    
    public var scheduleLayoutDelegate: ScheduleLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, scheduleLayoutDelegate: ScheduleLayoutDelegate) {
        super.init(superview: superview, delegate: scheduleLayoutDelegate)
        self.scheduleLayoutDelegate = scheduleLayoutDelegate
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "weekTasks".localized()
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var scheduleTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.isScrollEnabled = true
        tableView.register(ScheduleCell.self, forCellReuseIdentifier: ScheduleCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        
        let views = [topView, titleLabel, scheduleTableView]
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.top.equalTo(topView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
        
        scheduleTableView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.bottom.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "")
        self.topView.backImageView.image = UIImage(named: "side_menu")
        self.topView.notificationsImageView.isHidden = false
        self.topView.logoImageView.isHidden = false
        self.topView.screenTitleLabel.isHidden = true
        self.topView.delegate = self
        if AppDelegate.instance.unreadNotificationsNumber > 0 {
            self.topView.notificationsNumberLabel.isHidden = false
            self.topView.notificationsNumberLabel.layer.masksToBounds = true
            self.topView.notificationsNumberLabel.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1)
            self.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
        }
        self.topView.backImageView.addTapGesture(action: nil)
        self.topView.backImageView.addTapGesture { (_) in
            self.scheduleLayoutDelegate.openSideMenu()
        }
    }
}

extension ScheduleLayout : TopViewDelegate {
    public func goToNotifications() {
        // go to notifications screen
        self.scheduleLayoutDelegate.goToNotificationsScreen()
    }
    
    public func goBack() {
        // open side menu here
    }
    
    
}
