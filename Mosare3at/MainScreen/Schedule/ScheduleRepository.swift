//
//  ScheduleRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/29/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol SchedulePresenterDelegate: class {
    func operationFailed(message: String)
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable])
    func weekDeliverableUpdatedSuccess(weekDeliverable: WeekDeliverable)
    func getWeekSuccess(week: Week)
}


public class ScheduleRepository {
    var delegate: SchedulePresenterDelegate!
    
    var weekDeliverables = [WeekDeliverable]()
    
    public func setDelegate(delegate: SchedulePresenterDelegate) {
        self.delegate = delegate
    }
    
    func getWeek(token: String, weekID: Int) {
        let headers = ["X-AUTH-TOKEN" : token]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "weeks/\(weekID)")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let week = Week.getInstance(dictionary: json)
                        self.delegate.getWeekSuccess(week: week)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    func getWeekDeliverables(token: String, parameters: [String : String], page: String?) {
        let headers = ["X-AUTH-TOKEN" : token]
        var myParams = parameters
        if let _ = page {
            myParams["page"] = page
        } else {
            self.weekDeliverables.removeAll()
        }
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_deliverables")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let weekDeliverableResponse = WeekDeliverableResponse.getInstance(dictionary: json)
                        for weekDeliverable in weekDeliverableResponse.hydraMember {
                            self.weekDeliverables.append(weekDeliverable)
                        }
                        let hydraView = json["hydra:view"] as? Dictionary<String,String>
                        if let next = hydraView!["hydra:next"] as? String {
                            let page = next.components(separatedBy: "=")[next.components(separatedBy: "=").count - 1]
                            self.getWeekDeliverables(token: token, parameters: myParams, page: page)
                        } else {
                            self.delegate.getWeekDeliverableSuccess(weekDeliverables: self.weekDeliverables)
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    func updateWeekDeliverableStatus(token: String, weekDeliverable: WeekDeliverable, status: String) {
        let headers = ["X-AUTH-TOKEN" : token]
        let parameters = ["status" : status]
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_deliverables/\(weekDeliverable.id!)")!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON {
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.weekDeliverableUpdatedSuccess(weekDeliverable: weekDeliverable)
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
