//
//  ScheduleCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/29/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol ScheduleCellDelegate {
    func openDeliverableDetails(weekDeliverable: WeekDeliverable)
    func updateDeliverableStatus(weekDeliverable: WeekDeliverable, status: Bool)
    func openMeetingScreen(describtion: [Description])
}

class ScheduleCell: UITableViewCell {

    static let identifier = "ScheduleCell"
    var superView: UIView!
    
    var delegate: ScheduleCellDelegate!
    
    var weekDeliverable: WeekDeliverable!
    

    lazy var estimatedTimeLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var deliverableTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var mediaTypeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) / 2
        return imageView
    }()
    
    lazy var deliverableMediaTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 12)
        return label
    }()
    
    lazy var belongsToImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    lazy var belongsToLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 12)
        return label
    }()
    
    lazy var dateImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "alarm")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 12)
        return label
    }()
    
    lazy var checkImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "not_checked")
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            if imageView.image == UIImage(named: "not_checked") {
                self.weekDeliverable.status = "delivered"
                self.delegate.updateDeliverableStatus(weekDeliverable: self.weekDeliverable, status: true)
            }
        })
        return imageView
    }()
    
    func setupViews() {
        let views = [estimatedTimeLabel, deliverableTitleLabel, mediaTypeImageView, deliverableMediaTypeLabel, belongsToLabel, belongsToImageView, dateLabel, dateImageView, checkImageView]
        
        self.superView = self.contentView
        
        self.superView.addTapGesture { (_) in
            self.delegate.openMeetingScreen(describtion: self.weekDeliverable.deliverable.descriptions)
//            if self.weekDeliverable.deliverable.type == "meeting" {
//                // go to meeting screen
//                self.delegate.openMeetingScreen()
//            } else {
//                self.delegate.openDeliverableDetails(weekDeliverable: self.weekDeliverable)
//            }
            
        }
        
        self.superView.addSubviews(views)
        
        estimatedTimeLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        checkImageView.snp.makeConstraints { (maker) in
             maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(estimatedTimeLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        deliverableTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(estimatedTimeLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), font: deliverableTitleLabel.font))
            
            maker.trailing.equalTo(checkImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        mediaTypeImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        deliverableMediaTypeLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(mediaTypeImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
        }
        
        belongsToImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
            
            maker.leading.equalTo(deliverableMediaTypeLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        belongsToLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(belongsToImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 30))
        }
        
        dateImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
            maker.leading.equalTo(belongsToLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        dateLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(dateImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
        }
    }
    
    func populateData() {
       
        if weekDeliverable.deliverable.estimated == 0 {
            estimatedTimeLabel.text = "estimated".localized()
        } else {
            estimatedTimeLabel.text = "\(weekDeliverable.deliverable.estimated!) \("estimatedMinutes".localized())"
        }
        
        deliverableTitleLabel.text = weekDeliverable.deliverable.title
        
        switch self.weekDeliverable.deliverable.typeTitle {
            
        case CommonConstants.VIDEO_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.VIDEO_MEDIA_TYPE
            break
            
        case CommonConstants.REPORT_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.blue
            self.deliverableMediaTypeLabel.text = CommonConstants.REPORT_MEDIA_TYPE
            break
            
        case CommonConstants.VISUAL_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.primaryColor
            self.deliverableMediaTypeLabel.text = CommonConstants.VISUAL_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.PLAN_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.PLAN_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.MAP_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.MAP_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.PROJECT_REPORT_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.PROJECT_REPORT_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.MEETING_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.yellow
            self.deliverableMediaTypeLabel.text = CommonConstants.MEETING_MEDIA_TYPE
            break
            
        case CommonConstants.AGREEMENT_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.green
            self.deliverableMediaTypeLabel.text = CommonConstants.AGREEMENT_MEDIA_TYPE
            break
            
        default:
            break
        }
        
        if self.weekDeliverable.deliverable.collective {
            belongsToImageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
            belongsToImageView.tintColor = UIColor.AppColors.gray
            belongsToLabel.text = "team".localized()
        } else {
            belongsToImageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
            belongsToImageView.tintColor = UIColor.AppColors.gray
            belongsToLabel.text = "personal".localized()
        }
        
        dateLabel.text = weekDeliverable.week.endDate?.components(separatedBy: "T")[0]
        
        switch weekDeliverable.status {
        case "accepted", "delivered":
            checkImageView.image = UIImage(named: "checked")
            break
            
        default:
            checkImageView.image = UIImage(named: "not_checked")
            break
        }
        
        dateLabel.text = weekDeliverable.week.endDate?.components(separatedBy: "T")[0]
    }
}
