//
//  SchedulePresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/29/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation


public protocol ScheduleView: class {
    func operationFailed(message: String)
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable])
    func weekDeliverableUpdatedSuccess(weekDeliverable: WeekDeliverable)
    func getWeekSuccess(week: Week)
}

public class SchedulePresenter {
    fileprivate weak var scheduleView : ScheduleView?
    fileprivate let scheduleRepository : ScheduleRepository
    
    init(repository: ScheduleRepository) {
        self.scheduleRepository = repository
        self.scheduleRepository.setDelegate(delegate: self)
    }
    
    func setView(view : ScheduleView) {
        scheduleView = view
    }
}

extension SchedulePresenter {
    public func getWeekDeliverables(parameters: [String:String], token: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.scheduleRepository.getWeekDeliverables(token: token, parameters: parameters, page: nil)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func updateWeekDeliverableStatus(weekDeliverable: WeekDeliverable, token: String, status: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.scheduleRepository.updateWeekDeliverableStatus(token: token, weekDeliverable: weekDeliverable, status: status)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getWeek(token: String, weekID: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.scheduleRepository.getWeek(token: token, weekID: weekID)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension SchedulePresenter: SchedulePresenterDelegate {
   
    public func operationFailed(message: String) {
        UiHelpers.hideLoader()
        self.scheduleView?.operationFailed(message: message)
    }
    
    public func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable]) {
        UiHelpers.hideLoader()
       self.scheduleView?.getWeekDeliverableSuccess(weekDeliverables: weekDeliverables)
    }
    
    public func weekDeliverableUpdatedSuccess(weekDeliverable: WeekDeliverable) {
        UiHelpers.hideLoader()
        self.scheduleView?.weekDeliverableUpdatedSuccess(weekDeliverable: weekDeliverable)
    }
    
    public func getWeekSuccess(week: Week) {
        UiHelpers.hideLoader()
        self.scheduleView?.getWeekSuccess(week: week)
    }
}
