
//
//  ScheduleVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/14/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Localize_Swift
import SideMenu
import SwiftyUserDefaults

class ScheduleVC: BaseVC, UISideMenuNavigationControllerDelegate {
    
    var layout: ScheduleLayout!
    var sideMenuVC: SideMenuVC!
    var isTeamMemberCalled: Bool = false
    var presenter: SchedulePresenter!
    
    var weekDeliverables: [WeekDeliverable]!
    var week: Week!
    
    static func buildVC() -> ScheduleVC {
        return ScheduleVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.layout = ScheduleLayout(superview: self.view, scheduleLayoutDelegate: self)
        layout.setupViews()
        if AppDelegate.instance.unreadNotificationsNumber > 0 {
            self.layout.topView.notificationsNumberLabel.isHidden = false
            self.layout.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: CommonConstants.NOTIFICATIONS_UPDATED),
                                               object: self,
                                               queue: OperationQueue.main,
                                               using: notificationsUpdated(noti:))
        
        presenter = Injector.provideSchedulePresenter()
        presenter.setView(view: self)
        let currentWeek = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).week
        presenter.getWeek(token: Defaults[.token]!, weekID: Int((currentWeek?.requestId.components(separatedBy: "/")[(currentWeek?.requestId.components(separatedBy: "/").count)! - 1])!)!)
    }
    
    func notificationsUpdated(noti: Notification) {
        self.layout.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuVC = UiHelpers.setupSideMenu(delegate: self, viewToPresent: self.layout.topView.backImageView, viewToEdge: self.view, sideMenuCellDelegate: self, sideMenuHeaderDelegate: self)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension ScheduleVC: SideMenuHeaderDelegate {
    func headerClicked() {
        sideMenuVC.closeSideMenu()
        self.navigator = Navigator(navController: self.navigationController!)
        self.navigator.navigateToMyProfile()
    }
}

extension ScheduleVC: SideMenuCellDelegate {
    func sideMenuItemSelected(index: Int) {
        sideMenuVC.closeSideMenu()
        self.navigator = Navigator(navController: self.navigationController!)
        switch index {
        case 0:
            self.navigator.navigateToMyProfile()
            break
            
        case 1:
            self.navigator.navigateToVideos()
            break
            
        case 2:
            self.navigator.navigateToGameMethodology()
            break
            
        case 3:
            self.navigator.navigateToTerms()
            break
            
        case 4:
            self.navigator.navigateToSettings()
            break
            
        default:
            break
        }
        print("schedule :: item \(index) clicked")
    }
}

extension ScheduleVC: ScheduleLayoutDelegate {
    func retry() {
        let currentWeek = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).week
        presenter.getWeek(token: Defaults[.token]!, weekID: Int((currentWeek?.requestId.components(separatedBy: "/")[(currentWeek?.requestId.components(separatedBy: "/").count)! - 1])!)!)
    }
    
    func openSideMenu() {
        if Localize.currentLanguage() == "en" {
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        } else {
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
        
    }
    
    func goToNotificationsScreen() {
        self.navigator = Navigator(navController: self.navigationController!)
        self.navigator.navigateToNotifications()
    }
}

extension ScheduleVC: ScheduleView {
    func operationFailed(message: String) {
        self.view.makeToast(message, duration: 2, position: .center)
        
        for view in self.view.subviews {
            if !(view is TopView) {
                view.isHidden = true
            }
        }
        self.layout.showErrorViews()
    }
    
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable]) {
        
        if let _ = self.weekDeliverables {
            self.weekDeliverables.append(contentsOf: weekDeliverables)
        } else {
            self.weekDeliverables = weekDeliverables
        }
        
        if !isTeamMemberCalled {
            
            let teamMemberId = Int(Defaults[.teamMemberId]!.components(separatedBy: "/").get(at: Defaults[.teamMemberId]!.components(separatedBy: "/").count - 1)!)!
            
            presenter.getWeekDeliverables(parameters: ["teamMember":"\(teamMemberId)", "week":"\(week.requestId.components(separatedBy: "/").get(at: week.requestId.components(separatedBy: "/").count - 1)!)"], token: Defaults[.token]!)
            
            isTeamMemberCalled = true
        } else {
            self.weekDeliverables = self.weekDeliverables.sorted(by: { $0.feedbackCounter > $1.feedbackCounter })
            
            self.layout.scheduleTableView.dataSource = self
            self.layout.scheduleTableView.delegate = self
            self.layout.scheduleTableView.reloadData()
            
            for view in self.view.subviews {
                if !(view is TopView) {
                    view.isHidden = false
                }
                
            }
            self.layout.hideErrorViews()
        }
    }
    
    func weekDeliverableUpdatedSuccess(weekDeliverable: WeekDeliverable) {
        for wd in weekDeliverables {
            if wd.requestId == weekDeliverable.requestId {
                wd.status = weekDeliverable.status
                break
            }
        }
        self.layout.scheduleTableView.reloadData()
        
        for view in self.view.subviews {
            if !(view is TopView) {
                view.isHidden = false
            }
            
        }
        self.layout.hideErrorViews()
    }
    
    func getWeekSuccess(week: Week) {
        
        self.week = week
        
        let parameters = ["team":Defaults[.teamId]!, "week":"\(week.requestId.components(separatedBy: "/").get(at: week.requestId.components(separatedBy: "/").count - 1)!)"]
        presenter.getWeekDeliverables(parameters: parameters, token: Defaults[.token]!)
    }
    
}

extension ScheduleVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weekDeliverables.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = layout.scheduleTableView.dequeueReusableCell(withIdentifier: ScheduleCell.identifier, for: indexPath) as! ScheduleCell
        cell.delegate = self
        weekDeliverables.get(at: indexPath.row)!.week = self.week
        cell.weekDeliverable = weekDeliverables.get(at: indexPath.row)!
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let weekDeliverable = self.weekDeliverables.get(at: indexPath.row)!
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) + weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), font: AppFont.font(type: .Bold, size: 14))
    }
}

extension ScheduleVC: ScheduleCellDelegate {
    func openMeetingScreen(describtion: [Description]) {
        self.navigator.navigateToMeetingVC(describtion: describtion)
    }
    
    func openDeliverableDetails(weekDeliverable: WeekDeliverable) {
        self.navigator.navigateToDeliverableDetailsScreen(week: self.week, deliverable: weekDeliverable.deliverable)
    }
    
    func updateDeliverableStatus(weekDeliverable: WeekDeliverable, status: Bool) {
        var initialStatus = "delivered"
        if !status {
            initialStatus = "none"
        }
        self.presenter.updateWeekDeliverableStatus(weekDeliverable: weekDeliverable, token: Defaults[.token]!, status: initialStatus)
    }
}
