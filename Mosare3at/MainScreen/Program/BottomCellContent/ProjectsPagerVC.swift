//
//  ProjectsPagerVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/22/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ProjectsPagerVC: UIPageViewController {

    var presenter: ProjectPagerPresenter!
    var programId: Int!
    var startIndex = 0
    var pages: [ProjectPageVC] = []
    var pageControl = UIPageControl()
    // to return object of ProjectsPagerVC
    static func buildVC() -> ProjectsPagerVC {
        return ProjectsPagerVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = Injector.provideProjectPagerPresenter()
        presenter.setView(view: self)
        presenter.getProjects(programId: programId)
        
        self.dataSource = self
        self.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    func configurePageControl() {
        // The total number of pages that are available is based on how many available colors we have.
        pageControl = UIPageControl(frame: CGRect(x: 0,y: UIScreen.main.bounds.maxY - 50,width: UIScreen.main.bounds.width,height: 50))
        self.pageControl.numberOfPages = pages.count
        self.pageControl.currentPage = startIndex
        self.pageControl.tintColor = UIColor.black
        self.pageControl.pageIndicatorTintColor = UIColor.gray
        self.pageControl.currentPageIndicatorTintColor = UIColor.AppColors.darkRed
        self.view.addSubviews([pageControl])
        pageControl.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(self.view)
            maker.width.equalTo(UIScreen.main.bounds.width)
            maker.bottom.equalTo(self.view).offset(-1 * UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
    }
    
}

extension ProjectsPagerVC : ProjectPageView {
    func updateFirstTimeWeekSuccess() {
        
    }
    
    func getWeeksSuccess(weeks: [Week]) {
        
    }
    
    func getCurrentWeekStatusSuccess(currentWeekStatus: CurrentWeekStatus) {
        
    }
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getProjectsSuccess(projects: [Project]) {
        let currentProject = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).project
        for count in 0...projects.count-1 {
            let vc = ProjectPageVC()
            vc.projectId = projects[count].id
            vc.project = projects[count]
            if projects[count].requestId == currentProject?.requestId {
                startIndex = count
            }
            pages.append(vc)
        }
        
        setViewControllers([pages[startIndex]], direction: .reverse, animated: true, completion: nil)
        
        configurePageControl()
    }
}

extension ProjectsPagerVC: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController as! ProjectPageVC) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard pages.count > previousIndex else {
            return nil
        }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = pages.firstIndex(of: viewController as! ProjectPageVC) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let pagesCount = pages.count
        
        guard pagesCount != nextIndex else {
            return nil
        }
        
        guard pagesCount > nextIndex else {
            return nil
        }
        
        return pages[nextIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = pages.firstIndex(of: pageContentViewController as! ProjectPageVC)!
    }
}
