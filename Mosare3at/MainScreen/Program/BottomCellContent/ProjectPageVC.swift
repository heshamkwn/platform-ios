//
//  ProjectPageVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/22/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class ProjectPageVC: BaseVC {

    
    var presenter: ProjectPagerPresenter!
    var layout: ProjectPageLayout!
    var weeks: [Week] = []
    var currentWeekStatus: CurrentWeekStatus!
    var projectId: Int!
    var project: Project!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = Injector.provideProjectPagerPresenter()
        presenter.setView(view: self)
        
        layout = ProjectPageLayout(superview: self.view, projectPageLayoutDelegate: self)
        layout.setupViews()
        self.layout.weeksTableView.dataSource = self
        self.layout.weeksTableView.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.getWeeks(projectId: projectId)
        
        
        var title: String = "project1".localized()
        switch project.weight {
        case 1:
            title = "project1".localized()
            break
            
        case 2:
            title = "project2".localized()
            break
            
        case 3:
            title = "project3".localized()
            break
            
        case 4:
            title = "project4".localized()
            break
            
        case 5:
            title = "project5".localized()
            break
            
        case 6:
            title = "project6".localized()
            break
            
        case 7:
            title = "project7".localized()
            break
            
        case 8:
            title = "project8".localized()
            break
            
        default:
            break
        }
        
        self.layout.titleLabel.text = title
        self.layout.summaryLabel.text = project.title
        self.layout.mainImageView.af_setImage(withURL: URL(string: "\(CommonConstants.IMAGES_BASE_URL)\(project.bgImage!)")!)
    }
    
    func getIsOpened(week: Week) -> Bool {
        let currentDate = Date()
        let startDate = UiHelpers.convertStringToDate(dateString: week.startDate)
        if UiHelpers.compareDates(date1: currentDate, date2: startDate) == .FIRST_GREATER {
            return true
        } else {
            return false
        }
    }
    
    func getIsDone(week: Week) -> Bool {
        if !getIsOpened(week: week) {
            return false
        } else {
            let startDate = UiHelpers.convertStringToDate(dateString: week.startDate)
            let currentWeekStartDate = UiHelpers.convertStringToDate(dateString: Week.getInstance(dictionary: Defaults[.currentWeek]!).startDate)
            
            if UiHelpers.compareDates(date1: currentWeekStartDate, date2: startDate) == .FIRST_GREATER {
                return true
            } else {
                if week.weight < Week.getInstance(dictionary: Defaults[.currentWeek]!).weight {
                    return true
                } else {
                    return false
                }
            }
            
        }
    }
    
    func getIsWorkingOn(week: Week) -> Bool {
        if !getIsOpened(week: week) {
            return false
        } else {
            if week.requestId == Week.getInstance(dictionary: Defaults[.currentWeek]!).requestId {
                return true
            } else {
                return false
            }
        }
    }
    
    func goToWeekVisionScreen() {
        print("go to week vision screen")
    }
    
    func goToWeekDetailsScreen(screenTitle: String, weekTitle: String, week: Week, project: Project, isWorkingOn: Bool) {
        print("go to week details screen")
        if navigator == nil {
           navigator = Navigator(navController: self.navigationController!)
        }
        self.navigator.navigateToWeekDetailsScreen(screenTitle: screenTitle, weekTitle: weekTitle, week: week, project: project, isWorkingOn: isWorkingOn)
    }
}

extension ProjectPageVC : ProjectPageView {
    func updateFirstTimeWeekSuccess() {
        print("updating first week done")
        goToWeekVisionScreen()
        // on success of calling api, open week vision screen
        // in week vision screen, change the X icon to drawn icon in Kashkool
        // when click on new icon, open week details screen
    }
    
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getProjectsSuccess(projects: [Project]) {
        // nothing to do
    }
    
    func getWeeksSuccess(weeks: [Week]) {
        self.weeks.removeAll()
        self.weeks = weeks
        
        presenter.getCurrentWeekStatus(weekId: Week.getInstance(dictionary: Defaults[.currentWeek]!).id)
    }
    
    func getCurrentWeekStatusSuccess(currentWeekStatus: CurrentWeekStatus) {
        self.currentWeekStatus = currentWeekStatus
        self.layout.weeksTableView.reloadData()
    }
}

extension ProjectPageVC : ProjectPageLayoutDelegate {
    func retry() {
        
    }
}

extension ProjectPageVC : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weeks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:WeekCell = self.layout.weeksTableView.dequeueReusableCell(withIdentifier: WeekCell.identifier, for: indexPath) as! WeekCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.index = indexPath.row
        cell.week = weeks.get(at: indexPath.row)!
        cell.isWorkingOn = getIsWorkingOn(week: weeks.get(at: indexPath.row)!)
        if cell.isWorkingOn {
            cell.currentWeekStatus = self.currentWeekStatus
        }
        cell.isOpened = getIsOpened(week: weeks.get(at: indexPath.row)!)
        cell.isDone = getIsDone(week: weeks.get(at: indexPath.row)!)
        cell.setupViews()
        cell.populateCellData()
        cell.project = self.project
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 15)
    }
}

extension ProjectPageVC: WeekCellDelegate {
    func weekCellClicked(index: Int, isOpened: Bool, screenTitle: String, weekTitle: String, week: Week, project: Project, isWorkingOn: Bool) {
        let subscribtion = Subscribtion.getInstance(dictionary: Defaults[.subscription]!)
        if let weekId = self.weeks.get(at: index)?.id, isOpened {
            if subscribtion.firstTimeWeek.contains(weekId){
                 // open week details
                goToWeekDetailsScreen(screenTitle: screenTitle, weekTitle: weekTitle, week: week, project: project, isWorkingOn: isWorkingOn)
            } else {
                // update firstTimeWeek array
                subscribtion.firstTimeWeek.append(weekId)
                
                // save the subscribtion object to local
                Defaults[.subscription] = subscribtion.convertToDictionary()
                
                // call subscrition api and set firstTimeWeek data only
                self.presenter.updateFirstTimeWeek()
            }
        }
    }
}
