//
//  Project.swift
//  Mosare3at
//
//  Created by Hesham Donia on 10/16/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import SwiftyJSON

public class Project {
    
    var id: Int!
    var requestId: String!
    var title: String!
    var summary: String!
    var bgImage: String!
    var weight: Int!
    var image: String!
    var supervisor: String!
    var startDate: String!
    var endDate: String!
    var duration: String!
    var weeks: [Week]!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["id"] = id
        dictionary["@id"] = requestId
        dictionary["title"] = title
        dictionary["summary"] = summary
        dictionary["bgImage"] = bgImage
        dictionary["weight"] = weight
        dictionary["image"] = image
        dictionary["supervisor"] = supervisor
        dictionary["startDate"] = startDate
        dictionary["endDate"] = endDate
        dictionary["duration"] = duration
        
        if let _ = weeks {
            var weeksDicArray = [Dictionary<String, Any>]()
            
            for week in weeks {
                weeksDicArray.append(week.convertToDictionary())
            }
            
            dictionary["weeks"] = weeksDicArray
        }
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Project {
        let project = Project()
        project.id =  dictionary["id"] as? Int
        project.requestId =  dictionary["@id"] as? String
        project.title =  dictionary["title"] as? String
        project.summary =  dictionary["summary"] as? String
        project.bgImage =  dictionary["bgImage"] as? String
        project.weight =  dictionary["weight"] as? Int
        project.image =  dictionary["image"] as? String
        project.supervisor =  dictionary["supervisor"] as? String
        project.startDate =  dictionary["startDate"] as? String
        project.endDate =  dictionary["endDate"] as? String
        project.duration =  dictionary["duration"] as? String
        
        if let weeksDicArray = dictionary["weeks"] as? [Dictionary<String, Any>] {
            var weeks: [Week] = []
            for dic in weeksDicArray {
                weeks.append(Week.getInstance(dictionary: dic))
            }
            project.weeks =  weeks
        }
        
        return project
    }
}
