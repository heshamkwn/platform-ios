//
//  ProjectDetailsVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import AVKit

class ProjectDetailsVC: BaseVC {
    
    var projectId: Int!
    
    var presenter: ProjectDetailsPresenter!
    
    var layout: ProjectDetailsLayout!
    
    var projectDetails: ProjectDetails!
    
    public class func buildVC(projectId: Int) -> ProjectDetailsVC {
        let vc = ProjectDetailsVC()
        vc.projectId = projectId
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = ProjectDetailsLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        presenter = Injector.provideProjectDetailsPresenter()
        presenter.setView(view: self)
        
        presenter.getProjectDetails(projectId: self.projectId)
    }

}

extension ProjectDetailsVC: ProjectDetailsView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getProjectDetailsSuccess(projectDetails: ProjectDetails) {
        self.projectDetails = projectDetails
        
        self.layout.projectDetailsTableView.dataSource = self
        self.layout.projectDetailsTableView.delegate = self
        self.layout.projectDetailsTableView.reloadData()
    }
}

extension ProjectDetailsVC: ProjectDetailsLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func share(image: UIImage) {
        UiHelpers.shareImage(sharableImage: image, sourceView: self.view, vc: self)
    }
    
    func retry() {
        
    }
}

extension ProjectDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProjectDetailsCell.identifier, for: indexPath) as! ProjectDetailsCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.projectIndexName = UiHelpers.getIndexName(index: indexPath.row)
        cell.projectDetails = projectDetails
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.cellForRow(at: indexPath) as? ProjectDetailsCell
        
        if let _ = cell {
            var height:CGFloat = 0.0

            switch cell?.selection {
            case 0:
                height = cell?.mainTabView.getTabHeight() ?? 0
                break

            case 1:
                height = cell?.skillsTabView.getTabHeight() ?? 0
                break

            case 2:
                height = cell?.methodologyTabView.getTabHeight() ?? 0
                break

            case 3:
                height = cell?.questionTabView.getTabHeight() ?? 0
                break

            default:
                break
            }

            return height + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55)
        } else {

            var projectSummary = ""
            
            for data in projectDetails.data {
                if data.kind == "textBlock" && data.title == "نبذة عن المشروع" {
                    projectSummary = data.html.get(at: 0)!
                    projectSummary = projectSummary.byConvertingHTMLToPlainText()
                }
            }
            
            return  UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) +
                
                projectSummary.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: AppFont.font(type: .Bold, size: 16), lineBreakMode: .byWordWrapping) +
                
                UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10) * CGFloat(projectDetails.weeks.count) +
                
                UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55)
        }
    }
}

extension ProjectDetailsVC: ProjectDetailsCellDelegate {
    func playVideo() {
        let videoURL = URL(string: projectDetails.video)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func refreshBelowView(selection: Int) {
        self.layout.projectDetailsTableView.beginUpdates()
        self.layout.projectDetailsTableView.endUpdates()
    }
}
