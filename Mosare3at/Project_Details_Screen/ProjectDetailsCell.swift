//
//  ProjectDetailsCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Material

public protocol ProjectDetailsCellDelegate: class {
    func playVideo()
    func refreshBelowView(selection: Int)
}

class ProjectDetailsCell: UITableViewCell {

    public static let identifier = "ProjectDetailsCell"
    var superView: UIView!
    var selection: Int = 0
    
    var projectDetails: ProjectDetails!
    var projectIndexName: String!
    
    var delegate: ProjectDetailsCellDelegate!

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "project".localized() + " " + self.projectIndexName
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var programNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var videoThumbImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var playIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "play_icon")
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            self.delegate.playVideo()
        })
        return imageView
    }()
    
    lazy var playIconView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isUserInteractionEnabled = true
        view.addTapGesture(action: { (_) in
            self.delegate.playVideo()
        })
        return view
    }()
    
    lazy var videoDataView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.primaryColor
        return view
    }()
    
    lazy var dateImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "date_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    lazy var dateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var dateView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var languageImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "date_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    lazy var languageLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var languageView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    lazy var durationImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "alarm_white")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    lazy var durationLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var mainButton: RaisedButton = {
        let button = RaisedButton(title: "main".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.mainLine.isHidden = false
            self.skillsLine.isHidden = true
            self.methodologyLine.isHidden = true
            self.questionsLine.isHidden = true
            self.selection = 0
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 0)
            
        }
        return button
    }()
    
    lazy var mainLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = false
        return view
    }()
    
    lazy var skillsButton: RaisedButton = {
        let button = RaisedButton(title: "skills".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.isUserInteractionEnabled = true
        button.addTapGesture { recognizer in
            self.mainLine.isHidden = true
            self.skillsLine.isHidden = false
            self.methodologyLine.isHidden = true
            self.questionsLine.isHidden = true
            self.selection = 1
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 1)
            
        }
        return button
    }()
    
    lazy var skillsLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = true
        return view
    }()
    
    lazy var methodologyButton: RaisedButton = {
        let button = RaisedButton(title: "methodology".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.isUserInteractionEnabled = true
        button.addTapGesture { recognizer in
            self.mainLine.isHidden = true
            self.skillsLine.isHidden = true
            self.methodologyLine.isHidden = false
            self.questionsLine.isHidden = true
            self.selection = 2
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 2)
        }
        return button
    }()
    
    lazy var methodologyLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = true
        return view
    }()
    
    lazy var questionsButton: RaisedButton = {
        let button = RaisedButton(title: "questions".localized(), titleColor: .black)
        button.backgroundColor = UIColor.white
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 14)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.isUserInteractionEnabled = true
        button.addTapGesture { recognizer in
            self.mainLine.isHidden = true
            self.skillsLine.isHidden = true
            self.methodologyLine.isHidden = true
            self.questionsLine.isHidden = false
            self.selection = 3
            self.changeBelowViewResource()
            self.delegate.refreshBelowView(selection: 3)
            
        }
        return button
    }()
    
    lazy var questionsLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.isHidden = true
        return view
    }()
    
    var mainTabView: ProjectMainTabView!
    var skillsTabView: SkillsTabView!
    var methodologyTabView: ProjectMethodologyTabView!
    var questionTabView: ProjectQuestionsTabView!
    
    public func setupViews() {
        
        if mainTabView == nil {
            var projectSummary = ""
            
            var targetAuduince = ""
            
            var dataImageBox = [ImageBoxData]()
            
            for data in projectDetails.data {
                if data.kind == "textBlock" && data.title == "نبذة عن المشروع" {
                    projectSummary = data.html.get(at: 0)!
                    projectSummary = projectSummary.byConvertingHTMLToPlainText()
                }
                
                if let _ = data.dataImageBox {
                    dataImageBox = data.dataImageBox
                }
            }
            
            mainTabView = ProjectMainTabView.getInstance(projectSummary: projectSummary, weeks: projectDetails.weeks)
            mainTabView.setupViews()
            mainTabView.populateData()
            
            skillsTabView = SkillsTabView.getInstance(skills: projectDetails.skills)
            skillsTabView.setupViews()
            skillsTabView.populateData()
            skillsTabView.isHidden = true
            
            methodologyTabView = ProjectMethodologyTabView.getInstance(dataImageBox: dataImageBox)
            methodologyTabView.setupViews()
            methodologyTabView.populateData()
            methodologyTabView.isHidden = true
            
            questionTabView = ProjectQuestionsTabView.getInstance(questions: projectDetails.faqs, delegate: self)
            questionTabView.setupViews()
            questionTabView.populateData()
            questionTabView.isHidden = true
        }
        
        let views = [titleLabel, programNameLabel, videoThumbImageView, playIconImageView, playIconView, videoDataView, dateView, dateLabel, dateImageView, languageView, languageLabel, languageImageView, durationLabel, durationImageView, mainLine, mainButton, skillsLine, skillsButton, methodologyLine, methodologyButton, questionsLine, questionsButton, mainTabView, skillsTabView, methodologyTabView, questionTabView]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views as! [UIView])
        
        self.titleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.top.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.programNameLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        self.videoThumbImageView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(programNameLabel.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 30))
        }
        
        videoThumbImageView.addSubview(playIconImageView)
        self.playIconImageView.snp.makeConstraints { (maker) in
            maker.center.equalTo(videoThumbImageView)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
        }
        
        self.playIconView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(playIconImageView)
        }
        
        self.videoDataView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(videoThumbImageView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
        
        videoDataView.addSubviews([dateView, dateLabel, dateImageView, languageView, languageLabel, languageImageView, durationLabel, durationImageView])
        
        dateImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 4))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        dateLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(dateImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        dateView.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(dateLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        languageImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(dateView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 4))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        languageLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(languageImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 21))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        languageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(languageLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        durationImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(languageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        durationLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(durationImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 21))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        mainButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(superView)
        }
        
        mainLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(mainButton)
            maker.bottom.equalTo(mainButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        skillsButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(mainButton.snp.trailing)
        }
        
        skillsLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(skillsButton)
            maker.bottom.equalTo(skillsButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        methodologyButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(skillsButton.snp.trailing)
        }
        
        methodologyLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(methodologyButton)
            maker.bottom.equalTo(methodologyButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        questionsButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(videoDataView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 25))
            maker.leading.equalTo(methodologyButton.snp.trailing)
        }
        
        questionsLine.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(questionsButton)
            maker.bottom.equalTo(questionsButton.snp.bottom)
            maker.height.equalTo(2)
        }
        
        mainTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(mainButton.snp.bottom)
            maker.height.equalTo(mainTabView.getTabHeight())
        }
        
        skillsTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(mainButton.snp.bottom)
            maker.height.equalTo(skillsTabView.getTabHeight())
        }
        
        methodologyTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(mainButton.snp.bottom)
            maker.height.equalTo(methodologyTabView.getTabHeight())
        }
        
        questionTabView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(mainButton.snp.bottom)
            maker.height.equalTo(methodologyTabView.getTabHeight())
        }
        
        superView.bringSubviewToFront(mainLine)
        superView.bringSubviewToFront(skillsLine)
        superView.bringSubviewToFront(methodologyLine)
        superView.bringSubviewToFront(questionsLine)
        
    }
    
    public func populateData() {
        programNameLabel.text = projectDetails.title
        videoThumbImageView.image = UiHelpers.getVideoThumbnail(forUrl: URL(string: projectDetails.video)!)
        dateLabel.text = projectDetails.startDate.components(separatedBy: "T")[0]
        languageLabel.text = projectDetails.languages.get(at: 0)!
        durationLabel.text = projectDetails.duration
    }
    
    fileprivate func changeBelowViewResource() {
        
        switch self.selection {
        case 0:
            mainTabView.isHidden = false
            skillsTabView.isHidden = true
            methodologyTabView.isHidden = true
            questionTabView.isHidden = true
            superView.bringSubviewToFront(mainTabView)
            break
            
        case 1:
            mainTabView.isHidden = true
            skillsTabView.isHidden = false
            methodologyTabView.isHidden = true
            questionTabView.isHidden = true
            superView.bringSubviewToFront(skillsTabView)
            break
            
        case 2:
            mainTabView.isHidden = true
            skillsTabView.isHidden = true
            methodologyTabView.isHidden = false
            questionTabView.isHidden = true
            superView.bringSubviewToFront(methodologyTabView)
            break
            
        case 3:
            mainTabView.isHidden = true
            skillsTabView.isHidden = true
            methodologyTabView.isHidden = true
            questionTabView.isHidden = false
            superView.bringSubviewToFront(questionTabView)
            break
            
        default:
            break
        }
    }
}

extension ProjectDetailsCell: QuestionsTabViewDelegate {
    func expandCell() {
        self.delegate.refreshBelowView(selection: selection)
    }
}
