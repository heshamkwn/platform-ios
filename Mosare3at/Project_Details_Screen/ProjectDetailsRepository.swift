//
//  ProjectDetailsRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol ProjectDetailsPresenterDelegate: class {
    func operationFailed(message: String)
    func getProjectDetailsSuccess(projectDetails: ProjectDetails)
}

public class ProjectDetailsRepository {
    var delegate: ProjectDetailsPresenterDelegate!
    
    public func setDelegate(delegate: ProjectDetailsPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getProjectDetails(projectId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        //"Content-Type" : "application/json",
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "projects/\(projectId)/")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let projectDetails = ProjectDetails.getInstance(dictionary: json)
                        self.delegate.getProjectDetailsSuccess(projectDetails: projectDetails)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
