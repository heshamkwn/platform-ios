
//
//  SkillTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift

public class SkillsTabView: UIView {
    
    var skills: [Skill]!
    
    public class func getInstance(skills: [Skill]) -> SkillsTabView {
        let view: SkillsTabView = SkillsTabView()
        view.skills = skills
        return view
    }
    
    lazy var skillsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var skillsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "gainedSkills".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    lazy var skillsTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.isScrollEnabled = false
        tableView.register(TaskCell.self, forCellReuseIdentifier: TaskCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [skillsView, skillsLabel, skillsTableView]
        
        self.addSubviews(views)
        
        skillsView.addSubviews([skillsLabel, skillsTableView])
        
        skillsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5) * CGFloat(skills.count))
        }
        
        self.skillsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(skillsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(skillsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(skillsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.skillsTableView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(skillsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(skillsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(skillsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5) * CGFloat(skills.count))
        }
    }
    
    public func populateData() {
        self.skillsTableView.dataSource = self
        self.skillsTableView.delegate = self
        self.skillsTableView.reloadData()
    }
    
    public func getTabHeight() -> CGFloat {
        var height: CGFloat = 0.0
        
        height = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5) * CGFloat(skills.count)
        
        return height
    }
}

extension SkillsTabView: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return skills.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TaskCell.identifier, for: indexPath) as! TaskCell
        cell.setupViews()
        cell.populateCustomData(index: indexPath.row + 1, taskString: skills.get(at: indexPath.row)!.title)
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)
    }
}
