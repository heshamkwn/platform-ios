//
//  ProjectQuestionsTabView.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Localize_Swift

public class ProjectQuestionsTabView: UIView {
    
    var questions: [FAQ]!
    var delegate: QuestionsTabViewDelegate!
    
    public class func getInstance(questions: [FAQ], delegate: QuestionsTabViewDelegate) -> ProjectQuestionsTabView {
        let view = ProjectQuestionsTabView()
        view.questions = questions
        view.delegate = delegate
        return view
    }
    
    lazy var faqLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "faq".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    lazy var faqTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = false
        tableView.register(ExpandableQuestionCell.self, forCellReuseIdentifier: ExpandableQuestionCell.identifier)
        return tableView
    }()
    
    lazy var noDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "noData".localized()
        label.backgroundColor = .white
        label.textAlignment = .center
        label.isHidden = true
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    public func setupViews() {
        let views = [faqLabel, faqTableView, noDataLabel]
        
        self.addSubviews(views)
        
        self.faqLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.faqTableView.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(faqLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(calculateTableViewHeight())
        }
        
        self.noDataLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(faqLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
    }
    
    public func populateData() {
        if questions.count > 0 {
            faqTableView.dataSource = self
            faqTableView.delegate = self
            faqTableView.reloadData()
        } else {
            faqLabel.isHidden = true
            faqTableView.isHidden = true
            noDataLabel.isHidden = false
        }
        
    }
    
    fileprivate func calculateTableViewHeight() -> CGFloat {
        var height: CGFloat = 0.0
        for faq in  questions {
            if faq.isExpanded {
                height = height + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)
            } else {
                height = height + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5) + faq.answer!.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), font: AppFont.font(type: .Regular, size: 16), lineBreakMode: .byWordWrapping)
            }
        }
        return height
    }
    
    public func getTabHeight() -> CGFloat {
        var height: CGFloat = 0.0
        
        if questions.count > 0 {
            height = calculateTableViewHeight() + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)
        } else {
            height = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
        }
        
        return height
    }
}

extension ProjectQuestionsTabView: UITableViewDataSource, UITableViewDelegate {
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questions.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ExpandableQuestionCell.identifier, for: indexPath) as! ExpandableQuestionCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.index = indexPath.row
        cell.question = questions.get(at: indexPath.row)!
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        //        if !questions.get(at: indexPath.row)!.isExpanded {
        //            return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
        //        } else {
        //
        //        }
        
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10) + questions.get(at: indexPath.row)!.answer!.height(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), font: AppFont.font(type: .Regular, size: 16), lineBreakMode: .byWordWrapping)
    }
}

extension ProjectQuestionsTabView: ExpandableQuestionCellDelegate {
    public func expandCell(index: Int, isExpanded: Bool) {
        questions.get(at: index)!.isExpanded = isExpanded
        faqTableView.reloadData()
        self.delegate.expandCell()
    }
}

