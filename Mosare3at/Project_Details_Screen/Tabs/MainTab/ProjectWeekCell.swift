//
//  WeekCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift

class ProjectWeekCell: UITableViewCell {

    public static let identifier = "WeekCell"
    var superView: UIView!
    
    var week: Week!
    var index: Int!
    
    lazy var weekIndexView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8)/2
        view.clipsToBounds = true
        view.addBorder(width: 2, color: UIColor.AppColors.darkRed)
        return view
    }()
    
    lazy var weekIndexLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .clear
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var weekLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "week".localized()
        label.backgroundColor = .clear
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    lazy var weekTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.font = AppFont.font(type: .Regular, size: 18)
        return label
    }()
    
    public func setupViews() {
        let views = [weekIndexView, weekIndexLabel, weekTitleLabel, weekLabel]
        
        self.superView = self.contentView
        self.superView.addSubviews(views)
        
        weekIndexView.addSubviews([weekIndexLabel, weekLabel])
        
        weekIndexView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
        }
        
        weekIndexLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.top.equalTo(weekIndexView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        weekLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(weekIndexView)
            maker.top.equalTo(weekIndexLabel.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        weekTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(weekIndexView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
        }
        
    }
    
    public func populateData() {
        weekIndexLabel.text = "\(self.index + 1)"
        weekTitleLabel.text = week.title
    }

}
