//
//  ProjectDetailsPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
public protocol ProjectDetailsView: class {
    func operationFailed(message: String)
    func getProjectDetailsSuccess(projectDetails: ProjectDetails)
}

public class ProjectDetailsPresenter {
    fileprivate weak var projectDetailsView : ProjectDetailsView?
    fileprivate let projectDetailsRepository : ProjectDetailsRepository
    
    init(repository: ProjectDetailsRepository) {
        self.projectDetailsRepository = repository
        self.projectDetailsRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : ProjectDetailsView) {
        projectDetailsView = view
    }
}

extension ProjectDetailsPresenter {
    public func getProjectDetails(projectId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.projectDetailsRepository.getProjectDetails(projectId: projectId)
        } else {
            self.projectDetailsView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension ProjectDetailsPresenter: ProjectDetailsPresenterDelegate {
    public func operationFailed(message: String) {
        self.projectDetailsView?.operationFailed(message: message)
    }
    
    public func getProjectDetailsSuccess(projectDetails: ProjectDetails) {
        self.projectDetailsView?.getProjectDetailsSuccess(projectDetails: projectDetails)
    }
}
