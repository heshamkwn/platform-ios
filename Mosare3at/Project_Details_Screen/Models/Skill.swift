//
//  Skill.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class Skill {
    var requestId: String!
    var title: String!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["@id"] = requestId
        dictionary["title"] = title
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Skill {
        
        let skill = Skill()
        skill.requestId =  dictionary["@id"] as? String
        skill.title =  dictionary["title"] as? String
        return skill
    }
}
