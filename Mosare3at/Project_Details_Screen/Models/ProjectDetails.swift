//
//  ProjectDetails.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class ProjectDetails {
    var id: Int!
    var title: String!
    var summary: String!
    var image: String!
    var bgImage: String!
    var languages: [String]!
    var startDate: String!
    var endDate: String!
    var duration: String!
    var price: Int!
    var salePrice: Int!
    var booked: Int!
    var weight: Int!
    var total: Int!
    var batch: Int!
    var featured: Bool!
    var video: String!
    var faqs: [FAQ]!
    var data: [ProgramDetailsData]!
    var weeks: [Week]!
    var skills: [Skill]!
    var cancelSubscriptionDate: String!
    var approved: Bool!
    var isTeamed: Bool!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["id"] = "id"
        dictionary["title"] = title
        dictionary["summary"] = summary
        dictionary["image"] = image
        dictionary["bgImage"] = bgImage
        dictionary["language"] = languages
        dictionary["startDate"] = startDate
        dictionary["endDate"] = endDate
        dictionary["cancelSubscriptionDate"] = cancelSubscriptionDate
        dictionary["duration"] = duration
        dictionary["price"] = price
        dictionary["title"] = title
        dictionary["salePrice"] = salePrice
        dictionary["booked"] = booked
        dictionary["weight"] = weight
        dictionary["total"] = total
        
        if let _ = weeks {
            var weeksDicArray = [Dictionary<String, Any>]()
            
            for week in weeks {
                weeksDicArray.append(week.convertToDictionary())
            }
            dictionary["weeks"] = weeksDicArray
        }
        
        if let _ = faqs {
            var faqsDicArray = [Dictionary<String, Any>]()
            
            for faq in faqs {
                faqsDicArray.append(faq.convertToDictionary())
            }
            dictionary["faqs"] = faqsDicArray
        }
        
        if let _ = skills {
            var skillsDicArray = [Dictionary<String, Any>]()
            
            for skill in skills {
                skillsDicArray.append(skill.convertToDictionary())
            }
            dictionary["skills"] = skillsDicArray
        }
        
        dictionary["batch"] = batch
        dictionary["featured"] = featured
        dictionary["video"] = video
        
        if let _ = data {
            var dataDicArray = [Dictionary<String, Any>]()
            
            for dataObj in data {
                dataDicArray.append(dataObj.convertToDictionary())
            }
            dictionary["data"] = dataDicArray
        }
        
        dictionary["approved"] = approved
        dictionary["isTeamed"] = isTeamed
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> ProjectDetails {
        
        let projectDetails = ProjectDetails()
        projectDetails.id =  dictionary["id"] as? Int
        projectDetails.title =  dictionary["title"] as? String
        projectDetails.summary =  dictionary["summary"] as? String
        projectDetails.image =  dictionary["image"] as? String
        projectDetails.bgImage =  dictionary["bgImage"] as? String
        projectDetails.languages =  dictionary["language"] as? [String]
        projectDetails.startDate =  dictionary["startDate"] as? String
        projectDetails.endDate =  dictionary["endDate"] as? String
        projectDetails.title =  dictionary["title"] as? String
        projectDetails.cancelSubscriptionDate =  dictionary["cancelSubscriptionDate"] as? String
        projectDetails.duration =  dictionary["duration"] as? String
        projectDetails.price =  dictionary["price"] as? Int
        projectDetails.salePrice =  dictionary["salePrice"] as? Int
        projectDetails.booked =  dictionary["booked"] as? Int
        projectDetails.weight =  dictionary["weight"] as? Int
        projectDetails.total =  dictionary["total"] as? Int
        
        if let weeksDicArray = dictionary["weeks"], weeksDicArray is [Dictionary<String, Any>] {
            var weeksArray: [Week] = []
            for dic in weeksDicArray as! [Dictionary<String, Any>] {
                weeksArray.append(Week.getInstance(dictionary: dic))
            }
            projectDetails.weeks = weeksArray
        }
        
        if let faqsDicArray = dictionary["faqs"], faqsDicArray is [Dictionary<String, Any>] {
            var faqsArray: [FAQ] = []
            for dic in faqsDicArray as! [Dictionary<String, Any>] {
                faqsArray.append(FAQ.getInstance(dictionary: dic))
            }
            projectDetails.faqs = faqsArray
        }
        
        if let skillsDicArray = dictionary["skills"], skillsDicArray is [Dictionary<String, Any>] {
            var skillsArray: [Skill] = []
            for dic in skillsDicArray as! [Dictionary<String, Any>] {
                skillsArray.append(Skill.getInstance(dictionary: dic))
            }
            projectDetails.skills = skillsArray
        }
        
        projectDetails.batch =  dictionary["batch"] as? Int
        projectDetails.featured =  dictionary["featured"] as? Bool
        projectDetails.video =  dictionary["video"] as? String
        
        if let dataDicArray = dictionary["data"], dataDicArray is [Dictionary<String, Any>] {
            var dataArray: [ProgramDetailsData] = []
            for dic in dataDicArray as! [Dictionary<String, Any>] {
                dataArray.append(ProgramDetailsData.getInstance(dictionary: dic))
            }
            projectDetails.data = dataArray
        }
        
        projectDetails.approved =  dictionary["approved"] as? Bool
        projectDetails.isTeamed =  dictionary["isTeamed"] as? Bool
        return projectDetails
    }
}
