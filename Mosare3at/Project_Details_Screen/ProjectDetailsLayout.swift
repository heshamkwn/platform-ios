//
//  ProjectDetailsLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/5/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Material
import UIKit
import Alamofire
import SwiftyUserDefaults

public protocol ProjectDetailsLayoutDelegate: BaseLayoutDelegate {
    func goBack()
    func share(image: UIImage)
}

public class ProjectDetailsLayout: BaseLayout {
    var projectDetailsLayoutDelegate: ProjectDetailsLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: ProjectDetailsLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.projectDetailsLayoutDelegate = delegate
    }
    
    lazy var projectDetailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(ProjectDetailsCell.self, forCellReuseIdentifier: ProjectDetailsCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [topView, projectDetailsTableView]
        
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.projectDetailsTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.leading.trailing.bottom.equalTo(superview)
        }
        
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "")
        self.topView.screenTitleLabel.isHidden = true
        self.topView.notificationsImageView.isHidden = false
        self.topView.notificationsImageView.image = UIImage(named: "ic_share")?.withRenderingMode(.alwaysTemplate)
        self.topView.notificationsImageView.tintColor = .white
        self.topView.delegate = self
    }
}

extension ProjectDetailsLayout: TopViewDelegate {
    public func goBack() {
        self.projectDetailsLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        let programImageUrl = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.bgImage
        
        Alamofire.request(URL(string: CommonConstants.IMAGES_BASE_URL + programImageUrl!)!).responseData { (response) in
            if response.error == nil {
                print(response.result)
                if let data = response.data {
                    self.projectDetailsLayoutDelegate.share(image: UIImage(data: data)!)
                }
            }
        }
    }
}
