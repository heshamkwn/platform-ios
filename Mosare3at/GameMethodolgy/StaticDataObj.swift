//
//  MethodolofyGameObj.swift
//  Mosare3at
//
//  Created by Hesham Donia on 11/13/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class StaticDataObj {
    public var title:String!
    public var details: String!
    
    public init(title: String, details: String) {
        self.title = title
        self.details = details
    }
}
