//
//  DeliverableDetailsRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/8/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol DeliverableDetailsPresenterDelegate {
    func operationFailed(message: String)
    func getDeliverableSuccess(deliverable: Deliverable)
}

public class DeliverableDetailsRepository {
    
    var delegate: DeliverableDetailsPresenterDelegate!
    
    public func setDelegate(delegate: DeliverableDetailsPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getDeliverable(deliverableId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "deliverables/\(deliverableId)")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        
                        let deliverable = Deliverable.getInstance(dictionary: json)
                        self.delegate.getDeliverableSuccess(deliverable: deliverable)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                     self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}

