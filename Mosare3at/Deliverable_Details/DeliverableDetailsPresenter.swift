//
//  DeliverableDetailsPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/8/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation


public protocol DeliverableDetailsView: class {
    func operationFailed(message: String)
    func getDeliverableSuccess(deliverable: Deliverable)
}


public class DeliverableDetailsPresenter {
    fileprivate weak var deliverableDetailsView : DeliverableDetailsView?
    fileprivate let deliverableDetailsRepository : DeliverableDetailsRepository
    
    init(repository: DeliverableDetailsRepository) {
        self.deliverableDetailsRepository = repository
        self.deliverableDetailsRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : DeliverableDetailsView) {
        deliverableDetailsView = view
    }
}

extension DeliverableDetailsPresenter {
    public func getDeliverableDetails(deliverableId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.deliverableDetailsRepository.getDeliverable(deliverableId: deliverableId)
        } else {
            self.deliverableDetailsView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension DeliverableDetailsPresenter: DeliverableDetailsPresenterDelegate {
    public func operationFailed(message: String) {
        UiHelpers.hideLoader()
        self.deliverableDetailsView?.operationFailed(message: message)
    }
    
    public func getDeliverableSuccess(deliverable: Deliverable) {
        UiHelpers.hideLoader()
        self.deliverableDetailsView?.getDeliverableSuccess(deliverable: deliverable)
    }
}
