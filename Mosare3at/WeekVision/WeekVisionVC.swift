//
//  WeekVisionVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 11/1/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift
import AVKit

class WeekVisionVC: BaseVC {
    
    var weekMaterial: WeekMaterial!
    var project: Project!
    var week: Week!
    var videoLink: String!
    var layout: WeekVisionLayout!
    var presenter: WeekVisionPresenter!
    
    public static func buildVC(weekMaterial: WeekMaterial, project: Project, week: Week) -> WeekVisionVC {
        let vc = WeekVisionVC()
        vc.weekMaterial = weekMaterial
        vc.week = week
        vc.project = project
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = WeekVisionLayout(superview: self.view, weekVisionLayoutDelegate: self, screenTitle: getScreenTitle())
        layout.setupViews()
        layout.weekVisionTableView.dataSource = self
        layout.weekVisionTableView.delegate = self
        layout.weekVisionTableView.reloadData()
        
        presenter = Injector.provideWeekVisionPresenter()
        presenter.setView(view: self)
        if let video = self.weekMaterial.video {
            presenter.getVideoLinks(videoId: video.id)
        }
    }
    
    func getProjectIndexName() -> String{
        return UiHelpers.getIndexName(index: self.project.weight - 1)
    }
    
    func getWeekIndexName() -> String {
        return UiHelpers.getIndexName(index: self.week.weight - 1)
    }
    
    func getScreenTitle() -> String {
        switch (self.week.weight) {
            
        case 1:
            return "\("vision".localized()) \("week1".localized())"
        case 2:
            return "\("vision".localized()) \("week2".localized())"
        case 3:
            return "\("vision".localized()) \("week3".localized())"
        case 4:
            return "\("vision".localized()) \("week4".localized())"
        case 5:
            return "\("vision".localized()) \("week5".localized())"
        case 6:
            return "\("vision".localized()) \("week6".localized())"
        case 7:
            return "\("vision".localized()) \("week7".localized())"
        case 8:
            return "\("vision".localized()) \("week8".localized())"
            
        default:
            return "\("vision".localized()) \("week1".localized())"
        }
        
    }
    
    func getTeamDeliverables() -> [Deliverable] {
        var result: [Deliverable] = []
        for deliverable in weekMaterial.deliverables {
            if deliverable.collective {
                result.append(deliverable)
            }
        }
        return result
    }
    
    func getIndividualsDeliverables() -> [Deliverable] {
        var result: [Deliverable] = []
        for deliverable in weekMaterial.deliverables {
            if !deliverable.collective {
                result.append(deliverable)
            }
        }
        return result
    }
    
}

extension WeekVisionVC: WeekVisionLayoutDelegate {
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension WeekVisionVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:WeekVisionCell = self.layout.weekVisionTableView.dequeueReusableCell(withIdentifier: WeekVisionCell.identifier, for: indexPath) as! WeekVisionCell
        cell.selectionStyle = .none
        cell.startWeekButtonText = "\("startWeek".localized()) \(getWeekIndexName())"
        cell.tasks = weekMaterial.expectations
        cell.setupViews()
        cell.delegate = self
        cell.index = indexPath.row
        cell.teamDeliverables = getTeamDeliverables()
        cell.individualsDeliverables = getIndividualsDeliverables()
        if let video = self.weekMaterial.video {
            cell.populateData(congratulationsText: "\("congratToStart".localized()) \(getWeekIndexName()) \("fromProject".localized()) \(getProjectIndexName())", videoThumbUrl: (video.thumbnails.get(at: 0)?.thumb)!)
        } else {
            cell.populateData(congratulationsText: "\("congratToStart".localized()) \(getWeekIndexName()) \("fromProject".localized()) \(getProjectIndexName())", videoThumbUrl: "")
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 150)
    }
}

extension WeekVisionVC: WeekVisionCellDelegate {
    func outputCellClicked(deliverable: Deliverable) {
        self.navigator.navigateToDeliverableDetailsScreen(week: self.week, deliverable: deliverable)
    }
    
    func goToMilestonesScreen(index: Int) {
        self.navigator.navigateToMilestonesScreen(weekMaterial: self.weekMaterial, project: self.project, week: self.week, clickedMilestone: self.weekMaterial.milestones.get(at: index)!, currentMilestoneIndex: index)
    }
    
    func playVideo() {
        if videoLink != nil {
            let videoURL = URL(string: videoLink)
            let player = AVPlayer(url: videoURL!)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        } else {
            if let video = self.weekMaterial.video {
                presenter.getVideoLinks(videoId: video.id)
            } else {
                self.view.makeToast("noVideo".localized())
            }
        }
    }
}

extension WeekVisionVC: WeekVisionView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getVideoLinksSuccess(link: String) {
        videoLink = link
    }
}
