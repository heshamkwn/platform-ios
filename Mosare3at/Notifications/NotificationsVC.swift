//
//  NotificationsVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 11/8/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

class NotificationsVC: BaseVC {

    var layout: NotificationsLayout!
    
    var notifications: [NotificationObj]!
    
    var nextPage: String!
    
    var presenter: NotificationsPresenter!
    
    public static func buildVC() -> NotificationsVC {
        let vc = NotificationsVC()
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = NotificationsLayout(superview: self.view, notificationsLayoutDelegate: self, screenTitle: "notifications".localized())
        layout.setupViews()
        layout.notificationsTableView.dataSource = self
        layout.notificationsTableView.delegate = self
        
        presenter = Injector.provideNotificationsPresenter()
        presenter.setView(view: self)
        getNotifications(url: CommonConstants.BASE_URL + "notifications?flag=mob&both")
        
        NotificationCenter.default.addObserver(self, selector: #selector(notificationsUpdated(_:)), name: NSNotification.Name(rawValue: CommonConstants.NOTIFICATIONS_UPDATED), object: nil)
    }
    
    @objc func notificationsUpdated(_ notification:Notification) {
        getNotifications(url: CommonConstants.BASE_URL + "notifications?flag=mob&both")
    }
    
    func getNotifications(url: String) {
        presenter.getNotifications(url: url)
    }
}

extension NotificationsVC: NotificationsView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getNotificationsSuccess(notifications: [NotificationObj], nextPage: String) {
        self.notifications = notifications
        self.nextPage = nextPage
        
        self.layout.notificationsTableView.reloadData()
    }
}

extension NotificationsVC: NotificationsLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension NotificationsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if notifications != nil {
            return notifications.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:NotificationCell = self.layout.notificationsTableView.dequeueReusableCell(withIdentifier: NotificationCell.identifier, for: indexPath) as! NotificationCell
        cell.selectionStyle = .none
        cell.notification = self.notifications.get(at: indexPath.row)!
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        
//        if indexPath.row == notifications.count - 1 && nextPage != nil && !nextPage.isEmpty {
//            getNotifications(url: self.nextPage)
//        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 15)
    }
}

extension NotificationsVC: NotificationCellDelegate {
    func handleNotificationClick(notification: NotificationObj) {
        switch notification.type {
            
        case CommonConstants.weekSummaryNotificationType: // go to week summary screen (New Screen)
            let weekSummaryVC = WeekSummaryVC.buildVC(id: notification.data.objectId)
            self.pushVC(weekSummaryVC)
            break
            
        case CommonConstants.newPointsNotificationType: // go to leaderboard screen (tab 2)
            
            let mainScreenVC = MainScreenVC.buildVC()
            mainScreenVC.selectedIndex = 1
            self.pushVC(mainScreenVC)
            break
            
        case CommonConstants.newBadgeNotificationType: // go to all badges screen
            let allBadgesVC = AllBadgesVC.buildVC(badges: Singleton.getInstance().badges)
            presentVC(allBadgesVC)
            break
            
        case CommonConstants.programReminderNotificationType: // go to home screen on first tab
        
            let mainScreenVC = MainScreenVC.buildVC()
            mainScreenVC.selectedIndex = 0
            pushVC(mainScreenVC)
            break
            
        case CommonConstants.deliverableReminderNotificationType, CommonConstants.deliverableAcceptedNotificationType, CommonConstants.deliverableRejectedNotificationType: // go to deliverable details screen
            
            let deliverableDetailsVC = DeliverableDetailsVC.buildVC(deliverableId: notification.data.objectId)
            pushVC(deliverableDetailsVC)
            break
            
        case CommonConstants.newCertificateNotificationType: // go to achievements screen
            let achivementsVC = AchievementsVC.buildVC(isFromNotifications: true)
            pushVC(achivementsVC)
            break
            
        case CommonConstants.updatingTeamNotificationType: // go to team screen tab 3
          
            let mainScreenVC = MainScreenVC.buildVC()
            mainScreenVC.selectedIndex = 2
            pushVC(mainScreenVC)
            break
            
        case CommonConstants.sharingVideoNotificationType: // go to videos screen
            let videosVC = VideosVC.buildVC()
            pushVC(videosVC)
            break
            
        case CommonConstants.feedbackNotificationType: // go to feedbacks screen
            let weekDeliverableId = notification.data.weekDeliverableId
            let feedbackVC = FeedbackVC.buildVC(weekDeliverableId: weekDeliverableId!)
            pushVC(feedbackVC)
            break
            
        default:
            break
        }
    }
}
