//
//  WaitingLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/12/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol WaitingLayoutDelegate: BaseLayoutDelegate {
    func openSideMenu()
}

public class WaitingLayout: BaseLayout {

    var waitingLayoutDelegate: WaitingLayoutDelegate!
    
    var topView: TopView = TopView()
    
    lazy var waitingTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.isScrollEnabled = true
        tableView.register(WaitingCell.self, forCellReuseIdentifier: WaitingCell.identifier)
        return tableView
    }()
    
    init(superview: UIView, delegate: WaitingLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.waitingLayoutDelegate = delegate
    }
    
    public func setupViews() {
        
        let views = [topView, waitingTableView]
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.waitingTableView.snp.makeConstraints { maker in
            maker.leading.trailing.bottom.equalTo(superview)
            maker.top.equalTo(self.topView.snp.bottom)
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "")
        self.topView.backImageView.image = UIImage(named: "side_menu")
        self.topView.notificationsImageView.isHidden = true
        if AppDelegate.instance.unreadNotificationsNumber > 0 {
            self.topView.notificationsNumberLabel.text = "\(AppDelegate.instance.unreadNotificationsNumber)"
        }
        self.topView.logoImageView.isHidden = false
        self.topView.screenTitleLabel.isHidden = true
        self.topView.delegate = self
        self.topView.backImageView.addTapGesture(action: nil)
        self.topView.backImageView.addTapGesture { (_) in
            self.waitingLayoutDelegate.openSideMenu()
        }
    }
}

extension WaitingLayout : TopViewDelegate {
    public func goBack() {
        // open side menu here
    }
    
    public func goToNotifications() {
        // go to notifications screen
    }
}
