//
//  WaitingVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/12/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import SideMenu
import Localize_Swift

class WaitingVC: BaseVC, UISideMenuNavigationControllerDelegate {
    
    var programId: Int!
    var presenter: ProgramDetailsPresenter!
    var programDetails: ProgramDetails!
    var layout: WaitingLayout!
    var sideMenuVC: SideMenuVC!
    
    public static func buildVC() -> WaitingVC {
        let vc = WaitingVC()
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = WaitingLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        programId = Int(Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").get(at: Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").count - 1)!)!
        
        presenter = Injector.provideProgramDetailsPresenter()
        presenter.setView(view: self)
        
        presenter.getProgramDetails(programId: programId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sideMenuVC = UiHelpers.setupSideMenu(delegate: self, viewToPresent: self.layout.topView.backImageView, viewToEdge: self.view, sideMenuCellDelegate: self, sideMenuHeaderDelegate: self)
    }
}

extension WaitingVC: ProgramDetailsView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getProgramDetailsSuccess(programDetails: ProgramDetails) {
        self.programDetails = programDetails
        
        layout.waitingTableView.dataSource = self
        layout.waitingTableView.delegate = self
        layout.waitingTableView.reloadData()
    }
}

extension WaitingVC: WaitingLayoutDelegate {
    func openSideMenu() {
        if Localize.currentLanguage() == "en" {
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
        } else {
            present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        }
    }
    
    func retry() {
        
    }
    
    
}

extension WaitingVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WaitingCell.identifier, for: indexPath) as! WaitingCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.programName = programDetails.title
        cell.projects = programDetails.projects
        cell.guests = programDetails.guests
        cell.programStartDate = programDetails.startDate
        cell.programImageUrl = programDetails.bgImage
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 170)
    }
    
}

extension WaitingVC: WaitingCellDelegate {
    func goToProgramDetails() {
        navigator.navigateToProgramDetails()
    }
    
    func goToProjectDetails(index: Int) {
        let projectId = (programDetails.projects.get(at: index)?.requestId)!.components(separatedBy: "/")[(programDetails.projects.get(at: index)?.requestId)!.components(separatedBy: "/").count - 1]
        
        navigator.navigateToProjectDetails(projectId: Int(projectId)!)
    }
}

extension WaitingVC: SideMenuCellDelegate {
    func sideMenuItemSelected(index: Int) {
        sideMenuVC.closeSideMenu()
        self.navigator = Navigator(navController: self.navigationController!)
        
        switch index {
        case 0:
            self.navigator.navigateToMyProfile()
            break
            
        case 1:
            self.navigator.navigateToVideos()
            break
            
        case 2:
            self.navigator.navigateToGameMethodology()
            break
            
        case 3:
            self.navigator.navigateToTerms()
            break
            
        case 4:
            self.navigator.navigateToSettings()
            break
            
        default:
            break
        }
        print("program :: item \(index) clicked")
    }
}

extension WaitingVC: SideMenuHeaderDelegate {
    func headerClicked() {
        sideMenuVC.closeSideMenu()
        self.navigator = Navigator(navController: self.navigationController!)
        self.navigator.navigateToMyProfile()
    }
}

