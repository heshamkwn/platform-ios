//
//  WaitingCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/12/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import UICircularProgressRing
import Localize_Swift

public protocol WaitingCellDelegate: class {
    func goToProgramDetails()
    func goToProjectDetails(index: Int)
}

class WaitingCell: UITableViewCell {

    var programName: String!
    var projects: [Project]!
    var guests: [Guest]!
    var programStartDate: String!
    var programImageUrl: String!
    var delegate: WaitingCellDelegate!
    
    static let identifier = "WaitingCell"
    var superView: UIView!
    
    lazy var programPhotoImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var programNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 28)
        return label
    }()
    
    lazy var timerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 26) / 2
        return view
    }()
    
    lazy var timerProgressBar: UICircularProgressRing = {
        let progressBar = UICircularProgressRing()
        progressBar.maxValue = 60
        progressBar.innerRingColor = UIColor.AppColors.darkRed
        progressBar.outerRingColor = UIColor.white
        progressBar.outerRingWidth = 5
        progressBar.innerRingWidth = 5
        progressBar.startAngle = 270
        progressBar.shouldShowValueText = false
        progressBar.innerCapStyle = CGLineCap(rawValue: 2)!
        progressBar.ringStyle = UICircularProgressRingStyle(rawValue: 2)!
        progressBar.startProgress(to: CGFloat(1 * 100), duration: 0)
        return progressBar
    }()
    
    lazy var minuteNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.text = "minute".localized()
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var minuteVlueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 24)
        return label
    }()
    
    lazy var hourNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.text = "hour".localized()
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var hourValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var dayNameLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.textAlignment = .center
        label.text = "day".localized()
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var dayValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var programInfoImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "ic_info")
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4) / 2
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            self.delegate.goToProgramDetails()
        })
        return imageView
    }()
    
    lazy var projectsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var projectsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "programProjects".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    var projectsCollectionView: UICollectionView = UICollectionView(frame: CGRect(),collectionViewLayout: UICollectionViewFlowLayout())
    
    lazy var guestsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var guestsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "guests".localized()
        label.backgroundColor = .white
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    var guestsCollectionView: UICollectionView = UICollectionView(frame: CGRect(),collectionViewLayout: UICollectionViewFlowLayout())
    
    public func setupViews() {
        
        let views = [programPhotoImageview, programNameLabel, timerView, timerProgressBar, minuteNameLabel, minuteVlueLabel, hourNameLabel, hourValueLabel, dayNameLabel, dayValueLabel, programInfoImageview, projectsView, projectsLabel, projectsCollectionView, guestsView, guestsLabel, guestsCollectionView]
        
        superView = self.contentView
        
        superView.addSubviews(views)
        
        programPhotoImageview.snp.makeConstraints { (maker) in
            maker.leading.trailing.top.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 50))
        }
        
        programNameLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
        
        timerProgressBar.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(superView)
            maker.top.equalTo(programNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 30))
        }
        
        timerView.snp.makeConstraints { (maker) in
            maker.centerX.equalTo(timerProgressBar)
            maker.top.leading.equalTo(timerProgressBar).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.bottom.trailing.equalTo(timerProgressBar).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) * -1)
            
        }
        
        minuteNameLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(timerView)
            maker.top.equalTo(timerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        minuteVlueLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(timerView)
            maker.top.equalTo(minuteNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        dayValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(timerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 24))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.top.equalTo(minuteVlueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        hourValueLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(timerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 24))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.top.equalTo(minuteVlueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        dayNameLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(timerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 24))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.top.equalTo(dayValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        hourNameLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(timerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 24))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.top.equalTo(dayValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        programInfoImageview.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3) * -1)
            maker.bottom.equalTo(programPhotoImageview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
        }
        
        projectsView.addSubviews([projectsLabel, projectsCollectionView])
        
        projectsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(programInfoImageview.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55))
        }
        
        self.projectsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.projectsCollectionView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(projectsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(projectsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 48))
        }
        
        guestsView.addSubviews([guestsLabel, guestsCollectionView])
        
        guestsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(self).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(projectsView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65))
        }
        
        self.guestsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.guestsCollectionView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(guestsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.top.equalTo(guestsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 58))
        }
        
    }
    
    public func populateData() {
        
        programNameLabel.text = programName
        
        self.programPhotoImageview.af_setImage(withURL: URL(string: "\(CommonConstants.IMAGES_BASE_URL)\(programImageUrl!)")!)
        
        startTimer()
        
        projectsCollectionView.backgroundColor = .clear
        projectsCollectionView.dataSource = self
        projectsCollectionView.delegate = self
        projectsCollectionView.register(ProjectCell.self, forCellWithReuseIdentifier: ProjectCell.identifier)
        projectsCollectionView.showsHorizontalScrollIndicator = false
        projectsCollectionView.isScrollEnabled = true
        projectsCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        projectsCollectionView.contentSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 100), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 48))
        if let layout = projectsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 45), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 45))
        }
        
        projectsCollectionView.reloadData()
        
        guestsCollectionView.backgroundColor = .clear
        guestsCollectionView.dataSource = self
        guestsCollectionView.delegate = self
        guestsCollectionView.register(GuestCell.self, forCellWithReuseIdentifier: GuestCell.identifier)
        guestsCollectionView.showsHorizontalScrollIndicator = false
        guestsCollectionView.isScrollEnabled = true
        if Localize.currentLanguage() == "ar" {
            guestsCollectionView.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        }
        guestsCollectionView.contentSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 100), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 58))
        if let layout = guestsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.scrollDirection = .horizontal
            layout.itemSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 35), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 55))
        }
        
        guestsCollectionView.reloadData()
    }
    var days = 0, hours = 0, minutes = 0, seconds = 0
    var timer = Timer()
    
    func startTimer() {
        
        let currentDate = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss" //Your date format
        let finalProgramStartDate = programStartDate.components(separatedBy: "+").get(at: 0)
        let programDate = dateFormatter.date(from: finalProgramStartDate!)
//        let programDate = Date().addingTimeInterval(12345678910)
        let components = Calendar.current.dateComponents([.day, .hour, .minute, .second], from: currentDate, to: programDate!)

        
        timer.invalidate() // just in case this button is tapped multiple times
        
        
        days = components.day ?? -1
        hours = components.hour ?? -1
        minutes = components.minute ?? -1
        seconds = components.second ?? -1
        seconds = 60 - seconds
        
        dayValueLabel.text = "\(days)"
        hourValueLabel.text = "\(hours)"
        minuteVlueLabel.text = "\(minutes)"
        
        // start the timer
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(applyTimerLogic), userInfo: nil, repeats: true)
        
        
        
    }
    
    @objc func applyTimerLogic() {
        // apply timer logic
        /*
         1- get the current date
         2- get the difference between current date and project date in days, hours, minutes, seconds
         3- create timer that decrease the seconds by one every second and update the progress bar
         - if seconds is 0 --> decrease the minutes by one and update the minutes label, reset the progress
         - if minutes is 0 --> decrease the hours by one and update the hours label and minutes label to 59
         - if hours is 0 --> decrease the days by one and update the days label and hours label to 23
         */
        
        
        if seconds == 0 && minutes == 0 && hours == 0 && days == 0 {
            timer.invalidate()
        } else {
            if seconds > 59 {
                seconds = 0
                minutes = minutes - 1
                if minutes <= 0 {
                    minutes = 59
                    hours = hours - 1
                    if hours <= 0 {
                        hours = 23
                        days = days - 1
                        dayValueLabel.text = "\(days)"
                    }
                    hourValueLabel.text = "\(hours)"
                }
                minuteVlueLabel.text = "\(minutes)"
            }
            seconds = seconds + 1
            timerProgressBar.startProgress(to: UICircularProgressRing.ProgressValue(seconds), duration: 0.1)
        }
    }
    
    
    
}

extension WaitingCell: UICollectionViewDataSource, UICollectionViewDelegate {
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == projectsCollectionView {
            return projects.count
        } else {
            return guests.count
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == projectsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProjectCell.identifier, for: indexPath) as! ProjectCell
            cell.index = projects.count - 1 - indexPath.row
            cell.project = projects.get(at: projects.count - 1 - indexPath.row)!
            cell.delegate = self
            cell.setupViews()
            cell.populateData()
            if Localize.currentLanguage() == "ar" {
                cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GuestCell.identifier, for: indexPath) as! GuestCell
            cell.guest = guests.get(at: indexPath.row)!
            cell.setupViews()
            cell.populateData()
            if Localize.currentLanguage() == "ar" {
                cell.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            }
            return cell
        }
    }
}

extension WaitingCell: ProjectCellDelegate {
    func openProjectDetails(index: Int) {
        self.delegate.goToProjectDetails(index: index)
    }
}
