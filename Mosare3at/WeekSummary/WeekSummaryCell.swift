//
//  WeekSummaryCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/20/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift

public protocol WeekSummaryCellDelegate {
    func goToProgramScreen()
}

class WeekSummaryCell: UITableViewCell {

    public var delegate: WeekSummaryCellDelegate!
    public static let identifier = "WeekSummaryCell"
    var superView: UIView!
    
    var weekSummary: WeekSummary!
    var lastWeekSummary: WeekSummary?
    
    
    lazy var topContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var pointsImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "points")
        return imageView
    }()
    
    lazy var pointsTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "point".localized()
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    lazy var pointsValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var horizontalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()
    
    lazy var badgesImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "prize")
        return imageView
    }()
    
    lazy var badgesTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "badges".localized()
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    lazy var badgesValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var activityDetailsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var activityDetailsLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.text = "activitiesDetails".localized()
        
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
//    var barsChart: BarsChart! // We will create that object in the vc and equalize the barsView with chart.view
    
    lazy var barsView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    lazy var barsColorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5)/2
        view.clipsToBounds = true
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var barsColorLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.AppColors.gray
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.text = "gainedPointsInDay".localized()
        label.font = AppFont.font(type: .Bold, size: 10)
        return label
    }()
    
    lazy var statisticsContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()

    lazy var statisticsTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = false
        tableView.register(WeekSummaryStatisticsCell.self, forCellReuseIdentifier: WeekSummaryStatisticsCell.identifier)
        return tableView
    }()
    
    lazy var shareContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 4
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var shareWeekSummaryLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 16)
        label.addTapGesture(action: { (_) in
            self.delegate.goToProgramScreen()
        })
        
        return label
    }()
    
    lazy var shareImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.backgroundColor = UIColor.AppColors.gray
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3)
        return imageView
    }()
    
    lazy var programPageLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "programPage".localized()
        label.font = AppFont.font(type: .Regular, size: 16)
        label.addTapGesture(action: { (_) in
            self.delegate.goToProgramScreen()
        })
        
        return label
    }()
    
    public func setupViews() {
        let views = [topContainerView, pointsImageview, pointsTitleLabel, pointsValueLabel, horizontalView, badgesImageview, badgesTitleLabel, badgesValueLabel, activityDetailsContainerView, activityDetailsLabel, barsView, barsColorView, barsColorLabel, statisticsContainerView, statisticsTableView, programPageLabel, shareImageView, shareContainerView, shareWeekSummaryLabel]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        topContainerView.addSubviews([pointsImageview, pointsTitleLabel, pointsValueLabel, horizontalView, badgesImageview, badgesTitleLabel, badgesValueLabel])
        
        topContainerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 22))
        }
        
        topContainerView.addSubviews([pointsImageview, pointsTitleLabel, pointsValueLabel, horizontalView, badgesImageview, badgesTitleLabel, badgesValueLabel])
        
        pointsImageview.snp.makeConstraints { (maker) in
            maker.leading.equalTo(topContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 18))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 13))
            maker.top.equalTo(topContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        pointsValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(topContainerView)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49.5))
            maker.top.equalTo(pointsImageview.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        pointsTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(topContainerView)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49.5))
            maker.top.equalTo(pointsValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        horizontalView.snp.makeConstraints { (maker) in
            maker.top.bottom.equalTo(topContainerView)
            maker.leading.equalTo(pointsTitleLabel.snp.trailing)
            maker.width.equalTo(1)
        }
        
        badgesImageview.snp.makeConstraints { (maker) in
             maker.leading.equalTo(horizontalView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 18))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 13))
            maker.top.equalTo(topContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        badgesValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(horizontalView.snp.trailing)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49.5))
            maker.top.equalTo(badgesImageview.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        badgesTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(horizontalView.snp.trailing)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49.5))
            maker.top.equalTo(badgesValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        activityDetailsContainerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(topContainerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 50))
        }
        
        activityDetailsContainerView.addSubviews([activityDetailsLabel, barsView, barsColorView, barsColorLabel])
        
        activityDetailsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        barsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(activityDetailsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 35))
        }
        
        barsColorView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 30))
            maker.top.equalTo(barsView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
            
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
        }
        
        barsColorLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(barsColorView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(barsView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(activityDetailsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        statisticsContainerView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(activityDetailsContainerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 25))
        }
        
        statisticsContainerView.addSubviews([statisticsTableView])
        
        statisticsTableView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(statisticsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(statisticsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(statisticsContainerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 22))
        }
        
        programPageLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(statisticsContainerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
    }
    
    
    public func populateData() {
        pointsValueLabel.text = String(weekSummary.totalPoints)
        badgesValueLabel.text = String(weekSummary.totalBadges)
        
        statisticsTableView.dataSource = self
        statisticsTableView.delegate = self
        statisticsTableView.reloadData()
        
    }
}

extension WeekSummaryCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeekSummaryStatisticsCell.identifier, for: indexPath) as! WeekSummaryStatisticsCell
        cell.selectionStyle = .none
        cell.weekSummary = self.weekSummary
        cell.setupViews()
        switch indexPath.row {
        case 0:
            if let _ = self.lastWeekSummary {
                var centerImage: UIImage
                
                if (lastWeekSummary?.totalPoints!)! <= weekSummary.totalPoints! {
                   centerImage = UIImage(named: "green_arrow")!
                } else {
                    centerImage = UIImage(named: "red_arrow")!
                }
                cell.populateData(title: "totalPoints".localized(), firstNumber: "\(self.weekSummary.totalPoints!)", lastNumber: "\((lastWeekSummary?.totalPoints!)!)", centerImage: centerImage)
            } else {
                cell.populateData(title: "totalPoints".localized(), firstNumber: "\(self.weekSummary.totalPoints!)", lastNumber: "0", centerImage: UIImage(named: "green_arrow")!)
            }
            
            break
            
        case 1:
            if let _ = self.lastWeekSummary {
                var centerImage: UIImage
                
                if (lastWeekSummary?.totalBadges!)! <= weekSummary.totalBadges! {
                    centerImage = UIImage(named: "green_arrow")!
                } else {
                    centerImage = UIImage(named: "red_arrow")!
                }
                
                cell.populateData(title: "totalBadges".localized(), firstNumber: "\(self.weekSummary.totalBadges!)", lastNumber: "\((lastWeekSummary?.totalBadges!)!)", centerImage: centerImage)
            } else {
                cell.populateData(title: "totalBadges".localized(), firstNumber: "\(self.weekSummary.totalBadges!)", lastNumber: "0", centerImage: UIImage(named: "green_arrow")!)
            }
            break
            
        case 2:
            if let _ = self.lastWeekSummary {
                var centerImage: UIImage
                
                if (lastWeekSummary?.totalCorrectAnswers!)! <= weekSummary.totalCorrectAnswers! {
                    centerImage = UIImage(named: "green_arrow")!
                } else {
                    centerImage = UIImage(named: "red_arrow")!
                }
                
                cell.populateData(title: "totalRightQuestions".localized(), firstNumber: "\(self.weekSummary.totalCorrectAnswers!)", lastNumber: "\((lastWeekSummary?.totalCorrectAnswers!)!)", centerImage: centerImage)
            } else {
                cell.populateData(title: "totalRightQuestions".localized(), firstNumber: "\(self.weekSummary.totalCorrectAnswers!)", lastNumber: "0", centerImage: UIImage(named: "green_arrow")!)
            }
            break
            
        case 3:
            if let _ = self.lastWeekSummary {
                var centerImage: UIImage
                
                if (lastWeekSummary?.totalShares!)! <= weekSummary.totalShares! {
                    centerImage = UIImage(named: "green_arrow")!
                } else {
                    centerImage = UIImage(named: "red_arrow")!
                }
                
                cell.populateData(title: "totalShares".localized(), firstNumber: "\(self.weekSummary.totalShares!)", lastNumber: "\((lastWeekSummary?.totalShares!)!)", centerImage: centerImage)
                
            } else {
                cell.populateData(title: "totalShares".localized(), firstNumber: "\(self.weekSummary.totalShares!)", lastNumber: "0", centerImage: UIImage(named: "green_arrow")!)
            }
            break
            
        default:
            break
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)
    }
}
