//
//  WeekSummaryLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/15/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol WeekSummaryLayoutDelegate: BaseLayoutDelegate {
    func goBack()
    func share()
    func goToHome()
}

public class WeekSummaryLayout: BaseLayout {
    
    var weekSummaryLayoutDelegate: WeekSummaryLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: WeekSummaryLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.weekSummaryLayoutDelegate = delegate
    }
    
    lazy var weekDateLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var weekSummaryTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(WeekSummaryCell.self, forCellReuseIdentifier: WeekSummaryCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [topView, weekSummaryTableView, weekDateLabel]
        
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.weekDateLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.leading.trailing.equalTo(superview)
        }
        
        self.weekSummaryTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(weekDateLabel.snp.bottom)
            maker.leading.trailing.bottom.equalTo(superview)
        }
        
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "weekSummary".localized())
        self.topView.backImageView.image = UIImage(named: "close")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.delegate = self
    }
    
    public func populateData(weekDate: String) {
        weekDateLabel.text = weekDate
    }
    
}

extension WeekSummaryLayout: TopViewDelegate {
    public func goBack() {
        self.weekSummaryLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
