//
//  WeekSummaryVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/15/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftCharts

class WeekSummaryVC: BaseVC {

    var layout: WeekSummaryLayout!
    var presenter: WeekSummaryPresenter!
    var id: Int!
    var weekSummary: WeekSummary!
    var lastWeekSummary: WeekSummary?
    var chart: Chart!
    
    public static func buildVC(id: Int) -> WeekSummaryVC {
        let vc = WeekSummaryVC()
        vc.id = id
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = WeekSummaryLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        
        presenter = Injector.provideWeekSummaryPresenter()
        presenter.setView(view: self)
        presenter.getWeekSummary(id: id)

    }
    
    func createChart(x: CGFloat, y: CGFloat, to: Int, daysPoints: [DayPoint]) {
        let chartConfig = BarsChartConfig(
            valsAxisConfig: ChartAxisConfig(from: 0, to: Double(to + 2), by: 2)
        )
        
        let frame = CGRect(x: x, y: y, width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 96), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 35))
        
        let chart = BarsChart(
            frame: frame,
            chartConfig: chartConfig,
            xTitle: "",
            yTitle: "",
            bars: [
                ("tues".localized(), Double(daysPoints[0].points)),
                ("wed".localized(), Double(daysPoints[1].points)),
                ("thu".localized(), Double(daysPoints[2].points)),
                ("fri".localized(), Double(daysPoints[3].points)),
                ("sat".localized(), Double(daysPoints[4].points)),
                ("sun".localized(), Double(daysPoints[5].points)),
                ("mon".localized(), Double(daysPoints[6].points))
            ],
            color: UIColor.red,
            barWidth: 20
        )
        
        self.chart = chart
    }

}

extension WeekSummaryVC: WeekSummaryView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getWeekSummarySuccess(weekSummary: WeekSummary) {
        print("WeekSummaryID :: \(String(describing: weekSummary.id))")
        self.weekSummary = weekSummary
        let startDate = weekSummary.startDate.components(separatedBy: "T")[0]
        let endDate = weekSummary.endDate.components(separatedBy: "T")[0]
        
        let startDateDay = startDate.components(separatedBy: "-")[2]
        let startDateMonth = startDate.components(separatedBy: "-")[1]
        let startDateMonthName = UiHelpers.getMonthName(monthNumber: Int(startDateMonth)!)
        
        let endDateDay = endDate.components(separatedBy: "-")[2]
        let endDateMonth = endDate.components(separatedBy: "-")[1]
        let endDateMonthName = UiHelpers.getMonthName(monthNumber: Int(endDateMonth)!)
        
        if startDateMonth == endDateMonth {
            layout.populateData(weekDate: "\(startDateDay) - \(endDateDay) \(endDateMonthName)")
        } else {
            layout.populateData(weekDate: "\(startDateDay) \(startDateMonthName) - \(endDateDay) \(endDateMonthName)")
        }
        if weekSummary.weight == 1 {
            layout.weekSummaryTableView.dataSource = self
            layout.weekSummaryTableView.delegate = self
            layout.weekSummaryTableView.reloadData()
        } else {
            self.presenter.getLastWeekSummary(weight: weekSummary.weight + 1)
        }
    }
    
    func getLastWeekSummarySuccess(weekSummary: WeekSummary?) {
        if let _ = weekSummary {
            self.lastWeekSummary = weekSummary
        }
        layout.weekSummaryTableView.dataSource = self
        layout.weekSummaryTableView.delegate = self
        layout.weekSummaryTableView.reloadData()
    }
}

extension WeekSummaryVC: WeekSummaryLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func share() {
        
    }
    
    func goToHome() {
        let mainScreenVC = MainScreenVC.buildVC()
        mainScreenVC.selectedIndex = 0
        self.pushVC(mainScreenVC)
    }
    
    func retry() {
        
    }
}

extension WeekSummaryVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeekSummaryCell.identifier, for: indexPath) as! WeekSummaryCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.weekSummary = self.weekSummary
        cell.lastWeekSummary = self.lastWeekSummary
        cell.setupViews()
        self.createChart(x: cell.barsView.frame.x, y: cell.barsView.frame.x, to: self.weekSummary.totalPoints, daysPoints: self.weekSummary.daysPoints)
        cell.barsView.addSubview(self.chart.view)
        UIView.animate(withDuration: 0, animations: {
            cell.barsView.transform = CGAffineTransform(rotationAngle: CGFloat(0))
        })
        cell.populateData()
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 120)
    }
}

extension WeekSummaryVC: WeekSummaryCellDelegate {
    func goToProgramScreen() {
        let mainScreenVC = MainScreenVC.buildVC()
        mainScreenVC.selectedIndex = 0
        self.navigationController?.viewControllers.append(mainScreenVC)
    }
}
