//
//  WeekSummaryPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/3/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public protocol WeekSummaryView: class {
    func operationFailed(message: String)
    func getWeekSummarySuccess(weekSummary: WeekSummary)
    func getLastWeekSummarySuccess(weekSummary: WeekSummary?)
}

public class WeekSummaryPresenter {
    fileprivate weak var weekSummaryView : WeekSummaryView?
    fileprivate let weekSummaryRepository : WeekSummaryRepository
    
    init(repository: WeekSummaryRepository) {
        self.weekSummaryRepository = repository
        self.weekSummaryRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : WeekSummaryView) {
        weekSummaryView = view
    }
}

extension WeekSummaryPresenter {
    public func getWeekSummary(id: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.weekSummaryRepository.getWeekSummary(id: id)
        } else {
            self.weekSummaryView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getLastWeekSummary(weight: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.weekSummaryRepository.getLastWeekSummary(weight: weight)
        } else {
            self.weekSummaryView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension WeekSummaryPresenter: WeekSummaryPresenterDelegate {
    public func getLastWeekSummarySuccess(weekSummary: WeekSummary?) {
        self.weekSummaryView?.getLastWeekSummarySuccess(weekSummary: weekSummary)
    }
    
    public func operationFailed(message: String) {
        self.weekSummaryView?.operationFailed(message: message)
    }
    
    public func getWeekSummarySuccess(weekSummary: WeekSummary) {
        self.weekSummaryView?.getWeekSummarySuccess(weekSummary: weekSummary)
    }
}
