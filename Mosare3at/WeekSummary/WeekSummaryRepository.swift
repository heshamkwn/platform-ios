//
//  WeekSummaryRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/3/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol WeekSummaryPresenterDelegate: class {
    func operationFailed(message: String)
    func getWeekSummarySuccess(weekSummary: WeekSummary)
    func getLastWeekSummarySuccess(weekSummary: WeekSummary?)
}

public class WeekSummaryRepository {
    var delegate: WeekSummaryPresenterDelegate!
    
    public func setDelegate(delegate: WeekSummaryPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getWeekSummary(id: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_summaries/\(id)/")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let weekSummary = WeekSummary.getInstance(dictionary: json)
                        self.delegate.getWeekSummarySuccess(weekSummary: weekSummary)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getLastWeekSummary(weight: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        let parameters = ["teamMember": Defaults[.teamMemberId]!, "weight" : "\(weight)"]
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_summaries/")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let hydraMember = json["hydra:member"] as! [Dictionary<String, AnyObject>]
                        if hydraMember.count > 0 {
                            let weekSummary = WeekSummary.getInstance(dictionary: hydraMember[0])
                            self.delegate.getLastWeekSummarySuccess(weekSummary: weekSummary)
                        } else {
                            self.delegate.getLastWeekSummarySuccess(weekSummary: nil)
                        }
                        
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
