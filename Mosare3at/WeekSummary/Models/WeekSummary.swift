//
//  WeekSummary.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/3/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class WeekSummary {
    var id: Int!
    var weight: Int!
    var teamMember: String!
    var startDate: String!
    var endDate: String!
    var createDate: String!
    var totalPoints: Int!
    var totalBadges: Int!
    var totalCorrectAnswers: Int!
    var totalShares: Int!
    var totalViews: Int!
    var daysPoints: [DayPoint]!
    var isShared: Bool!
    var shareDate: String!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["id"] = id
        dictionary["weight"] = weight
        dictionary["teamMember"] = teamMember
        dictionary["startDate"] = startDate
        dictionary["endDate"] = endDate
        dictionary["CreateDate"] = createDate
        dictionary["TotalPoints"] = totalPoints
        dictionary["TotalBadges"] = totalBadges
        dictionary["TotalCorrectQuestions"] = totalCorrectAnswers
        dictionary["totalShares"] = totalShares
        dictionary["totalViews"] = totalViews
        
        if let _ = daysPoints {
            var daysPointsDicArray = [Dictionary<String, Any>]()
            
            for dayPoint in daysPoints {
                daysPointsDicArray.append(dayPoint.convertToDictionary())
            }
            dictionary["DaysPoints"] = daysPointsDicArray
        }
        
        dictionary["isShared"] = isShared
        dictionary["shareDate"] = shareDate
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> WeekSummary {
        
        let weekSummary = WeekSummary()
        weekSummary.id =  dictionary["id"] as? Int
        weekSummary.weight =  dictionary["weight"] as? Int
        weekSummary.teamMember =  dictionary["teamMember"] as? String
        weekSummary.startDate =  dictionary["startDate"] as? String
        weekSummary.endDate =  dictionary["endDate"] as? String
        weekSummary.createDate =  dictionary["CreateDate"] as? String
        weekSummary.totalPoints =  dictionary["TotalPoints"] as? Int
        weekSummary.totalBadges =  dictionary["TotalBadges"] as? Int
        weekSummary.totalCorrectAnswers =  dictionary["TotalCorrectQuestions"] as? Int
        weekSummary.totalShares =  dictionary["TotalShares"] as? Int
        weekSummary.totalViews =  dictionary["TotalViews"] as? Int
        
        if let daysPointsDicArray = dictionary["DaysPoints"], daysPointsDicArray is [Dictionary<String, Any>] {
            var daysPointsArray: [DayPoint] = []
            for dic in daysPointsDicArray as! [Dictionary<String, Any>] {
                daysPointsArray.append(DayPoint.getInstance(dictionary: dic))
            }
            weekSummary.daysPoints = daysPointsArray
        }
        
        weekSummary.isShared =  dictionary["isShared"] as? Bool
        weekSummary.shareDate =  dictionary["shareDate"] as? String
        return weekSummary
    }
    
}
