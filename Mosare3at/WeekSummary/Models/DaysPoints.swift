//
//  DaysPoints.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/3/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class DayPoint {
    var date: String!
    var points: Int!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["date"] = date
        dictionary["points"] = points
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> DayPoint {
        
        let dayPoint = DayPoint()
        dayPoint.points =  dictionary["points"] as? Int
        dayPoint.date =  dictionary["date"] as? String
        return dayPoint
    }
}
