//
//  WeekSummaryStatisticsCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 1/20/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift

class WeekSummaryStatisticsCell: UITableViewCell {

    public static let identifier = "WeekSummaryStatisticsCell"
    var superView: UIView!
    
    var weekSummary: WeekSummary!
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var firstLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .right
        } else {
            label.textAlignment = .left
        }
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var centerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "green_arrow")
        return imageView
    }()
    
    lazy var lastLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
            label.text = ")"
        } else {
            label.textAlignment = .right
            label.text = "("
        }
        
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    public func setupViews() {
        let views = [titleLabel, firstLabel, centerImageView, lastLabel]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.bottom.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 70))
        }
        
        lastLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.bottom.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
        }
        
        centerImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(lastLabel.snp.leading)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        firstLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(centerImageView.snp.leading)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.bottom.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 12))
        }
        
        
    }
    
    public func populateData(title: String, firstNumber: String, lastNumber: String, centerImage: UIImage) {
        titleLabel.text = title
        firstLabel.text = "\(lastNumber)) \(firstNumber)"
        self.centerImageView.image = centerImage
        
    }
}
