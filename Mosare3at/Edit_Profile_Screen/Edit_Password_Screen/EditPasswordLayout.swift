//
//  EditPasswordLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Material

public protocol EditPasswordLayoutDelegate: BaseLayoutDelegate {
    func updatePassword()
    func goBack()
}

public class EditPasswordLayout: BaseLayout {
    var editPasswordLayoutDelegate: EditPasswordLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: EditPasswordLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.editPasswordLayoutDelegate = delegate
    }
    
    lazy var currentPasswordField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "currentPassword".localized())
        field.isSecureTextEntry = true
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var currentPasswordVisibilityToggleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.currentPasswordField.isSecureTextEntry = !self.currentPasswordField.isSecureTextEntry
            if self.currentPasswordField.isSecureTextEntry {
                imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
            } else {
                imageView.image = UIImage(named: "eye")!.withRenderingMode(.alwaysTemplate)
            }
            imageView.tintColor = UIColor.AppColors.gray
            
        })
        return imageView
    }()
    
    lazy var newPasswordField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "newPassword".localized())
        field.isSecureTextEntry = true
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var newPasswordVisibilityToggleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.newPasswordField.isSecureTextEntry = !self.newPasswordField.isSecureTextEntry
            if self.newPasswordField.isSecureTextEntry {
                imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
            } else {
                imageView.image = UIImage(named: "eye")!.withRenderingMode(.alwaysTemplate)
            }
            imageView.tintColor = UIColor.AppColors.gray
            
        })
        return imageView
    }()
    
    lazy var newConfirmationPasswordField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "newConfirmPassword".localized())
        field.isSecureTextEntry = true
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var newConfirmationPasswordVisibilityToggleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.newConfirmationPasswordField.isSecureTextEntry = !self.newConfirmationPasswordField.isSecureTextEntry
            if self.newConfirmationPasswordField.isSecureTextEntry {
                imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
            } else {
                imageView.image = UIImage(named: "eye")!.withRenderingMode(.alwaysTemplate)
            }
            imageView.tintColor = UIColor.AppColors.gray
            
        })
        return imageView
    }()
    
    lazy var updatePasswordButton: RaisedButton = {
        let button = RaisedButton(title: "updatePassword".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.green
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 18)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.editPasswordLayoutDelegate.updatePassword()
        }
        return button
    }()
    
    public func setupViews() {
        let views = [currentPasswordField, currentPasswordVisibilityToggleImageView, newPasswordField, newPasswordVisibilityToggleImageView, newConfirmationPasswordField, newConfirmationPasswordVisibilityToggleImageView, updatePasswordButton, topView]
        
        superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.currentPasswordField.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.top.equalTo(topView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            
            maker.height.equalTo(30)
        }
        
        self.currentPasswordVisibilityToggleImageView.snp.makeConstraints { maker in
            maker.trailing.equalTo(currentPasswordField.snp.trailing)
            maker.top.equalTo(currentPasswordField)
            maker.bottom.equalTo(currentPasswordField).offset(-4)
            maker.width.equalTo(20)
        }
        
        self.newPasswordField.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.top.equalTo(currentPasswordField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            
            maker.height.equalTo(30)
        }
        
        self.newPasswordVisibilityToggleImageView.snp.makeConstraints { maker in
            maker.trailing.equalTo(newPasswordField.snp.trailing)
            maker.top.equalTo(newPasswordField)
            maker.bottom.equalTo(newPasswordField).offset(-4)
            maker.width.equalTo(20)
        }
        
        self.newConfirmationPasswordField.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.top.equalTo(newPasswordField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            
            maker.height.equalTo(30)
        }
        
        self.newConfirmationPasswordVisibilityToggleImageView.snp.makeConstraints { maker in
            maker.trailing.equalTo(newConfirmationPasswordField.snp.trailing)
            maker.top.equalTo(newConfirmationPasswordField)
            maker.bottom.equalTo(newConfirmationPasswordField).offset(-4)
            maker.width.equalTo(20)
        }
        
        self.updatePasswordButton.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10) * -1)
            maker.top.equalTo(newConfirmationPasswordField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "updatePassword".localized())
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.delegate = self
    }
}

extension EditPasswordLayout: TopViewDelegate {
    public func goBack() {
        self.editPasswordLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
