//
//  EditPasswordVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

class EditPasswordVC: BaseVC {

    var layout: EditPasswordLayout!
    var presenter: EditProfilePresenter!
    
    public class func buildVC() -> EditPasswordVC {
        return EditPasswordVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = EditPasswordLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        presenter = Injector.provideEditProfilePresenter()
        presenter.setView(view: self)
        
    }

}

extension EditPasswordVC: EditPasswordLayoutDelegate {
    func updatePassword() {
        if (layout.currentPasswordField.text?.isEmpty)! || (layout.newPasswordField.text?.isEmpty)! || (layout.newConfirmationPasswordField.text?.isEmpty)! {
            self.view.makeToast("emptyFieldsError".localized())
        } else if layout.newPasswordField.text != layout.newConfirmationPasswordField.text {
            self.view.makeToast("passwordConfirmError".localized())
        } else {
            presenter.updatePassword(currentPassword: layout.currentPasswordField.text!, newPassword: layout.newPasswordField.text!, newConfirmPassword: layout.newConfirmationPasswordField.text!)
        }
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension EditPasswordVC: EditProfileView {
    func mobileNumberUpdatedSuccess() {
        
    }
    
    func passwordUpdatedSuccess() {
        self.view.makeToast("updatePasswordSuccess".localized(), duration: 1) {
            self.goBack()
        }
    }
    
    func updateAvatarSuccess(message: String) {
        
    }
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
}
