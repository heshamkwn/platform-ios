//
//  EditProfileLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol EditProfileLayoutDelegate: BaseLayoutDelegate {
    func goBack()
}

public class EditProfileLayout: BaseLayout {
    
    var editProfileLayoutDelegate: EditProfileLayoutDelegate!
    
    init(superview: UIView, delegate: EditProfileLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.editProfileLayoutDelegate = delegate
    }
    
    lazy var editProfileTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(EditProfileCell.self, forCellReuseIdentifier: EditProfileCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [editProfileTableView]
        
        self.superview.addSubviews(views)
        self.editProfileTableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(superview)
        }
        
    }
    
}
