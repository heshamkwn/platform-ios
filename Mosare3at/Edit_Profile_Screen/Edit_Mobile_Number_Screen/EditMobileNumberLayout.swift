//
//  EditMobileNumberLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/12/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Material

public protocol EditMobileNumberLayoutDelegate: BaseLayoutDelegate {
    func updateMobileNumber()
    func goBack()
}

public class EditMobileNumberLayout: BaseLayout {
    var editMobileNumberLayoutDelegate: EditMobileNumberLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: EditMobileNumberLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.editMobileNumberLayoutDelegate = delegate
    }
    
    lazy var newMobileNumberField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "newMobileNumber".localized())
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.keyboardType = .phonePad
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var currentPasswordField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "currentPassword".localized())
        field.isSecureTextEntry = true
        field.placeholderNormalColor = UIColor.AppColors.gray
        field.font = AppFont.font(type: .Regular, size: 15)
        return field
    }()
    
    lazy var currentPasswordVisibilityToggleImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.gray
        imageView.backgroundColor = .white
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.currentPasswordField.isSecureTextEntry = !self.currentPasswordField.isSecureTextEntry
            if self.currentPasswordField.isSecureTextEntry {
                imageView.image = UIImage(named: "hidden")!.withRenderingMode(.alwaysTemplate)
            } else {
                imageView.image = UIImage(named: "eye")!.withRenderingMode(.alwaysTemplate)
            }
            imageView.tintColor = UIColor.AppColors.gray
            
        })
        return imageView
    }()
    
    lazy var updateMobileNumberButton: RaisedButton = {
        let button = RaisedButton(title: "updateMobileNumber".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.green
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 18)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.editMobileNumberLayoutDelegate.updateMobileNumber()
        }
        return button
    }()
    
    public func setupViews() {
        let views = [topView, newMobileNumberField, currentPasswordField, currentPasswordVisibilityToggleImageView, updateMobileNumberButton]
        
        superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.newMobileNumberField.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.top.equalTo(topView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            
            maker.height.equalTo(30)
        }
        
        self.currentPasswordField.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5) * -1)
            maker.top.equalTo(newMobileNumberField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            
            maker.height.equalTo(30)
        }
        
        self.currentPasswordVisibilityToggleImageView.snp.makeConstraints { maker in
            maker.trailing.equalTo(currentPasswordField.snp.trailing)
            maker.top.equalTo(currentPasswordField)
            maker.bottom.equalTo(currentPasswordField).offset(-4)
            maker.width.equalTo(20)
        }
        
        self.updateMobileNumberButton.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10))
            maker.trailing.equalTo(superview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10) * -1)
            maker.top.equalTo(currentPasswordField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "updateMobileNumber".localized())
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.delegate = self
    }
}

extension EditMobileNumberLayout: TopViewDelegate {
    public func goBack() {
        self.editMobileNumberLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
