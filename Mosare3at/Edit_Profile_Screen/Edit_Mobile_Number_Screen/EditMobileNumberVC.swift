//
//  EditMobileNumberVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/12/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

class EditMobileNumberVC: BaseVC {

    var layout: EditMobileNumberLayout!
    var presenter: EditProfilePresenter!
    
    public class func buildVC() -> EditMobileNumberVC {
        return EditMobileNumberVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = EditMobileNumberLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        presenter = Injector.provideEditProfilePresenter()
        presenter.setView(view: self)
    }

}

extension EditMobileNumberVC: EditMobileNumberLayoutDelegate {
    func updateMobileNumber() {
        if (layout.currentPasswordField.text?.isEmpty)! || (layout.newMobileNumberField.text?.isEmpty)! {
            self.view.makeToast("emptyFieldsError".localized())
        } else {
            presenter.updateMobileNumber(currentPassword: layout.currentPasswordField.text!, newMobileNumber: layout.newMobileNumberField.text!)
        }
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension EditMobileNumberVC: EditProfileView {
    func mobileNumberUpdatedSuccess() {
        self.view.makeToast("updateMobileNumberSuccess".localized(), duration: 1) {
            self.goBack()
        }
    }
    
    func passwordUpdatedSuccess() {
        
    }
    
    func updateAvatarSuccess(message: String) {
        
    }
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
}

