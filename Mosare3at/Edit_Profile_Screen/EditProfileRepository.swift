//
//  EditProfileRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol EditProfilePresenterDelegate: class {
    func updateAvatarSuccess(message: String)
    func operationFailed(message: String)
    func passwordUpdatedSuccess()
    func mobileNumberUpdatedSuccess()
}

public class EditProfileRepository {
    var delegate: EditProfilePresenterDelegate!
    
    public func setDelegate(delegate: EditProfilePresenterDelegate) {
        self.delegate = delegate
    }
    
    public func updateUserAvatar(imageName: String, userId: Int) {
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        let headers = ["Content-Type" : "application/json", "X-AUTH-TOKEN" : "\(user.token!)"]
        let parameters = ["profilePic" : imageName]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "users/\(userId)")!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.updateAvatarSuccess(message: "updateAvatarSuccess".localized())
                    // update the user's profile image using the uploaded one
                    let user = User.getInstance(dictionary: Defaults[.user]!)
                    user.profilePic = imageName
                    Defaults[.user] = user.convertToDictionary()
                } else {
                    self.delegate.operationFailed(message: "updateAvatarFailed".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func uploadUserImageToServer(imageData: Data) {
        let fileName = "image.png"
        let mimeType = "image/png"
        UiHelpers.showLoader()
        Alamofire.upload(multipartFormData:{ multipartFormData in
            multipartFormData.append(imageData, withName: "file", fileName: fileName, mimeType: mimeType)
        }, to:"\(CommonConstants.BASE_URL)media/upload", method:.post, headers : nil, encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    if let dictionary = response.value as? Dictionary<String, Any> {
                        let name = dictionary["name"] as! String
                        let user = User.getInstance(dictionary: Defaults[.user]!)
                        self.updateUserAvatar(imageName: name, userId: user.id)
                    } else {
                        self.delegate.operationFailed(message: "updateAvatarFailed".localized())
                    }
                }
            case .failure(let encodingError):
                self.delegate.operationFailed(message: encodingError as! String)
            }
        })
    }
    
    public func updatePassword(currentPassword: String, newPassword: String, newConfirmPassword: String) {
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        let headers = ["X-AUTH-TOKEN" : "\(user.token!)"]
        let parameters = ["cur_password" : currentPassword, "password" : newPassword, "repassword" : newConfirmPassword]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "reset-password")!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.passwordUpdatedSuccess()
                } else {
                    self.delegate.operationFailed(message: "updateAvatarFailed".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func updateMobileNumber(currentPassword: String, newMobileNumber: String) {
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        let headers = ["X-AUTH-TOKEN" : "\(user.token!)"]
        let parameters = ["cur_password" : currentPassword, "mobile" : newMobileNumber]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "user/\(user.id!)")!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.mobileNumberUpdatedSuccess()
                } else {
                    self.delegate.operationFailed(message: "updateAvatarFailed".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
