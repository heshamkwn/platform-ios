//
//  EditProfilePresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol EditProfileView : class {
    func updateAvatarSuccess(message: String)
    func operationFailed(message: String)
    func passwordUpdatedSuccess()
    func mobileNumberUpdatedSuccess()
}

public class EditProfilePresenter {
    fileprivate weak var editProfileView : EditProfileView?
    fileprivate let editProfileRepository : EditProfileRepository
    
    init(repository: EditProfileRepository) {
        self.editProfileRepository = repository
        self.editProfileRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : EditProfileView) {
        editProfileView = view
    }
}

extension EditProfilePresenter {
    
    public func uploadUserImageToServer(image: UIImage) {
        if UiHelpers.isInternetAvailable() {
             UiHelpers.showLoader()
            self.editProfileRepository.uploadUserImageToServer(imageData: image.jpeg(.lowest)!)
        } else {
            self.editProfileView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func updatePassword(currentPassword: String, newPassword: String, newConfirmPassword: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.editProfileRepository.updatePassword(currentPassword: currentPassword, newPassword: newPassword, newConfirmPassword: newConfirmPassword)
        } else {
            self.editProfileView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
     public func updateMobileNumber(currentPassword: String, newMobileNumber: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.editProfileRepository.updateMobileNumber(currentPassword: currentPassword, newMobileNumber: newMobileNumber)
        } else {
            self.editProfileView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension EditProfilePresenter: EditProfilePresenterDelegate {
    public func passwordUpdatedSuccess() {
        UiHelpers.hideLoader()
        self.editProfileView?.passwordUpdatedSuccess()
    }
    
    public func updateAvatarSuccess(message: String) {
        UiHelpers.hideLoader()
        self.editProfileView?.updateAvatarSuccess(message: message)
    }
    
    public func operationFailed(message: String) {
        UiHelpers.hideLoader()
        self.editProfileView?.operationFailed(message: message)
    }
    
    public func mobileNumberUpdatedSuccess() {
        UiHelpers.hideLoader()
        self.editProfileView?.mobileNumberUpdatedSuccess()
    }
}
