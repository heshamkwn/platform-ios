//
//  EditProfileCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Material

public protocol EditProfileCellDelegate: class {
    func close()
    func goToEditPasswordScreen()
    func goToEditMobileNumberScreen()
    func chooseImage()
}

class EditProfileCell: UITableViewCell {

    public static let identifier = "EditProfileCell"
    var superView: UIView!
    
    var delegate: EditProfileCellDelegate!
    
    var user: User!
    var choosenImage: UIImage!
    
    lazy var closeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "close")
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.delegate.close()
        })
        return imageView
    }()
    
    lazy var userProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 23) / 2
        return imageView
    }()
    
    lazy var changeImageView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.darkRed
        view.clipsToBounds = true
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)/2
        view.addTapGesture(action: { (_) in
            self.delegate.chooseImage()
        })
        return view
    }()
    
    lazy var changeImageLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .white
        lbl.numberOfLines = 1
        lbl.text = "+"
        lbl.textAlignment = .center
        lbl.font = AppFont.font(type: .Regular, size: 16)
        return lbl
    }()
    
    lazy var mainUserProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var curveImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "curve")
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    lazy var emailImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "email_icon")
        return imageView
    }()
    
    lazy var emailTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.text = "email".localized()
        lbl.font = AppFont.font(type: .Regular, size: 16)
        return lbl
    }()
    
    lazy var emailValueLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.font = AppFont.font(type: .Bold, size: 14)
        return lbl
    }()
    
    lazy var emailLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        return view
    }()
    
    lazy var passwordImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "password_icon")
        return imageView
    }()
    
    lazy var passwordTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.text = "password".localized()
        lbl.font = AppFont.font(type: .Regular, size: 16)
        return lbl
    }()
    
    lazy var passwordValueLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.text = "********"
        lbl.font = AppFont.font(type: .Bold, size: 14)
        return lbl
    }()
    
    lazy var passwordLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        return view
    }()
    
    lazy var passwordArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "left_arrow")
        return imageView
    }()
    
    lazy var passwordView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.addTapGesture(action: { (_) in
            self.delegate.goToEditPasswordScreen()
        })
        return view
    }()
    
    lazy var mobileNumberImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "phone_icon")
        return imageView
    }()
    
    lazy var mobileNumberTitleLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.text = "phoneNumber".localized()
        lbl.font = AppFont.font(type: .Regular, size: 16)
        return lbl
    }()
    
    lazy var mobileNumberValueLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.font = AppFont.font(type: .Bold, size: 14)
        return lbl
    }()
    
    lazy var mobileNumberLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.gray
        return view
    }()
    
    lazy var mobileNumberArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "left_arrow")
        return imageView
    }()
    
    lazy var mobileNumberView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.addTapGesture(action: { (_) in
            self.delegate.goToEditMobileNumberScreen()
        })
        return view
    }()

    lazy var connectedApplicationsLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.text = "connectedApplications".localized()
        lbl.font = AppFont.font(type: .Bold, size: 16)
        return lbl
    }()
    
    lazy var connectedApplicationsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        view.layer.cornerRadius = 6
        view.addTapGesture(action: { (_) in
            UiHelpers.openSlack()
        })
        return view
    }()
    
    lazy var connectedApplicationsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "slack_ic")
        return imageView
    }()
    
    lazy var logoutButton: RaisedButton = {
        let button = RaisedButton(title: "logout".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.darkRed
        button.titleLabel?.font = AppFont.font(type: .Bold, size: 18)
        
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 1)
        button.addTapGesture { recognizer in
            AppDelegate.logout(view: self.superView)
        }
        return button
    }()
    
    public func setupViews() {
        let views = [closeImageView, userProfilePicImageView, mainUserProfilePicImageView, curveImageView, emailImageView, emailImageView, emailTitleLabel, emailLineView, emailValueLabel, passwordLineView, passwordImageView, passwordTitleLabel, passwordValueLabel, passwordArrowImageView, mobileNumberLineView, mobileNumberImageView, mobileNumberTitleLabel, mobileNumberValueLabel, mobileNumberArrowImageView, connectedApplicationsLabel, connectedApplicationsView, connectedApplicationsImageView, logoutButton, changeImageView, changeImageLabel, passwordView, mobileNumberView]
        
        superView = self.contentView
        superView.addSubviews(views)
        
        self.mainUserProfilePicImageView.snp.makeConstraints { maker in
            maker.leading.trailing.top.equalTo(self.superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 65))
        }
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = mainUserProfilePicImageView.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mainUserProfilePicImageView.addSubview(blurEffectView)
        
        superView.bringSubviewToFront(closeImageView)
        superView.bringSubviewToFront(userProfilePicImageView)
        superView.bringSubviewToFront(curveImageView)
        
        
        self.closeImageView.snp.makeConstraints { maker in
            maker.leading.equalTo(self.superView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(self.superView.snp.top).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        self.userProfilePicImageView.snp.makeConstraints { maker in
            maker.centerX.equalTo(self.superView)
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 15))
            maker.height.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 23))
        }
        
        self.changeImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(userProfilePicImageView)
            maker.top.equalTo(userProfilePicImageView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 17))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        self.changeImageLabel.snp.makeConstraints { (maker) in
            maker.center.equalTo(changeImageView)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        superView.bringSubviewToFront(changeImageView)
        superView.bringSubviewToFront(changeImageLabel)
        
        self.curveImageView.snp.makeConstraints { maker in
            maker.leading.trailing.equalTo(self.superView)
            maker.top.equalTo(mainUserProfilePicImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        superView.bringSubviewToFront(emailImageView)
        superView.bringSubviewToFront(emailTitleLabel)
        superView.bringSubviewToFront(emailValueLabel)
        superView.bringSubviewToFront(emailLineView)
        
        emailImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(curveImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
        }
        
        emailTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(emailImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(emailImageView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        emailValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(emailImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(emailTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        emailLineView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(emailImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(emailValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(2)
            maker.trailing.equalTo(superView)
        }
        
        passwordImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(emailLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
        }
        
        passwordTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(passwordImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(emailLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        passwordArrowImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 7))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.top.equalTo(emailLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        passwordValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(passwordImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(passwordTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(passwordArrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        passwordLineView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(passwordImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(passwordValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(2)
            maker.trailing.equalTo(superView)
        }
        
        passwordView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(emailLineView.snp.bottom)
            maker.bottom.equalTo(passwordLineView)
        }
        
        mobileNumberImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(passwordLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
        }
        
        mobileNumberTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(mobileNumberImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(passwordLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        mobileNumberArrowImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 7))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.top.equalTo(passwordLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        mobileNumberValueLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(mobileNumberImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(mobileNumberTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(mobileNumberArrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        mobileNumberLineView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(mobileNumberImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 3))
            maker.top.equalTo(mobileNumberValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(2)
            maker.trailing.equalTo(superView)
        }
        
        mobileNumberView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(passwordLineView.snp.bottom)
            maker.bottom.equalTo(mobileNumberLineView)
        }
        
        connectedApplicationsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(mobileNumberLineView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        self.connectedApplicationsView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2.5))
            maker.top.equalTo(connectedApplicationsLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        superView.bringSubviewToFront(passwordView)
        superView.bringSubviewToFront(mobileNumberView)
        
        self.connectedApplicationsImageView.snp.makeConstraints { (maker) in
            maker.center.equalTo(connectedApplicationsView)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        logoutButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(connectedApplicationsView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
    }
    
    public func populateData() {
        if choosenImage != nil {
            mainUserProfilePicImageView.image = choosenImage
            userProfilePicImageView.image = choosenImage
        } else {
            mainUserProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + user.profilePic)!)
            userProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + user.profilePic)!)
        }
        
        emailValueLabel.text = user.email
        mobileNumberValueLabel.text = user.mobile
    }
}
