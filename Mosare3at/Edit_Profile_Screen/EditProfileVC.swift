//
//  EditProfileVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/11/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults
import AVKit

class EditProfileVC: BaseVC {

    var layout: EditProfileLayout!
    var imagePicker = UIImagePickerController()
    let user = User.getInstance(dictionary: Defaults[.user]!)
    
    var choosenImage: UIImage!
    var presenter: EditProfilePresenter!
    
    public class func buildVC() -> EditProfileVC {
        return EditProfileVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = EditProfileLayout(superview: self.view, delegate: self)
        layout.setupViews()
        layout.editProfileTableView.dataSource = self
        layout.editProfileTableView.delegate = self
        layout.editProfileTableView.reloadData()
        
        presenter = Injector.provideEditProfilePresenter()
        presenter.setView(view: self)
    }
    
    func chooseImageFromGallery() {
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            //already authorized
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                        self.imagePicker.delegate = self
                        self.imagePicker.sourceType = .photoLibrary;
                        self.imagePicker.allowsEditing = false
                        self.present(self.imagePicker, animated: true, completion: nil)
                    }
                } else {
                    //access denied
                }
            })
        }
    }

}

extension EditProfileVC: EditProfileView {
    func mobileNumberUpdatedSuccess() {
        
    }
    
    func passwordUpdatedSuccess() {
        
    }
    
    func updateAvatarSuccess(message: String) {
        self.view.makeToast(message)
    }
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
}

extension EditProfileVC : UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        picker.dismiss(animated: true, completion: { () -> Void in
            self.choosenImage = image
            self.layout.editProfileTableView.reloadData()
            self.presenter.uploadUserImageToServer(image: image)
            // upload image to server
            // update user image with new url like avatar
        })
    }
    
    @objc func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: { () -> Void in
            print("cancelled")
        })
    }
}

extension EditProfileVC: EditProfileLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension EditProfileVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EditProfileCell = layout.editProfileTableView.dequeueReusableCell(withIdentifier: EditProfileCell.identifier, for: indexPath) as! EditProfileCell
        cell.selectionStyle = .none
        cell.user = user
        cell.choosenImage = choosenImage
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 120)
    }
}

extension EditProfileVC: EditProfileCellDelegate {
    func chooseImage() {
        chooseImageFromGallery()
    }
    
    func close() {
        self.goBack()
    }
    
    func goToEditPasswordScreen() {
        navigator.navigateToEditPassword()
    }
    
    func goToEditMobileNumberScreen() {
        navigator.navigateToEditMobile()
    }
}
