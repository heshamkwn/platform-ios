//
//  FiltersVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 11/19/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import DropDown

public protocol FiltersDelegate: class {
    func applyFilters(selectedProgramIndex: Int, selectedProjectIndex: Int, selectedTeamIndex: Int, selectedOrderIndex: Int)
}

class FiltersVC: BaseVC {

    var delegate: FiltersDelegate!
    
    var programs: [Program]!
    var selectedProgramIndex: Int = -1
    
    var projects: [Project]!
    var selectedProjectIndex: Int = -1
    
    var teams: [Team]!
    var selectedTeamIndex: Int = -1
    
    var orders: [String] = ["asc".localized(), "desc".localized()]
    var selectedOrderIndex: Int = 0
    
    var layout: FiltersLayout!
    
    public static func buildVC() -> FiltersVC {
        let vc = FiltersVC()
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = FiltersLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        if let _ = programs {
            if selectedProgramIndex > -1 {
                self.layout.programTextField.text = self.programs.get(at: self.selectedProgramIndex)?.title
            }
        }
        
        if let _ = projects {
            if selectedProjectIndex > -1 {
                self.layout.projectTextField.text = self.projects.get(at: self.selectedProjectIndex)?.title
            }
        }
        
        if let _ = teams {
            if selectedTeamIndex > -1 {
                self.layout.teamTextField.text = self.teams.get(at: self.selectedTeamIndex)?.name
            }
        }
        
        if selectedOrderIndex > 0 {
            self.layout.orderTextField.text = self.orders.get(at: self.selectedOrderIndex)!
        }
        
    }
}

extension FiltersVC: FiltersLayoutDelegate {
    
    func setupProgramsDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.programTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.programTextField.bounds.height)
        var dataSource = ["allPrograms".localized()]
        
        if let _ = programs {
            
            if selectedProgramIndex > -1 {
                self.layout.programTextField.text = self.programs.get(at: self.selectedProgramIndex)?.title
            }
            
            for program in programs {
                dataSource.append(program.title)
            }
            dropDown.dataSource = dataSource
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                if index == 0 {
                    self.selectedProgramIndex = -1
                    self.layout.programTextField.text = "allPrograms".localized()
                } else {
                    self.selectedProgramIndex = index - 1
                    self.layout.programTextField.text = self.programs.get(at: self.selectedProgramIndex)?.title
                }
            }
        }
        dropDown.show()
    }
    
    func retry() {
        
    }
    
    func setupProjectsDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.projectTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.projectTextField.bounds.height)
        
        var dataSource = ["allProjects".localized()]
        
        if let _ = projects {
            
            if selectedProjectIndex > -1 {
                self.layout.projectTextField.text = self.projects.get(at: self.selectedProjectIndex)?.title
            }
            for project in projects {
                dataSource.append(project.title)
            }
            
            dropDown.dataSource = dataSource
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                if index == 0 {
                    self.selectedProjectIndex = -1
                    self.layout.projectTextField.text = "allProjects".localized()
                } else {
                    self.selectedProjectIndex = index - 1
                    self.layout.projectTextField.text = self.projects.get(at: self.selectedProjectIndex)?.title
                }
                
            }
        }
        dropDown.show()
    }
    
    func setupOrdersDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.orderTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.orderTextField.bounds.height)
        dropDown.dataSource = orders
        
        if selectedOrderIndex > 0 {
            self.layout.orderTextField.text = self.orders.get(at: self.selectedOrderIndex)!
        }
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedOrderIndex = index
            self.layout.orderTextField.text = self.orders.get(at: self.selectedOrderIndex)!
        }
        dropDown.show()
    }
    
    func setupTeamsDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.teamTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.teamTextField.bounds.height)
        var dataSource = ["allTeams".localized()]
        if let _ = teams {
            for team in teams {
                dataSource.append(team.name)
            }
            
            dropDown.dataSource = dataSource
            if selectedTeamIndex != -1 {
                self.layout.teamTextField.text = self.teams.get(at: self.selectedTeamIndex)?.name
            }
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                if index == 0 {
                    self.selectedTeamIndex = -1
                    self.layout.teamTextField.text = "allTeams".localized()
                } else {
                    self.selectedTeamIndex = index - 1
                    self.layout.teamTextField.text = self.teams.get(at: self.selectedTeamIndex)?.name
                }
                
            }
        }
        dropDown.show()
    }
    
    func applyFilters() {
        self.dismiss(animated: true) {
            self.delegate.applyFilters(selectedProgramIndex: self.selectedProgramIndex, selectedProjectIndex: self.selectedProjectIndex, selectedTeamIndex: self.selectedTeamIndex, selectedOrderIndex: self.selectedOrderIndex)
        }
    }
    
    func cancel() {
        dismiss(animated: true, completion: nil)
    }
}
