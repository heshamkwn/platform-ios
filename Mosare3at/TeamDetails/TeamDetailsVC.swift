//
//  TeamDetailsVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/1/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

class TeamDetailsVC: BaseVC {

    var team: Team!
    var layout: TeamDetailsLayout!
    
    public static func buildVC(team: Team) -> TeamDetailsVC {
        let vc = TeamDetailsVC()
        vc.team = team
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        layout = TeamDetailsLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        layout.teamDetailsTableView.dataSource = self
        layout.teamDetailsTableView.delegate = self
        layout.teamDetailsTableView.reloadData()
    }

}

extension TeamDetailsVC: TeamDetailsLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension TeamDetailsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TeamDetailsCell = self.layout.teamDetailsTableView.dequeueReusableCell(withIdentifier: TeamDetailsCell.identifier, for: indexPath) as! TeamDetailsCell
        
        cell.selectionStyle = .none
        cell.team = team
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) * CGFloat(team.teamMembers.count) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 50) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 30) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5)
    }
}

extension TeamDetailsVC: TeamDetailsCellDelegate {
    func goToMemberDetails(index: Int, isTeamMate: Bool) {
        self.navigator.navigateToMemberDetails(member: self.team.teamMembers.get(at: index)!, isTeamMate: isTeamMate)
    }
    
    func close() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func openMoreVideos() {
        
    }
    
    func playVideo(index: Int) {
        
    }
}
