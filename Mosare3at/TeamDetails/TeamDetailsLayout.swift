//
//  TeamDetailsLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/1/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol TeamDetailsLayoutDelegate: BaseLayoutDelegate {
    func goBack()
}

public class TeamDetailsLayout: BaseLayout {
    
    var teamDetailsLayoutDelegate: TeamDetailsLayoutDelegate!
    
    init(superview: UIView, delegate: TeamDetailsLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.teamDetailsLayoutDelegate = delegate
    }
    
    lazy var teamDetailsTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(TeamDetailsCell.self, forCellReuseIdentifier: TeamDetailsCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [teamDetailsTableView]
        
        self.superview.addSubviews(views)
        self.teamDetailsTableView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(superview)
        }
        
    }
    
}
