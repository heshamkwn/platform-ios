//
//  TeamDetailsCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/1/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

public protocol TeamDetailsCellDelegate: class {
    func close()
    func openMoreVideos()
    func playVideo(index: Int)
    func goToMemberDetails(index: Int, isTeamMate: Bool)
}

class TeamDetailsCell: UITableViewCell {

    var team: Team!
    var videos: [Video]!
    
    public static let identifier = "TeamDetailsCell"
    var superView: UIView!
    
    var delegate: TeamDetailsCellDelegate!
    
    lazy var closeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "back_icon_arabic")
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            self.delegate.close()
        })
        return imageView
    }()
    
    lazy var mainTeamProfilePicView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexString: "#000000")?.withAlphaComponent(0.5)
        return view
    }()
    
    lazy var mainTeamProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    lazy var teamPicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = .white
        return imageView
    }()
    
    lazy var teamNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var projectNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var pointsDiffLabel: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var pointsView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.alphaBlack
        view.clipsToBounds = true
        view.addBorder(width: 2, color: UIColor.AppColors.darkRed)
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)/2
        return view
    }()
    
    lazy var pointsTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "point".localized()
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var pointsValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var rankingView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.alphaBlack
        view.clipsToBounds = true
        view.addBorder(width: 2, color: UIColor.AppColors.darkRed)
        view.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)/2
        return view
    }()
    
    lazy var rankingTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "rank".localized()
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var rankingValueLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var membersView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var membersLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .right
        label.text = "team".localized()
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var membersTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.isScrollEnabled = false
        tableView.register(MemberCell.self, forCellReuseIdentifier: MemberCell.identifier)
        return tableView
    }()
    
    lazy var videosView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var videosLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .right
        label.text = "videos".localized()
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    var videosCollectionView: UICollectionView = UICollectionView(frame: CGRect(),collectionViewLayout: UICollectionViewFlowLayout())
    
    lazy var noVideosLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.gray
        label.textAlignment = .center
        label.isHidden = true
        label.text = "noVideos".localized()
        label.font = AppFont.font(type: .Bold, size: 15)
        return label
    }()
    
    lazy var moreVideosImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .white
        imageView.image = UIImage(named: "left_arrow")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.darkRed
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 20)/2
        imageView.contentMode = .scaleToFill
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            self.delegate.openMoreVideos()
        })
        return imageView
    }()
    
    public func setupViews() {
        superView = self.contentView
        
        let views = [closeImageView, mainTeamProfilePicImageView, mainTeamProfilePicView, teamPicImageView, teamNameLabel, projectNameLabel, pointsDiffLabel, pointsView, pointsTitleLabel, pointsValueLabel, rankingView, rankingTitleLabel, rankingValueLabel, membersView, membersLabel, membersTableView, videosLabel, videosView, videosCollectionView, noVideosLabel, moreVideosImageview]
        
        superView.addSubviews(views)
        
        mainTeamProfilePicImageView.snp.makeConstraints { (maker) in
            maker.top.leading.trailing.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 50))
        }
        
        mainTeamProfilePicView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(mainTeamProfilePicImageView)
        }
        
        self.closeImageView.snp.makeConstraints { maker in
            maker.leading.equalTo(superView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(superView.snp.top).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        superView.bringSubviewToFront(closeImageView)
        
        teamPicImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 20))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 42))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        teamNameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(teamPicImageView)
            maker.leading.equalTo(teamPicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 70))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        projectNameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(teamNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.trailing.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        pointsDiffLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(projectNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.trailing.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        pointsView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superView.snp.centerX).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(pointsDiffLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
        }
        
        pointsView.addSubviews([pointsTitleLabel, pointsValueLabel])
        
        pointsValueLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(pointsView)
            maker.top.equalTo(pointsView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        pointsTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(pointsView)
            maker.top.equalTo(pointsValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        rankingView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView.snp.centerX).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.top.equalTo(pointsDiffLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10))
        }
        
        rankingView.addSubviews([rankingTitleLabel, rankingValueLabel])
        
        rankingValueLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(rankingView)
            maker.top.equalTo(rankingView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        rankingTitleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(rankingView)
            maker.top.equalTo(rankingValueLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2.5))
        }
        
        membersView.snp.makeConstraints { (maker) in
            maker.top.equalTo(mainTeamProfilePicImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8) + (UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) * CGFloat(self.team.teamMembers.count)))
        }
        
        membersView.addSubviews([membersLabel, membersTableView])
        
        membersLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(membersView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(membersView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(membersView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        membersTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(membersLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(membersView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(membersView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12) * CGFloat(self.team.teamMembers.count))
        }
        
        videosView.snp.makeConstraints { (maker) in
            maker.top.equalTo(membersView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 30))
        }
        
        videosView.addSubviews([videosLabel, videosCollectionView, noVideosLabel, moreVideosImageview])
        
        videosLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(videosView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(videosView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(videosView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        videosCollectionView.snp.makeConstraints { (maker) in
            maker.top.equalTo(videosLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(videosView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(videosView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 23))
        }
        
        noVideosLabel.snp.makeConstraints { (maker) in
            maker.center.equalTo(videosCollectionView)
            maker.leading.trailing.equalTo(videosCollectionView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        moreVideosImageview.snp.makeConstraints { (maker) in
            maker.bottom.equalTo(videosView)
            maker.trailing.equalTo(videosView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
    }
    
    public func populateData() {
        mainTeamProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + team.image)!)
        teamNameLabel.text = team.name
        projectNameLabel.text = "\("aProject".localized()) \(team.project.title!)"
        var pointsString = ""
        if team.pointsDiff > 0 {
            pointsString = "(" + "+" + "\(team.pointsDiff!)" + ")" + "difference".localized()
        } else if team.pointsDiff < 0 {
            pointsString = "(" + "-" + "\(team.pointsDiff!)" + ")" + "difference".localized()
        }
        pointsDiffLabel.text = pointsString
        pointsValueLabel.text = "\(team.points!)"
        rankingValueLabel.text = "\(team.rank!)"
        
        membersTableView.dataSource = self
        membersTableView.delegate = self
        membersTableView.reloadData()
        
        if let _ = videos, videos.count > 0 {
            videosCollectionView.isHidden = false
            noVideosLabel.isHidden = true
            
            videosCollectionView.backgroundColor = .clear
            videosCollectionView.dataSource = self
            videosCollectionView.delegate = self
            videosCollectionView.register(MemberVideoCell.self, forCellWithReuseIdentifier: MemberVideoCell.identifier)
            videosCollectionView.showsHorizontalScrollIndicator = false
            videosCollectionView.isScrollEnabled = true
            videosCollectionView.contentSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 100), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 22))
            if let layout = videosCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
                layout.itemSize = CGSize(width: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 30), height: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 30))
            }
            
            videosCollectionView.reloadData()
            
            let lastVideoIndex = IndexPath(item: videos.count - 1, section: 0)
            videosCollectionView.scrollToItem(at: lastVideoIndex, at: UICollectionView.ScrollPosition.right, animated: true)
        } else {
            videosCollectionView.isHidden = true
            noVideosLabel.isHidden = false
        }
    }
}

extension TeamDetailsCell: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return team.teamMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MemberCell = self.membersTableView.dequeueReusableCell(withIdentifier: MemberCell.identifier, for: indexPath) as! MemberCell
        
        cell.selectionStyle = .none
        
        cell.user = User.getInstance(dictionary: Defaults[.user]!)
        cell.index = indexPath.row
        cell.member = team.teamMembers.get(at: indexPath.row)!
        
        cell.tabIndex = 0
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 12)
    }
}

extension TeamDetailsCell: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let _ = videos {
            return videos.count
        }
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MemberVideoCell.identifier, for: indexPath) as! MemberVideoCell
        cell.video = videos.get(at: indexPath.row)
        cell.delegate = self
        cell.index = indexPath.row
        cell.setupView()
        cell.populateData()
        return cell
    }
}

extension TeamDetailsCell: MemberCellDelegate {
    func goToMyProfile() {
        
    }
    
    func goToMyTeam(index: Int) {
        
    }
    
    func goToMemberDetails(index: Int, isTeamMate: Bool) {
        self.delegate.goToMemberDetails(index: index, isTeamMate: isTeamMate)
    }
    
    func goToTeamDetails(index: Int) {
        
    }
}

extension TeamDetailsCell: MemberVideoCellDelegate {
    func playVideo(index: Int) {
        self.delegate.playVideo(index: index)
    }
}
