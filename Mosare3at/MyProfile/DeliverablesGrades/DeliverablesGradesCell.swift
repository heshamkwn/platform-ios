//
//  DeliverablesGradesCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/12/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol DeliverablesGradesCellDelegate: class {
    func openFeedback(index: Int)
    func goToDeliverableDetails(index: Int)
}

class DeliverablesGradesCell: UITableViewCell {

    public static let identifier = "DeliverablesGradesCell"
    var superView: UIView!
    
    var weekDeliverable :WeekDeliverable!
    var delegate: DeliverablesGradesCellDelegate!
    var index: Int!
    
    lazy var deliverableTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var mediaTypeImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) / 2
        return imageView
    }()
    
    lazy var deliverableMediaTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 12)
        return label
    }()
    
    lazy var belongsToImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    lazy var belongsToLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Bold, size: 12)
        return label
    }()
    
    lazy var commentsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "comments_icon")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.AppColors.darkRed
        return imageView
    }()
    
    lazy var commentsNumberLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.darkRed
        label.textAlignment = .center
        label.backgroundColor = .clear
        label.text = "comments".localized()
        label.font = AppFont.font(type: .Regular, size: 11)
        return label
    }()
    
    lazy var commentsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.darkRed
        label.textAlignment = .center
        let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        let underlineAttributedString = NSAttributedString(string: "comments".localized(), attributes: underlineAttribute)
        label.attributedText = underlineAttributedString
        label.font = AppFont.font(type: .Bold, size: 12)
        label.addTapGesture(action: { (_) in
            self.delegate.openFeedback(index: self.index)
        })
        return label
    }()
    
    lazy var deliverableGradeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        label.clipsToBounds = true
        label.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7)/2
        label.addBorder(width: 2, color: UIColor.AppColors.darkRed)
        return label
    }()
    
    public func setupViews() {
        let views = [deliverableTitleLabel, mediaTypeImageView, deliverableMediaTypeLabel, belongsToImageView, belongsToLabel, commentsImageView, commentsNumberLabel, commentsLabel, deliverableGradeLabel]
        
        self.superView = self.contentView
        
        superView.addTapGesture { (_) in
            self.delegate.goToDeliverableDetails(index: self.index)
        }
        
        self.superView.addSubviews(views)
        
        deliverableTitleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 12) * -1)
            maker.height.equalTo(weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: deliverableTitleLabel.font))
        }
        
        mediaTypeImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        deliverableMediaTypeLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(mediaTypeImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
        }
        
        belongsToImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
    
            maker.leading.equalTo(deliverableMediaTypeLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        belongsToLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(belongsToImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10))
        }
        
        commentsImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1.5))
            
            maker.leading.equalTo(belongsToLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        commentsNumberLabel.snp.makeConstraints { (maker) in
            maker.edges.equalTo(commentsImageView)
        }
        
        commentsLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(deliverableTitleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(commentsImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
        }
        
        deliverableGradeLabel.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(superView)
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2.5) * -1)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
        }
    }
    
    public func populateData() {
        self.deliverableTitleLabel.text = self.weekDeliverable.deliverable.title
        switch self.weekDeliverable.deliverable.typeTitle {
            
        case CommonConstants.VIDEO_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.VIDEO_MEDIA_TYPE
            break
            
        case CommonConstants.REPORT_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.blue
            self.deliverableMediaTypeLabel.text = CommonConstants.REPORT_MEDIA_TYPE
            break
            
        case CommonConstants.VISUAL_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.primaryColor
            self.deliverableMediaTypeLabel.text = CommonConstants.VISUAL_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.PLAN_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.PLAN_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.MAP_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.MAP_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.PROJECT_REPORT_SHOW_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.red
            self.deliverableMediaTypeLabel.text = CommonConstants.PROJECT_REPORT_SHOW_MEDIA_TYPE
            break
            
        case CommonConstants.MEETING_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.yellow
            self.deliverableMediaTypeLabel.text = CommonConstants.MEETING_MEDIA_TYPE
            break
            
        case CommonConstants.AGREEMENT_MEDIA_TYPE:
            self.mediaTypeImageView.backgroundColor = UIColor.AppColors.green
            self.deliverableMediaTypeLabel.text = CommonConstants.AGREEMENT_MEDIA_TYPE
            break
            
        default:
            break
        }
        
        if self.weekDeliverable.deliverable.collective {
            belongsToImageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
            belongsToImageView.tintColor = UIColor.AppColors.gray
            belongsToLabel.text = "team".localized()
        } else {
            belongsToImageView.image = UIImage(named: "team")?.withRenderingMode(.alwaysTemplate)
            belongsToImageView.tintColor = UIColor.AppColors.gray
            belongsToLabel.text = "personal".localized()
        }
        
        commentsNumberLabel.text = "\(weekDeliverable.feedbackCounter!)"
        var topNumber = ""
        
        if weekDeliverable.grades > 0 {
            topNumber = "\(weekDeliverable.grades!)"
        } else {
           topNumber = "-"
        }
        
        let bottomNumber = weekDeliverable.deliverable.grades!
        deliverableGradeLabel.text = "\(topNumber) / \(bottomNumber)"
    }

}
