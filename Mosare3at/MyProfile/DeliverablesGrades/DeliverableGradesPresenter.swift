//
//  DeliverableGradesPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/18/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public protocol DeliverableGradesView: class {
    func operationFailed(message: String)
    func getProjectsSuccess(projects: [Project])
    func getTeamIdSuccess(teamId: String, teamMemberId: String)
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable])
}

public class DeliverableGradesPresenter {
    fileprivate weak var deliverableGradesView : DeliverableGradesView?
    fileprivate let deliverableGradesRepository : DeliverableGradesRepository
    
    init(repository: DeliverableGradesRepository) {
        self.deliverableGradesRepository = repository
        self.deliverableGradesRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : DeliverableGradesView) {
        deliverableGradesView = view
    }
}

extension DeliverableGradesPresenter {
    
    public func getProjects(programId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.deliverableGradesRepository.getProjects(programId: programId)
        } else {
            self.deliverableGradesView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getWeekDeliverables(parameters: [String:String], token: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.deliverableGradesRepository.getWeekDeliverables(token: token, parameters: parameters, page: nil)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getTeamId(projectId: String, userId: Int, token: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.deliverableGradesRepository.getTeamId(projectId: projectId, userId: userId, token: token)
        } else {
            self.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension DeliverableGradesPresenter: DeliverableGradesPresenterDelegate {
    public func getTeamIdSuccess(teamId: String, teamMemberId: String) {
        self.deliverableGradesView?.getTeamIdSuccess(teamId: teamId, teamMemberId: teamMemberId)
    }
    
    public func getProjectsSuccess(projects: [Project]) {
        self.deliverableGradesView?.getProjectsSuccess(projects: projects)
    }
    
    public func operationFailed(message: String) {
        self.deliverableGradesView?.operationFailed(message: message)
    }
    
    public func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable]) {
        self.deliverableGradesView?.getWeekDeliverableSuccess(weekDeliverables: weekDeliverables)
    }
}
