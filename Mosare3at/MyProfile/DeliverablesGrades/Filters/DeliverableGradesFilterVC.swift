//
//  DeliverableGradesFilterVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/18/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import DropDown

public protocol DeliverableGradesFilterDelegate: class {
    func applyFilters(selectedProjectIndex: Int, selectedWeekIndex: Int, selectedBelongsToIndex: Int)
    func updateTopTitle(title: String)
}


class DeliverableGradesFilterVC: BaseVC {

    var delegate: DeliverableGradesFilterDelegate!
    
    var projects: [Project]!
    var selectedProjectIndex: Int!
    
    var weeks: [Week]!
    var selectedWeekIndex: Int!
    
    var belongsTo: [String] = ["teamAndPersonal".localized(), "team".localized(), "personal".localized()]
    var selectedBelongsToIndex: Int!
    
    var layout: DeliverableGradesFilterLayout!
    
    public static func buildVC() -> DeliverableGradesFilterVC {
        let vc = DeliverableGradesFilterVC()
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = DeliverableGradesFilterLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
       weeks = projects.get(at: selectedProjectIndex)?.weeks
        
        layout.populateData(projectText: projects.get(at: selectedProjectIndex)!.title, weekText: projects.get(at: selectedProjectIndex)!.weeks.get(at: selectedWeekIndex)!.title, belongsToText: belongsTo.get(at: selectedBelongsToIndex)!)
    }

}

extension DeliverableGradesFilterVC: DeliverableGradesFilterLayoutDelegate {
    func applyFilters() {
        self.dismiss(animated: true) {
            self.delegate.applyFilters(selectedProjectIndex: self.selectedProjectIndex, selectedWeekIndex: self.selectedWeekIndex, selectedBelongsToIndex: self.selectedBelongsToIndex)
        }
    }
    
    func retry() {
        
    }
    
    func cancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func setupProjectsDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.projectTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.projectTextField.bounds.height)
        
        var dataSource = [String]()
        for project in projects {
            dataSource.append(project.title)
        }
        
        dropDown.dataSource = dataSource
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedProjectIndex = index
            self.layout.projectTextField.text = self.projects.get(at: self.selectedProjectIndex)?.title
            
            self.weeks = self.projects.get(at: self.selectedProjectIndex)?.weeks
            
            let startDate = self.weeks.get(at: 0)!.startDate
            let endDate = self.weeks.get(at: 0)!.endDate
            
            self.populateTitle(startDate: startDate!, endDate: endDate!)
        }
        dropDown.show()
    }
    
    func setupWeeksDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.weeksTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.weeksTextField.bounds.height)
        
        var dataSource = [String]()
        for week in weeks {
            dataSource.append(week.title)
        }
        
        dropDown.dataSource = dataSource
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedWeekIndex = index
            self.layout.weeksTextField.text = self.weeks.get(at: self.selectedWeekIndex)?.title
            
            let startDate = self.weeks.get(at: self.selectedWeekIndex)!.startDate
            let endDate = self.weeks.get(at: self.selectedWeekIndex)!.endDate
            
            self.populateTitle(startDate: startDate!, endDate: endDate!)
            
            
        }
        dropDown.show()
    }
    
    func setupBelongsToDropDown() {
        let dropDown = DropDown()
        dropDown.anchorView = self.layout.belongsToTextField
        dropDown.bottomOffset = CGPoint(x: 0, y: self.layout.belongsToTextField.bounds.height)
        dropDown.dataSource = belongsTo
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectedBelongsToIndex = index
            self.layout.belongsToTextField.text = self.belongsTo.get(at: self.selectedBelongsToIndex)!
        }
        dropDown.show()
    }
    
    func populateTitle(startDate: String, endDate: String) {
        let finalStartDate = startDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (startDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 1)
        
        let finalEndDate = endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 1)
        
        let month = endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count) - 2)
        
        let monthString = UiHelpers.getMonthName(monthNumber: Int(month!)!)
        
        self.delegate.updateTopTitle(title: "\("week".localized()) \(UiHelpers.getIndexName(index: self.weeks.get(at: 0)!.weight)) - \(finalStartDate!) - \(finalEndDate!) \(monthString)")
    }
}
