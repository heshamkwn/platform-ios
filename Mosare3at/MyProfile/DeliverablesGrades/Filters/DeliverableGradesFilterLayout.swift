//
//  DeliverableGradesFilterLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/18/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import DropDown
import UIKit
import Material

public protocol DeliverableGradesFilterLayoutDelegate: BaseLayoutDelegate {
    func applyFilters()
    func cancel()
    func setupProjectsDropDown()
    func setupWeeksDropDown()
    func setupBelongsToDropDown()
}

public class DeliverableGradesFilterLayout: BaseLayout {
    var deliverableGradesFilterLayoutDelegate: DeliverableGradesFilterLayoutDelegate!
    
    init(superview: UIView, delegate: DeliverableGradesFilterLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.deliverableGradesFilterLayoutDelegate = delegate
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "filter".localized()
        label.font = AppFont.font(type: .Bold, size: 24)
        return label
    }()
    
    lazy var projectLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.text = "project".localized()
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var projectTextField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "")
        field.returnKeyType = UIReturnKeyType.next
//        field.text = "allProjects".localized()
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.textColor = UIColor.AppColors.gray
        field.isEnabled = false
        field.textAlignment = .center
        return field
    }()
    
    lazy var projectView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.addTapGesture(action: { (recognizer) in
            self.deliverableGradesFilterLayoutDelegate.setupProjectsDropDown()
        })
        return view
    }()
    
    lazy var projectArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "down_arrow")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var weeksLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.text = "time".localized()
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var weeksTextField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "")
        field.returnKeyType = UIReturnKeyType.next
//        field.text = "allProjects".localized()
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.textColor = UIColor.AppColors.gray
        field.isEnabled = false
        field.textAlignment = .center
        return field
    }()
    
    lazy var weekView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.addTapGesture(action: { (recognizer) in
            self.deliverableGradesFilterLayoutDelegate.setupWeeksDropDown()
        })
        return view
    }()
    
    lazy var weekArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "down_arrow")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var belongsToLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.black
        label.text = "belongsTo".localized()
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var belongsToTextField: ErrorTextField = {
        let field = UiHelpers.textField(placeholder: "")
        field.returnKeyType = UIReturnKeyType.next
        //        field.text = "allProjects".localized()
        field.autocorrectionType = .no
        field.autocapitalizationType = .none
        field.textColor = UIColor.AppColors.gray
        field.isEnabled = false
        field.textAlignment = .center
        return field
    }()
    
    lazy var belongsToView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        view.addTapGesture(action: { (recognizer) in
            self.deliverableGradesFilterLayoutDelegate.setupBelongsToDropDown()
        })
        return view
    }()
    
    lazy var belongsToArrowImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "down_arrow")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    lazy var applyLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "apply".localized()
        label.addTapGesture(action: { (_) in
            self.deliverableGradesFilterLayoutDelegate.applyFilters()
        })
        label.font = AppFont.font(type: .Bold, size: 14)
        return label
    }()
    
    lazy var cancelLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.textAlignment = .center
        label.text = "cancel".localized()
        label.font = AppFont.font(type: .Bold, size: 14)
        label.addTapGesture(action: { (_) in
            self.deliverableGradesFilterLayoutDelegate.cancel()
        })
        return label
    }()
    
    lazy var verticalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()
    
    lazy var horizontalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()
    
    public func setupViews() {
        let views = [titleLabel, projectView, projectLabel, projectTextField, projectArrowImageView, weekView, weeksLabel, weeksTextField, weekArrowImageView, belongsToView, belongsToLabel, belongsToTextField, belongsToArrowImageView, applyLabel, cancelLabel, verticalView, horizontalView]
        
        self.superview.addSubviews(views)
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
            maker.centerX.equalTo(superview)
        }
        
        projectLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(titleLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        projectArrowImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(projectLabel).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 0.5))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        projectTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(projectLabel)
            maker.leading.equalTo(projectLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(projectArrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        projectView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(projectTextField)
        }
        
        weeksLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(projectLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 15))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        weekArrowImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(weeksLabel).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 0.5))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        weeksTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(weeksLabel)
            maker.leading.equalTo(weeksLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(weekArrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        weekView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(weeksTextField)
        }
        
        belongsToLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(weeksLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 18))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        belongsToArrowImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.top.equalTo(belongsToLabel).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 0.5))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        belongsToTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(belongsToLabel)
            maker.leading.equalTo(belongsToLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.trailing.equalTo(belongsToArrowImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        belongsToView.snp.makeConstraints { (maker) in
            maker.edges.equalTo(belongsToTextField)
        }
        
        horizontalView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.height.equalTo(2)
            maker.top.equalTo(belongsToLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        applyLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.top.equalTo(horizontalView.snp.bottom)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49))
        }
        
        verticalView.snp.makeConstraints { (maker) in
            maker.top.equalTo(horizontalView.snp.bottom)
            maker.leading.equalTo(applyLabel.snp.trailing)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.width.equalTo(2)
        }
        
        cancelLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(verticalView.snp.trailing)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 7))
            maker.top.equalTo(horizontalView.snp.bottom)
            maker.trailing.equalTo(superview)
        }
    }
    
    public func populateData(projectText: String, weekText: String, belongsToText: String) {
        projectTextField.text = projectText
        weeksTextField.text = weekText
        belongsToTextField.text = belongsToText
    }
}
