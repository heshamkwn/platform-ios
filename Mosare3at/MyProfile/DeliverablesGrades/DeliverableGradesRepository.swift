//
//  DeliverableGradesRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/18/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol DeliverableGradesPresenterDelegate: class {
    func operationFailed(message: String)
    func getProjectsSuccess(projects: [Project])
    func getTeamIdSuccess(teamId: String, teamMemberId: String)
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable])
}

public class DeliverableGradesRepository {
    var delegate: DeliverableGradesPresenterDelegate!
    
    var weekDeliverables = [WeekDeliverable]()
    
    public func setDelegate(delegate: DeliverableGradesPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getProjects(programId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        //"Content-Type" : "application/json",
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "programs/\(programId)/projects")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    let jsonArray = json["hydra:member"] as? [Dictionary<String,AnyObject>]
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        var projects = [Project]()
                        for dic in jsonArray! {
                            let project = Project.getInstance(dictionary: dic)
                            projects.append(project)
                        }
                        self.delegate.getProjectsSuccess(projects: projects)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
 
    func getWeekDeliverables(token: String, parameters: [String : String], page: String?) {
        let headers = ["X-AUTH-TOKEN" : token]
        var myParams = parameters
        if let _ = page {
            myParams["page"] = page
        } else {
            self.weekDeliverables.removeAll()
        }
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_deliverables")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON{
            (response) in
            
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let weekDeliverableResponse = WeekDeliverableResponse.getInstance(dictionary: json)
                        for weekDeliverable in weekDeliverableResponse.hydraMember {
                            self.weekDeliverables.append(weekDeliverable)
                        }
                        let hydraView = json["hydra:view"] as? Dictionary<String,String>
                        if let next = hydraView!["hydra:next"] as? String {
                            let page = next.components(separatedBy: "=")[next.components(separatedBy: "=").count - 1]
                            self.getWeekDeliverables(token: token, parameters: myParams, page: page)
                        } else {
                            self.delegate.getWeekDeliverableSuccess(weekDeliverables: self.weekDeliverables)
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    func getTeamId(projectId: String, userId: Int, token: String) {
        
        let headers = ["X-AUTH-TOKEN" : token]
        
        let parameters = ["project" : projectId, "member":"/users/\(userId)"]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "team_members")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON {
            (response) in
            UiHelpers.hideLoader()
            
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        
                        let members = json["hydra:member"] as! [Dictionary<String,Any>]
                        
                        for dic in members {
                            let memberDic = dic["member"] as! Dictionary<String,Any>
                            
                            if let memberId = memberDic["@id"], (memberId as! String) == "/users/\(userId)" {
                                self.delegate.getTeamIdSuccess(teamId: dic["team"] as! String, teamMemberId: dic["@id"] as! String)
                            }
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
