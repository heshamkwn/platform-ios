//
//  DeliverablesGradesVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/12/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import EzPopup
import SwiftyUserDefaults

class DeliverablesGradesVC: BaseVC {

    var weekDeliverables :[WeekDeliverable]!
    
    var projects = [Project]()
    
    var currentProject: Project!
    var currentWeek: Week!
    
    var layout: DeliverablesGradesLayout!
    
    var presenter: DeliverableGradesPresenter!
    
    var teamId: Int = 0
    var teamMemberId: Int = 0
    
    var selectedProjectIndex: Int = 0
    var selectedWeekIndex: Int = 0
    var selectedBelongsToIndex: Int = 0
    
    public class func buildVC() -> DeliverablesGradesVC {
        return DeliverablesGradesVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = DeliverablesGradesLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        layout.deliverablesGradesTableView.dataSource = self
        layout.deliverablesGradesTableView.delegate = self
        
        currentProject = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).project!
        currentWeek = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).week!
        
        presenter = Injector.provideDeliverableGradesPresenter()
        presenter.setView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if selectedWeekIndex == 0 && selectedProjectIndex == 0 && selectedBelongsToIndex == 0 {
            let program = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program!
            
            let programId = Int(program.requestId.components(separatedBy: "/")[program.requestId.components(separatedBy: "/").count - 1])!
            self.projects.removeAll()
            presenter.getProjects(programId: programId)
        } else {
            let selectedProject = self.projects.get(at: selectedProjectIndex)!
            let selectedWeek = selectedProject.weeks.get(at: selectedWeekIndex)!
            self.weekDeliverables.removeAll()
            getDataAccordingToChoices(selectedProject: selectedProject, selectedWeek: selectedWeek)
        }
        
    }
    
    func populateTitleData(selectedWeek: Week) {
        let startDate = selectedWeek.startDate
        let endDate = selectedWeek.endDate
        
        let finalStartDate = startDate?.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (startDate?.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count)! - 1)
        
        let finalEndDate = endDate?.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate?.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count)! - 1)
        
        let month = endDate?.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").get(at: (endDate?.components(separatedBy: "T").get(at: 0)!.components(separatedBy: "-").count)! - 2)
        
        let monthString = UiHelpers.getMonthName(monthNumber: Int(month!)!)
        
        self.layout.populateData(dateString: "\("week".localized()) \(UiHelpers.getIndexName(index: selectedWeek.weight)) - \(finalStartDate!) - \(finalEndDate!) \(monthString)")
    }
    
    func getDataAccordingToChoices(selectedProject: Project, selectedWeek: Week) {
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        if selectedProject.requestId == currentProject.requestId {
            teamId = Int(Defaults[.teamId]!.components(separatedBy: "/").get(at: Defaults[.teamId]!.components(separatedBy: "/").count - 1)!)!
            teamMemberId = Int(Defaults[.teamMemberId]!.components(separatedBy: "/").get(at: Defaults[.teamMemberId]!.components(separatedBy: "/").count - 1)!)!
            
            switch selectedBelongsToIndex {
            case 0:
                presenter.getWeekDeliverables(parameters: ["teamMember":"\(teamMemberId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
                
                presenter.getWeekDeliverables(parameters: ["team":"\(teamId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
                break
                
            case 1:
                presenter.getWeekDeliverables(parameters: ["team":"\(teamId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
                break
                
            case 2:
                presenter.getWeekDeliverables(parameters: ["teamMember":"\(teamMemberId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
                break
            default:
                break
            }
        } else {
            presenter.getTeamId(projectId: String(selectedProject.id!), userId: user.id, token: user.token)
        }
    }
}

extension DeliverablesGradesVC: DeliverablesGradesLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showFilters() {
        if  self.projects.count > 0 {
            let vc = DeliverableGradesFilterVC.buildVC()
            vc.projects = projects
            vc.weeks = projects.get(at: 0)!.weeks
            vc.delegate = self
            
//            if self.selectedProjectIndex != 0 {
                vc.selectedProjectIndex = self.selectedProjectIndex
//            }
            
//            if self.selectedWeekIndex != 0 {
                vc.selectedWeekIndex = self.selectedWeekIndex
//            }
            
//            if self.selectedBelongsToIndex != 0 {
                vc.selectedBelongsToIndex = self.selectedBelongsToIndex
//            }
            
            let popupVC = PopupViewController(contentController: vc, popupWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 90), popupHeight: UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 36))
            present(popupVC, animated: true)
        }
    }
    
    func retry() {
        
    }
}

extension DeliverablesGradesVC: DeliverableGradesView {
    func getTeamIdSuccess(teamId: String, teamMemberId: String) {
        self.teamId = Int(teamId.components(separatedBy: "/").get(at: teamId.components(separatedBy: "/").count - 1)!)!
        self.teamMemberId = Int(teamMemberId.components(separatedBy: "/").get(at: teamMemberId.components(separatedBy: "/").count - 1)!)!
        
        let selectedProject = self.projects.get(at: selectedProjectIndex)!
        let selectedWeek = selectedProject.weeks.get(at: selectedWeekIndex)!
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        
        switch selectedBelongsToIndex {
        case 0:
            presenter.getWeekDeliverables(parameters: ["teamMember":"\(self.teamMemberId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            
            presenter.getWeekDeliverables(parameters: ["team":"\(self.teamId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            
            break
            
        case 1:
            presenter.getWeekDeliverables(parameters: ["team":"\(self.teamId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            break
            
        case 2:
           presenter.getWeekDeliverables(parameters: ["teamMember":"\(self.teamMemberId)", "week":"\(selectedWeek.requestId.components(separatedBy: "/").get(at: selectedWeek.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            break
        default:
            break
        }
        
        populateTitleData(selectedWeek: selectedWeek)
    }
    
    func getProjectsSuccess(projects: [Project]) {
        
        for project in projects {
            if project.weight <= currentProject.weight {
                self.projects.append(project)
            }
        }
        
        for count in 0...self.projects.count {
            if projects.get(at: count)?.requestId == currentProject.requestId {
                self.selectedProjectIndex = count
                break
            }
        }
        
        for count in 0...(self.projects.get(at: selectedProjectIndex)?.weeks.count)! {
            if self.projects.get(at: selectedProjectIndex)?.weeks.get(at: count)?.requestId == currentWeek.requestId {
                self.selectedWeekIndex = count
                break
            }
        }
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        if self.projects.get(at: selectedProjectIndex)!.requestId == currentProject.requestId {
            teamMemberId = Int(Defaults[.teamMemberId]!.components(separatedBy: "/")[Defaults[.teamMemberId]!.components(separatedBy: "/").count - 1])!
            teamId = Int(Defaults[.teamId]!.components(separatedBy: "/")[Defaults[.teamId]!.components(separatedBy: "/").count - 1])!
            
            presenter.getWeekDeliverables(parameters: ["teamMember":"\(teamMemberId)", "week":"\(projects.get(at: selectedProjectIndex)!.weeks.get(at: selectedWeekIndex)!.requestId.components(separatedBy: "/").get(at: projects.get(at: selectedProjectIndex)!.weeks.get(at: selectedWeekIndex)!.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            
            presenter.getWeekDeliverables(parameters: ["team":"\(teamId)", "week":"\(projects.get(at: selectedProjectIndex)!.weeks.get(at: selectedWeekIndex)!.requestId.components(separatedBy: "/").get(at: projects.get(at: selectedProjectIndex)!.weeks.get(at: selectedWeekIndex)!.requestId.components(separatedBy: "/").count - 1)!)"], token: user.token)
            
            populateTitleData(selectedWeek: projects.get(at: selectedProjectIndex)!.weeks.get(at: selectedWeekIndex)!)
        } else {
            presenter.getTeamId(projectId: String(projects.get(at: selectedProjectIndex)!.id!), userId: user.id, token: user.token)
        }
    }
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getWeekDeliverableSuccess(weekDeliverables: [WeekDeliverable]) {
        if let _ = self.weekDeliverables {
            self.weekDeliverables.append(contentsOf: weekDeliverables)
        } else {
             self.weekDeliverables = weekDeliverables
        }
        
       self.weekDeliverables = self.weekDeliverables.sorted(by: { $0.feedbackCounter > $1.feedbackCounter })

       
        if self.weekDeliverables.count > 0 {
            self.layout.deliverablesGradesTableView.isHidden = false
            self.layout.noDataLabel.isHidden = true
            self.layout.deliverablesGradesTableView.reloadData()
        } else {
            self.layout.deliverablesGradesTableView.isHidden = true
            self.layout.noDataLabel.isHidden = false
        }
    }
}

extension DeliverablesGradesVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = weekDeliverables {
            return weekDeliverables.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:DeliverablesGradesCell = layout.deliverablesGradesTableView.dequeueReusableCell(withIdentifier: DeliverablesGradesCell.identifier, for: indexPath) as! DeliverablesGradesCell
        cell.selectionStyle = .none
        cell.weekDeliverable = self.weekDeliverables.get(at: indexPath.row)!
        cell.index = indexPath.row
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let weekDeliverable = self.weekDeliverables.get(at: indexPath.row)!
        return weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: AppFont.font(type: .Bold, size: 16)) + UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 10)
    }
}

extension DeliverablesGradesVC: DeliverablesGradesCellDelegate {
    func openFeedback(index: Int) {
        print("open comments number \(index)")
        self.navigator.navigateToFeedbacks(weekDeliverable: weekDeliverables.get(at: index)!)
    }
    
    func goToDeliverableDetails(index: Int) {
        self.navigator.navigateToDeliverableDetailsScreen(deliverableId: Int((weekDeliverables.get(at: index)?.deliverable.requestId.components(separatedBy: "/")[(weekDeliverables.get(at: index)?.deliverable.requestId.components(separatedBy: "/").count)! - 1])!)!)
    }
}

extension DeliverablesGradesVC: DeliverableGradesFilterDelegate {
    func applyFilters(selectedProjectIndex: Int, selectedWeekIndex: Int, selectedBelongsToIndex: Int) {
        
        self.weekDeliverables.removeAll()
        self.selectedProjectIndex = selectedProjectIndex
        self.selectedWeekIndex = selectedWeekIndex
        self.selectedBelongsToIndex = selectedBelongsToIndex
        
        let selectedProject = self.projects.get(at: selectedProjectIndex)!
        let selectedWeek = selectedProject.weeks.get(at: selectedWeekIndex)!
        
        getDataAccordingToChoices(selectedProject: selectedProject, selectedWeek: selectedWeek)
    }
    
    func updateTopTitle(title: String) {
        self.layout.populateData(dateString: title)
    }
}
