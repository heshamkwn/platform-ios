//
//  DeliverablesGradesLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/12/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Material

public protocol DeliverablesGradesLayoutDelegate: BaseLayoutDelegate {
    func goBack()
    func showFilters()
}

public class DeliverablesGradesLayout: BaseLayout {
    
    public var deliverablesGradesLayoutDelegate: DeliverablesGradesLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: DeliverablesGradesLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.deliverablesGradesLayoutDelegate = delegate
    }
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.AppColors.primaryColor
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = AppFont.font(type: .Bold, size: 20)
        return label
    }()
    
    lazy var deliverablesGradesTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(DeliverablesGradesCell.self, forCellReuseIdentifier: DeliverablesGradesCell.identifier)
        return tableView
    }()
    
    lazy var noDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "noData".localized()
        label.backgroundColor = .white
        label.textAlignment = .center
        label.isHidden = true
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    public func setupViews() {
        let views = [topView, titleLabel, deliverablesGradesTableView, noDataLabel]
        
        superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.titleLabel.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(topView.snp.bottom)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        self.deliverablesGradesTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.leading.trailing.bottom.equalTo(superview)
        }
        
        self.noDataLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.center.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "deliverablesGrades".localized())
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.leftImageView.isHidden = false
        self.topView.videosFilterDelegate = self
        self.topView.delegate = self
    }
    
    public func populateData(dateString: String) {
        self.titleLabel.text = dateString
    }
}

extension DeliverablesGradesLayout: TopViewDelegate {
    public func goBack() {
        self.deliverablesGradesLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}

extension DeliverablesGradesLayout: VideosFiltersDelegate {
    public func showFilters() {
        self.deliverablesGradesLayoutDelegate.showFilters()
    }
}
