//
//  V1.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/12/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class V1 {
    var img : Img!
    var pdf : Img!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["img"] = img.convertToDictionary()
        dictionary["pdf"] = pdf.convertToDictionary()
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> V1 {
        let v1 = V1()
        v1.img =  Img.getInstance(dictionary: dictionary["img"] as! Dictionary<String, Any>)
        v1.pdf =  Img.getInstance(dictionary: dictionary["pdf"] as! Dictionary<String, Any>)
        return v1
    }
}
