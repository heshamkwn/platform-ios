//
//  Img.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/12/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class Img {
    var key : String!
    var status : String!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["key"] = key
        dictionary["status"] = status
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Img {
        let img = Img()
        img.key =  dictionary["key"] as? String
        img.status =  dictionary["status"] as? String
        return img
    }
}
