//
//  Achievements.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/19/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class Achievement {
    
    var projectID: Int!
    var projectTitle: String!
    var projectImage: String!
    var points: Int!
    var gpa: String!
    var grades: Double!
    var certificate: Certificate!
    
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["projectID"] = projectID
        dictionary["projectTitle"] = projectTitle
        dictionary["projectImage"] = projectImage
        dictionary["points"] = points
        dictionary["gpa"] = gpa
        dictionary["grades"] = grades
        dictionary["certificate"] = certificate.convertToDictionary()
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Achievement {
        let achievement = Achievement()
        achievement.projectID =  dictionary["projectID"] as? Int
        achievement.projectTitle =  dictionary["projectTitle"] as? String
        achievement.projectImage =  dictionary["projectImage"] as? String
        achievement.points =  dictionary["points"] as? Int
        achievement.gpa =  dictionary["gpa"] as? String
        achievement.grades =  dictionary["grades"] as? Double
        if let certificateDictionary = dictionary["certificate"] as? Dictionary<String, Any> {
            achievement.certificate =  Certificate.getInstance(dictionary: certificateDictionary)
        }
        return achievement
    }
}


