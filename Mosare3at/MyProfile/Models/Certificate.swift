//
//  Certificate.swift
//  Mosare3at
//
//  Created by Hesham Donia on 3/12/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class Certificate {

    var uuid : String!
    var v1 : V1!
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["uuid"] = uuid
        dictionary["v1"] = v1.convertToDictionary()
        
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Certificate {
        let certificate = Certificate()
        certificate.uuid = dictionary["uuid"] as? String
        certificate.v1 =  V1.getInstance(dictionary: dictionary["v1"] as! Dictionary<String, Any>)
        return certificate
    }
    
}
