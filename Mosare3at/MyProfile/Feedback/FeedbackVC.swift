//
//  FeedbackVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/20/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

class FeedbackVC: BaseVC {

    var weekDeliverable: WeekDeliverable!
    var weekDeliverableId: Int!
    
    var feedbacks: [Feedback]!
    
    var layout: FeedbackLayout!
    var presenter: FeedbackPresenter!
    
    public class func buildVC(weekDeliverable: WeekDeliverable) -> FeedbackVC {
        let vc = FeedbackVC()
        vc.weekDeliverable = weekDeliverable
        return vc
    }
    
    public class func buildVC(weekDeliverableId: Int) -> FeedbackVC {
        let vc = FeedbackVC()
        vc.weekDeliverableId = weekDeliverableId
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = FeedbackLayout(superview: self.view, delegate: self)
        presenter = Injector.provideFeedbackPresenter()
        presenter.setView(view: self)
        
        if weekDeliverable != nil {
            layout.weekDeliverable = weekDeliverable
            layout.setupViews()
            layout.populateData()
            
            layout.commentField.delegate = self
            presenter.getFeedbacks(weekDeliverableId: weekDeliverable.id)
        } else {
            presenter.getWeekDeliverable(weekDeliverableId: weekDeliverableId)
        }
    }
}

extension FeedbackVC: FeedbackLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func publishFeedback() {
        if let comment = layout.commentField.text, !comment.isEmpty {
            let feedback = Feedback()
            feedback.content = comment
            feedback.user = User.getInstance(dictionary: Defaults[.user]!)
            feedback.weekDeliverableRequestId = weekDeliverable.requestId
            
            presenter.publishFeedback(feedback: feedback)
        } else {
            self.view.makeToast("commentEmptyError".localized())
        }
    }
    
    func retry() {
        
    }
}

extension FeedbackVC: FeedbackView {
    func getWeekDeliverableSuccess(weekDeliverable: WeekDeliverable) {
        self.weekDeliverable = weekDeliverable
        
        layout.weekDeliverable = weekDeliverable
        layout.setupViews()
        layout.populateData()
        
        layout.commentField.delegate = self
        
        presenter = Injector.provideFeedbackPresenter()
        presenter.setView(view: self)
        
        presenter.getFeedbacks(weekDeliverableId: weekDeliverable.id)
    }
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getFeedbacksSuccess(feedbacks: [Feedback]) {
        
//        self.feedbacks = feedbacks.reversed()
        self.feedbacks = feedbacks
        if feedbacks.count > 0 {
            
            layout.feedbackTableView.isHidden = false
            layout.noDataLabel.isHidden = true
            
            layout.feedbackTableView.dataSource = self
            layout.feedbackTableView.delegate = self
            layout.feedbackTableView.reloadData()
        } else {
            layout.feedbackTableView.isHidden = true
            layout.noDataLabel.isHidden = false
        }
    }
    
    func publishFeedbackSuccess(feedback: Feedback) {
        self.feedbacks.insert(feedback, at: 0)
        layout.feedbackTableView.isHidden = false
        layout.noDataLabel.isHidden = true
        layout.numberOfCommentsLabel.text = "\(feedbacks.count) \("comment".localized())"
        layout.commentField.text = ""
        layout.feedbackTableView.reloadData()
    }
    
    func updateFeedbackSuccess(id: Int, newContent: String) {
        
        for counter in 0...feedbacks.count {
            if feedbacks.get(at: counter)!.id == id {
                feedbacks[counter].content = newContent
                layout.feedbackTableView.reloadData()
                break
            }
        }
    }
    
    func deleteFeedbackSuccess(feedback: Feedback) {
        for counter in 0...feedbacks.count {
            if feedbacks.get(at: counter)!.requestId == feedback.requestId {
                feedbacks.remove(at: counter)
                layout.feedbackTableView.reloadData()
                break
            }
        }
        
        if feedbacks.count == 0 {
            layout.feedbackTableView.isHidden = true
            layout.noDataLabel.isHidden = false
        }
        
        layout.numberOfCommentsLabel.text = "\(feedbacks.count) \("comment".localized())"
    }
}

extension FeedbackVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = feedbacks {
            return feedbacks.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:FeedbackCell = layout.feedbackTableView.dequeueReusableCell(withIdentifier: FeedbackCell.identifier, for: indexPath) as! FeedbackCell
        cell.selectionStyle = .none
        cell.feedback = feedbacks.get(at: indexPath.row)!
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let feedback = feedbacks.get(at: indexPath.row)!
        
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 14) + feedback.content.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: AppFont.font(type: .Regular, size: 16))
    }
}

extension FeedbackVC: FeedbackCellDelegate {
    func openEditOrDelete(feedback: Feedback) {
        var alertController: UIAlertController!
        
        alertController = UiHelpers.createAlertView(title: "openMenuTitle".localized(), message: "openMenuMessage".localized(), actions: [UIAlertAction(title: "editComment".localized(), style: UIAlertAction.Style.default, handler: { action in
                alertController.dismiss(animated: true, completion: nil)
                self.navigator.navigateToEditFeedback(feedback: feedback, delegate: self)
        }), UIAlertAction(title: "deleteComment".localized(), style: UIAlertAction.Style.destructive, handler: { action in
                alertController.dismiss(animated: true, completion: nil)
                self.showDeleteCommentAlert(feedback: feedback)
        })])
        
        self.presentVC(alertController)
    }
    
    func showDeleteCommentAlert(feedback: Feedback) {
        
        var alertController: UIAlertController!
        
        alertController = UiHelpers.createAlertView(title: "deleteCommentTitle".localized(), message: "deleteCommentMessage".localized()
            , actions:
            [UIAlertAction(title: "cancel".localized(), style: UIAlertAction.Style.cancel, handler: { action in
            alertController.dismiss(animated: true, completion: nil)
            }), UIAlertAction(title: "confirm".localized(), style: UIAlertAction.Style.destructive, handler: { action in
                alertController.dismiss(animated: true, completion: nil)
                self.presenter.deleteFeedback(feedback: feedback)
                
            })
            ])
        
        self.presentVC(alertController)
    }
}

extension FeedbackVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "addComment".localized()
            textView.textColor = UIColor.lightGray
        }
    }
}

extension FeedbackVC: EditFeedbackDelegate {
    func editDone(id: Int, newContent: String) {
        self.updateFeedbackSuccess(id: id, newContent: newContent)
    }
}
