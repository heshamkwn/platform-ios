//
//  FeedbackRepository.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/20/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyUserDefaults

public protocol FeedbackPresenterDelegate {
    func operationFailed(message: String)
    func getWeekDeliverableSuccess(weekDeliverable: WeekDeliverable)
    func getFeedbacksSuccess(feedbacks: [Feedback])
    func publishFeedbackSuccess(feedback: Feedback)
    func updateFeedbackSuccess(id: Int, newContent: String)
    func deleteFeedbackSuccess(feedback: Feedback)
}

public class FeedbackRepository {
    
    var delegate: FeedbackPresenterDelegate!
    
    public func setDelegate(delegate: FeedbackPresenterDelegate) {
        self.delegate = delegate
    }
    
    public func getWeekDeliverable(weekDeliverableId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "week_deliverables/\(weekDeliverableId)")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let weekDeliverable = WeekDeliverable.getInstance(dictionary: json)
                        self.delegate.getWeekDeliverableSuccess(weekDeliverable: weekDeliverable)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func getFeedbacks(weekDeliverableId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        let parameters = ["weekDeliverable" : "\(weekDeliverableId)"]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "feedback")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        
                        if let jsonArray = json["hydra:member"] as? [Dictionary<String,AnyObject>] {
                            var feedbacks = [Feedback]()
                            for dic in jsonArray {
                                let feedback = Feedback.getInstance(dictionary: dic)
                                feedbacks.append(feedback)
                            }
                            self.delegate.getFeedbacksSuccess(feedbacks: feedbacks)
                        } else {
                            self.delegate.operationFailed(message: "somethingWentWrong".localized())
                        }
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func publishFeedback(feedback: Feedback) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!, "Content-Type" : "application/json"]
        let parameters = ["content" : feedback.content, "weekDeliverable" : feedback.weekDeliverableRequestId, "user" : "/users/\(feedback.user.id!)"] as [String : Any]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "feedback")!, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let newFeedback = Feedback.getInstance(dictionary: json)
                        self.delegate.publishFeedbackSuccess(feedback: newFeedback)
                    } else {
                        self.delegate.operationFailed(message: "somethingWentWrong".localized())
                    }
                } else {
                     self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func updateFeedback(id: Int, newContent: String) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!, "Content-Type" : "application/json"]
        let parameters = ["content" : newContent] as [String : Any]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "feedback/\(id)")!, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.updateFeedbackSuccess(id: id, newContent: newContent)
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
    
    public func deleteFeedback(feedback: Feedback) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "feedback/\(feedback.id!)")!, method: .delete, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                    self.delegate.deleteFeedbackSuccess(feedback: feedback)
                } else {
                    self.delegate.operationFailed(message: "somethingWentWrong".localized())
                }
            } else {
                self.delegate.operationFailed(message: "somethingWentWrong".localized())
            }
        }
    }
}
