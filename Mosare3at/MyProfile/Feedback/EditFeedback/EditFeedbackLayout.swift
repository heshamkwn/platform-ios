//
//  EditFeedbackLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/22/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import Material

public protocol EditFeedbackLayoutDelegate: BaseLayoutDelegate {
    func updateFeedback(id: Int, newContent: String)
    func goBack()
}

public class EditFeedbackLayout: BaseLayout {
    var editFeedbackLayoutDelegate: EditFeedbackLayoutDelegate!
    
    var feedback: Feedback!
    
    init(superview: UIView, delegate: EditFeedbackLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.editFeedbackLayoutDelegate = delegate
    }
    
    var topView: TopView = TopView()
    
    lazy var horizontalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()
    
    lazy var userProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8)/2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var commentField: UITextView = {
        let field = UITextView()
        field.autocorrectionType = .no
        field.text = "addComment".localized()
        field.textColor = UIColor.black
        field.textAlignment = .right
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 14)
        return field
    }()
    
    lazy var updateButton: RaisedButton = {
        let button = RaisedButton(title: "edit".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.darkRed
        button.layer.cornerRadius = 8
        button.titleLabel?.font = AppFont.font(type: .Regular, size: 14)
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.editFeedbackLayoutDelegate.updateFeedback(id: self.feedback.id, newContent: self.commentField.text)
        }
        return button
    }()
    
    lazy var cancelButton: RaisedButton = {
        let button = RaisedButton(title: "cancel".localized(), titleColor: .black)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 8
        button.addBorder(width: 1, color: UIColor.lightGray)
        button.titleLabel?.font = AppFont.font(type: .Regular, size: 14)
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addTapGesture { recognizer in
            self.editFeedbackLayoutDelegate.goBack()
        }
        return button
    }()
    
    public func setupViews() {
        let views = [topView, horizontalView, userProfilePicImageView, commentField, updateButton, cancelButton]
        
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        horizontalView.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.height.equalTo(2)
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        userProfilePicImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(horizontalView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
        }
        
        commentField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(userProfilePicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(horizontalView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
        }
        
        updateButton.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(commentField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        cancelButton.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(updateButton.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(commentField.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "edit".localized())
        self.topView.backgroundColor = .white
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")?.withRenderingMode(.alwaysTemplate)
        self.topView.backImageView.tintColor = .black
        self.topView.screenTitleLabel.isHidden = false
        self.topView.screenTitleLabel.textColor = .black
        self.topView.leftImageView.isHidden = true
        self.topView.delegate = self
    }
    
    public func populateData() {
        userProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + feedback.user.profilePic)!)
        self.commentField.text = feedback.content
    }
}

extension EditFeedbackLayout: TopViewDelegate {
    public func goBack() {
        self.editFeedbackLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
