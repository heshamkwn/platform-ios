//
//  EditFeedbackVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/22/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit

public protocol EditFeedbackDelegate: class {
    func editDone(id: Int, newContent: String)
}

class EditFeedbackVC: BaseVC {

    var layout: EditFeedbackLayout!
    var presenter: FeedbackPresenter!
    var feedback: Feedback!
    var delegate: EditFeedbackDelegate!
    
    public class func buildVC(feedback: Feedback, delegate: EditFeedbackDelegate) -> EditFeedbackVC {
        let vc = EditFeedbackVC()
        vc.feedback = feedback
        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = EditFeedbackLayout(superview: self.view, delegate: self)
        layout.feedback = feedback
        layout.setupViews()
        layout.commentField.delegate = self
        layout.populateData()
        
        presenter = Injector.provideFeedbackPresenter()
        presenter.setView(view: self)
    }

}

extension EditFeedbackVC: EditFeedbackLayoutDelegate {
    func updateFeedback(id: Int, newContent: String) {
        self.presenter.updateFeedback(id: id, newContent: newContent)
    }
    
    func retry() {
        
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
}

extension EditFeedbackVC: FeedbackView {
    func getWeekDeliverableSuccess(weekDeliverable: WeekDeliverable) {
        
    }
    
    
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getFeedbacksSuccess(feedbacks: [Feedback]) {
        
    }
    
    func publishFeedbackSuccess(feedback: Feedback) {
        
    }
    
    func updateFeedbackSuccess(id: Int, newContent: String) {
        self.delegate.editDone(id: id, newContent: newContent)
        goBack()
    }
    
    func deleteFeedbackSuccess(feedback: Feedback) {
        
    }
}

extension EditFeedbackVC: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "addComment".localized()
            textView.textColor = UIColor.lightGray
        }
    }
}
