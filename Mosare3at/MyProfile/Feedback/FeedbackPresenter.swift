//
//  FeedbackPresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/20/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public protocol FeedbackView: class {
    func operationFailed(message: String)
    func getWeekDeliverableSuccess(weekDeliverable: WeekDeliverable)
    func getFeedbacksSuccess(feedbacks: [Feedback])
    func publishFeedbackSuccess(feedback: Feedback)
    func updateFeedbackSuccess(id: Int, newContent: String)
    func deleteFeedbackSuccess(feedback: Feedback)
}

public class FeedbackPresenter {
    fileprivate weak var feedbackView : FeedbackView?
    fileprivate let feedbackRepository : FeedbackRepository
    
    init(repository: FeedbackRepository) {
        self.feedbackRepository = repository
        self.feedbackRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : FeedbackView) {
        feedbackView = view
    }
}

extension FeedbackPresenter {
    public func getFeedbacks(weekDeliverableId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.feedbackRepository.getFeedbacks(weekDeliverableId: weekDeliverableId)
        } else {
            self.feedbackView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getWeekDeliverable(weekDeliverableId: Int) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.feedbackRepository.getWeekDeliverable(weekDeliverableId: weekDeliverableId)
        } else {
            self.feedbackView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func publishFeedback(feedback: Feedback) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.feedbackRepository.publishFeedback(feedback: feedback)
        } else {
            self.feedbackView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func updateFeedback(id: Int, newContent: String) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.feedbackRepository.updateFeedback(id: id, newContent: newContent)
        } else {
            self.feedbackView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func deleteFeedback(feedback: Feedback) {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.feedbackRepository.deleteFeedback(feedback: feedback)
        } else {
            self.feedbackView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension FeedbackPresenter: FeedbackPresenterDelegate {
    public func operationFailed(message: String) {
        self.feedbackView?.operationFailed(message: message)
    }
    
    public func getFeedbacksSuccess(feedbacks: [Feedback]) {
        self.feedbackView?.getFeedbacksSuccess(feedbacks: feedbacks)
    }
    
    public func publishFeedbackSuccess(feedback: Feedback) {
        self.feedbackView?.publishFeedbackSuccess(feedback: feedback)
    }
    
    public func updateFeedbackSuccess(id: Int, newContent: String) {
        self.feedbackView?.updateFeedbackSuccess(id: id, newContent: newContent)
    }
    
    public func deleteFeedbackSuccess(feedback: Feedback) {
        self.feedbackView?.deleteFeedbackSuccess(feedback: feedback)
    }
    
    public func getWeekDeliverableSuccess(weekDeliverable: WeekDeliverable) {
        self.feedbackView?.getWeekDeliverableSuccess(weekDeliverable: weekDeliverable)
    }
}
