//
//  Feedback.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/20/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public class Feedback {
    
    var id: Int!
    var requestId: String!
    var weekDeliverableRequestId: String!
    var content: String!
    var user: User!
    var createDate: String!
    var updateDate: String!
    var nickname: String!
    
    init() {
        
    }
    
    public func convertToDictionary() -> Dictionary<String, Any> {
        var dictionary = Dictionary<String, Any>()
        dictionary["id"] = id
        dictionary["@id"] = requestId
        dictionary["content"] = content
        dictionary["weekDeliverable"] = weekDeliverableRequestId
        if let _ = user {
            dictionary["user"] = user.convertToDictionary()
        }
        dictionary["createDate"] = createDate
        dictionary["updateDate"] = updateDate
        dictionary["nickname"] = nickname
        return dictionary
    }
    
    public static func getInstance(dictionary: Dictionary<String, Any>) -> Feedback {
        
        let feedback = Feedback()
        
        feedback.id =  dictionary["id"] as? Int
        feedback.requestId = dictionary["@id"] as? String
        feedback.content = dictionary["content"] as? String
        feedback.weekDeliverableRequestId = dictionary["weekDeliverable"] as? String
        feedback.user = User.getInstance(dictionary: dictionary["user"] as! Dictionary<String, Any>)
        feedback.createDate = dictionary["createDate"] as? String
        feedback.updateDate = dictionary["updateDate"] as? String
        feedback.nickname = dictionary["nickname"] as? String
        
        return feedback
    }
}
