//
//  FeedbackCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/20/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

public protocol FeedbackCellDelegate: class {
    func openEditOrDelete(feedback: Feedback)
}

class FeedbackCell: UITableViewCell {
    
    public static let identifier = "FeedbackCell"
    var superView: UIView!

    var feedback: Feedback!
    
    var delegate: FeedbackCellDelegate!
    
    lazy var userProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "no_badge")
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8)/2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var nicknameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var createDateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = AppFont.font(type: .Regular, size: 13)
        return label
    }()
    
    lazy var commentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var dotsMenuImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "dots")
        imageView.isHidden = true
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (_) in
            self.delegate.openEditOrDelete(feedback: self.feedback)
        })
        return imageView
    }()
    
    public func setupViews() {
        let views = [userProfilePicImageView, usernameLabel, nicknameLabel, createDateLabel, dotsMenuImageView, commentLabel]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        self.userProfilePicImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 8))
        }
        
        usernameLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(userProfilePicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        nicknameLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(userProfilePicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
             maker.top.equalTo(usernameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        dotsMenuImageView.snp.makeConstraints { (maker) in
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
        
        createDateLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
             maker.trailing.equalTo(dotsMenuImageView.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 2) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 20))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
        }
        
        commentLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(userProfilePicImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            
            maker.leading.equalTo(userProfilePicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.height.equalTo(feedback.content.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 95), font: commentLabel.font))
        }
    }
    
    public func populateData() {
        if let _ = feedback.user.profilePic {
            userProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + feedback.user.profilePic)!)
        }
        
        usernameLabel.text = feedback.user.firstName + " " + feedback.user.lastName
        
        commentLabel.text = feedback.content
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        if user.id == feedback.user.id {
            dotsMenuImageView.isHidden = false
            superView.backgroundColor = UIColor.AppColors.lightGray
        } else {
            dotsMenuImageView.isHidden = true
            superView.backgroundColor = UIColor.white
        }
        
        let createDate = feedback.createDate.components(separatedBy: "T").get(at: 0)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-mm-dd"//Thh:MM:ss"
        
        
        
        let startDate = dateFormatter.date(from: createDate)
        let endDate = Date()
        let components = Calendar.current.dateComponents([.day], from: startDate!, to: endDate)
        
        if components.day == 0 || components.day == nil {
            if components.hour == 0 || components.hour == nil {
                if components.minute == 0 || components.minute == nil { // الان
                    createDateLabel.text = "now".localized()
                } else { // منذ كذا دقيقه
                    if components.minute == 1 {
                        createDateLabel.text = "from".localized() + " \(components.minute!)" + "minute".localized()
                    } else if components.minute == 2 {
                        createDateLabel.text = "from".localized() + " \(components.minute!)" + "2minute".localized()
                    } else {
                        createDateLabel.text = "from".localized() + " \(components.minute!)" + "minutes".localized()
                    }
                }
            } else { // منذ كذا ساعه
                if components.hour == 1 {
                    createDateLabel.text = "from".localized() + " \(components.hour!)" + "hour".localized()
                } else if components.minute == 2 {
                    createDateLabel.text = "from".localized() + " \(components.hour!)" + "2hour".localized()
                } else {
                    createDateLabel.text = "from".localized() + " \(components.hour!)" + "hours".localized()
                }
            }
        } else if components.day == 1 { // أمس في س س : ث ث
            createDateLabel.text = "yesterday".localized() + " " + "in".localized() + " " + feedback.createDate.components(separatedBy: ":").get(at: 1)! + ":" + feedback.createDate.components(separatedBy: ":").get(at: 2)!
        } else { // نكتب التاريخ و الساعه
            let hms = feedback.createDate.components(separatedBy: "T").get(at: 1)!
            createDateLabel.text = feedback.createDate.components(separatedBy: "T").get(at: 0)! + " " + "in".localized() + " " + hms.components(separatedBy: ":").get(at: 0)! + ":" + hms.components(separatedBy: ":").get(at: 1)!
        }
        
        if let _ = feedback.nickname {
            nicknameLabel.text = feedback.nickname
        }
    }
}
