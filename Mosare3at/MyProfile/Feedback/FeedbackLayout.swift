//
//  FeedbackLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/20/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit
import Material
import SwiftyUserDefaults

public protocol FeedbackLayoutDelegate: BaseLayoutDelegate {
    func goBack()
    func publishFeedback()
}

public class FeedbackLayout: BaseLayout {
    var feedbackLayoutDelegate: FeedbackLayoutDelegate!
    
    var weekDeliverable: WeekDeliverable!
    
    init(superview: UIView, delegate: FeedbackLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.feedbackLayoutDelegate = delegate
    }
    
    var topView: TopView = TopView()
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var mediaTypeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var weekDeliverableTitleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = AppFont.font(type: .Regular, size: 16)
        return label
    }()
    
    lazy var visibilityImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "eye")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    lazy var visibilityLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.textAlignment = .right
        label.font = AppFont.font(type: .Regular, size: 12)
        return label
    }()
    
    lazy var numberOfCommentsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.backgroundColor = UIColor.AppColors.gray
        label.layer.cornerRadius = 6
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    lazy var feedbackTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(FeedbackCell.self, forCellReuseIdentifier: FeedbackCell.identifier)
        return tableView
    }()
    
    lazy var noDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "noCommentsAvailable".localized()
        label.backgroundColor = .white
        label.textAlignment = .center
        label.isHidden = true
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    lazy var horizontalView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AppColors.lightGray
        return view
    }()
    
    lazy var userProfilePicImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4)/2
        imageView.clipsToBounds = true
        return imageView
    }()
    
    lazy var commentField: UITextView = {
        let field = UITextView()
        field.autocorrectionType = .no
        field.text = "addComment".localized()
        field.textColor = UIColor.lightGray
        field.textAlignment = .right
        field.autocapitalizationType = .none
        field.font = AppFont.font(type: .Regular, size: 14)
        return field
    }()
    
    lazy var publishButton: RaisedButton = {
        let button = RaisedButton(title: "publish".localized(), titleColor: .white)
        button.backgroundColor = UIColor.AppColors.darkRed
        button.layer.cornerRadius = 8
        button.titleLabel?.font = AppFont.font(type: .Regular, size: 14)
        button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.center
        button.addShadow(offset: CGSize.zero, radius: 2.0, color: .black, opacity: 0.5)
        button.addTapGesture { recognizer in
            self.feedbackLayoutDelegate.publishFeedback()
        }
        return button
    }()
    
    public func setupViews() {
        let views = [topView, containerView, mediaTypeLabel, visibilityLabel, visibilityImageview, weekDeliverableTitleLabel, numberOfCommentsLabel, feedbackTableView, horizontalView, userProfilePicImageView, commentField, publishButton, noDataLabel]
        
        superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        containerView.addSubviews([mediaTypeLabel, visibilityLabel, visibilityImageview, weekDeliverableTitleLabel, numberOfCommentsLabel])
        
        containerView.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6) + weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 50), font: weekDeliverableTitleLabel.font))
        }
        
        mediaTypeLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 49))
        }
        
        visibilityImageview.snp.makeConstraints { (maker) in
            maker.top.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(mediaTypeLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
        }
        
        visibilityLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(visibilityImageview.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
        }
        
        weekDeliverableTitleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(mediaTypeLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 60))
            maker.height.equalTo(weekDeliverable.deliverable.title.height(constraintedWidth: UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 50), font: weekDeliverableTitleLabel.font))
        }
        
        numberOfCommentsLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(mediaTypeLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.leading.equalTo(weekDeliverableTitleLabel.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        userProfilePicImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.bottom.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) * -1)
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        noDataLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.center.equalTo(superview)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }

        publishButton.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.bottom.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) * -1)
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        commentField.snp.makeConstraints { (maker) in
            maker.leading.equalTo(userProfilePicImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(publishButton.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.bottom.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2) * -1)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        horizontalView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.bottom.equalTo(commentField.snp.top).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            maker.height.equalTo(2)
        }
        
        feedbackTableView.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superview)
            maker.top.equalTo(containerView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.bottom.equalTo(horizontalView.snp.top)
                //.offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "comments".localized())
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.leftImageView.isHidden = true
        self.topView.delegate = self
    }
    
    public func populateData() {
        mediaTypeLabel.text = weekDeliverable.deliverable.typeTitle
        if weekDeliverable.deliverable.collective {
            visibilityLabel.text = "teamvisibility".localized()
        } else {
            visibilityLabel.text = "personalVisibility".localized()
        }
        weekDeliverableTitleLabel.text = weekDeliverable.deliverable.title
        numberOfCommentsLabel.text = "\(weekDeliverable.feedbackCounter!) \("comment".localized())"
        
        let user = User.getInstance(dictionary: Defaults[.user]!)
        
        userProfilePicImageView.af_setImage(withURL: URL(string: CommonConstants.IMAGES_BASE_URL + user.profilePic)!)
    }
}

extension FeedbackLayout: TopViewDelegate {
    public func goBack() {
        self.feedbackLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
