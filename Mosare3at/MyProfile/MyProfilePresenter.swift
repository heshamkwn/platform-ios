//
//  MyProfilePresenter.swift
//  Mosare3at
//
//  Created by Hesham Donia on 11/18/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation

public protocol MyProfileView : class {
    func operationFailed(message: String)
    func getUserInfoSuccess(userInfo: UserInfo)
    func getProgramsProgressSuccess(programs: [RegisteredProgram])
}

public class MyProfilePresenter {
    fileprivate weak var myProfileView : MyProfileView?
    fileprivate let myProfileRepository : MyProfileRepository
    
    init(repository: MyProfileRepository) {
        self.myProfileRepository = repository
        self.myProfileRepository.setDelegate(delegate: self)
    }
    
    // this initialize the presenter view methods
    func setView(view : MyProfileView) {
        myProfileView = view
    }
}

extension MyProfilePresenter {
    public func getUserInfo() {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.myProfileRepository.getUserInfo()
        } else {
            self.myProfileView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
    
    public func getProgramsProgress() {
        if UiHelpers.isInternetAvailable() {
            UiHelpers.showLoader()
            self.myProfileRepository.getProgramsProgress()
        } else {
            self.myProfileView?.operationFailed(message: "noInternetConnection".localized())
        }
    }
}

extension MyProfilePresenter: MyProfilePresenterDelegate {
    public func getProgramsProgressSuccess(programs: [RegisteredProgram]) {
        UiHelpers.hideLoader()
        self.myProfileView?.getProgramsProgressSuccess(programs: programs)
    }
    
    public func operationFailed(message: String) {
        UiHelpers.hideLoader()
        self.myProfileView?.operationFailed(message: message)
    }
    
    public func getUserInfoSuccess(userInfo: UserInfo) {
        UiHelpers.hideLoader()
        self.myProfileView?.getUserInfoSuccess(userInfo: userInfo)
    }
    
    
}
