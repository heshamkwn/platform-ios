//
//  AchievementsVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/19/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Alamofire

class AchievementsVC: BaseVC {

    
    var achivements: [Achievement]!
    
    var layout: AchievementsLayout!
    
    var presenter: MyProfilePresenter!
    var isFromNotifications: Bool = false
    var userInfo: UserInfo!
    
    public class func buildVC(achivements: [Achievement]) -> AchievementsVC {
        let vc = AchievementsVC()
        vc.achivements = achivements
        return vc
    }
    
    public class func buildVC(isFromNotifications: Bool) -> AchievementsVC {
        let vc = AchievementsVC()
        vc.isFromNotifications = isFromNotifications
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        layout = AchievementsLayout(superview: self.view, delegate: self)
        layout.setupViews()

        if let _ = achivements, achivements.count > 0 {
            layout.achievementsTableView.isHidden = false
            layout.noDataLabel.isHidden = true
            
            layout.achievementsTableView.dataSource = self
            layout.achievementsTableView.delegate = self
            layout.achievementsTableView.reloadData()
        } else {
            if !isFromNotifications {
                layout.achievementsTableView.isHidden = false
                layout.noDataLabel.isHidden = true
            } else {
                presenter = Injector.provideMyProfilePresenter()
                presenter.setView(view: self)
                
                presenter.getUserInfo()
            }
        }
    }
}

extension AchievementsVC: MyProfileView {
    func operationFailed(message: String) {
        self.view.makeToast(message)
    }
    
    func getUserInfoSuccess(userInfo: UserInfo) {
        self.userInfo = userInfo
        self.achivements = self.userInfo.achievements
        
        if let _ = achivements, achivements.count > 0 {
            layout.achievementsTableView.isHidden = false
            layout.noDataLabel.isHidden = true
            
            layout.achievementsTableView.dataSource = self
            layout.achievementsTableView.delegate = self
            layout.achievementsTableView.reloadData()
        } else {
            layout.achievementsTableView.isHidden = false
            layout.noDataLabel.isHidden = true
        }
    }
    
    func getProgramsProgressSuccess(programs: [RegisteredProgram]) {
        
    }
}

extension AchievementsVC: AchievementsLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension AchievementsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return achivements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AchievementsCell = layout.achievementsTableView.dequeueReusableCell(withIdentifier: AchievementsCell.identifier, for: indexPath) as! AchievementsCell
        cell.selectionStyle = .none
        cell.achievement = self.achivements.get(at: indexPath.row)!
        cell.delegate = self
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 50)
    }
}

extension AchievementsVC: AchievementsCellDelegate {
    func share(image: UIImage) {
        UiHelpers.shareImage(sharableImage: image, sourceView: self.view, vc: self)
    }
}
