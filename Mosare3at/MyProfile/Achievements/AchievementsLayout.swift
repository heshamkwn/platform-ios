//
//  AchievementsLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/19/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol AchievementsLayoutDelegate: BaseLayoutDelegate {
    func goBack()
}

public class AchievementsLayout: BaseLayout {
    
    public var achievementsLayoutDelegate: AchievementsLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: AchievementsLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.achievementsLayoutDelegate = delegate
    }
    
    lazy var achievementsTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(AchievementsCell.self, forCellReuseIdentifier: AchievementsCell.identifier)
        return tableView
    }()
    
    lazy var noDataLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.text = "noData".localized()
        label.backgroundColor = .white
        label.textAlignment = .center
        label.isHidden = true
        label.font = AppFont.font(type: .Bold, size: 18)
        return label
    }()
    
    public func setupViews() {
        let views = [topView, achievementsTableView, noDataLabel]
        
        superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.achievementsTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.leading.trailing.bottom.equalTo(superview)
        }
        
        self.noDataLabel.snp.remakeConstraints { (maker) in
            maker.leading.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            
            maker.trailing.equalTo(superview).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            
            maker.center.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 5))
        }
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "acheivements".localized())
        self.topView.backImageView.image = UIImage(named: "back_icon_arabic")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.leftImageView.isHidden = true
        self.topView.delegate = self
    }
}

extension AchievementsLayout: TopViewDelegate {
    public func goBack() {
        self.achievementsLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}
