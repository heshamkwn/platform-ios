//
//  AchievementsCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 12/19/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import SwiftyUserDefaults

protocol AchievementsCellDelegate: BaseLayoutDelegate {
    func share(image: UIImage)
}

class AchievementsCell: UITableViewCell {

    public static let identifier = "AchievementsCell"
    var superView: UIView!
    var delegate: AchievementsCellDelegate!
    var achievement: Achievement!
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 8
        view.addShadow(offset: CGSize(width: -1, height: 1), radius: 3.0, color: .black, opacity: 0.3)
        return view
    }()
    
    lazy var certificateImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.layer.cornerRadius = 8
        return imageView
    }()

    lazy var projectNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 16)
        return label
    }()
    
    lazy var shareImageview: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "ic_share")
        imageView.isUserInteractionEnabled = true
        imageView.addTapGesture(action: { (recognizer) in
            if let _ = self.achievement.certificate {
                self.delegate.share(image: self.certificateImageView.image!)
            }
        })
        return imageView
    }()
    
    lazy var pointsImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "points")
        return imageView
    }()
    
    lazy var pointsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 13)
        return label
    }()
    
    lazy var gpaImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "prize")
        return imageView
    }()
    
    lazy var gpaLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.AppColors.primaryColor
        label.textAlignment = .right
        label.font = AppFont.font(type: .Bold, size: 13)
        return label
    }()
    
    public func setupViews() {
        let views = [containerView, certificateImageView, projectNameLabel, shareImageview, pointsLabel, pointsImageView, gpaLabel, gpaImageView]
        
        self.superView = self.contentView
        
        self.superView.addSubviews(views)
        
        self.containerView.addSubviews([certificateImageView, projectNameLabel, shareImageview, pointsLabel, pointsImageView, gpaLabel, gpaImageView])
        
        containerView.snp.makeConstraints { (maker) in
            maker.leading.top.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.trailing.bottom.equalTo(superView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1) * -1)
            
        }
        
        certificateImageView.snp.makeConstraints { (maker) in
            maker.leading.trailing.top.equalTo(containerView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 40))
        }
        
        shareImageview.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(certificateImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 5))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        projectNameLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.trailing.equalTo(shareImageview.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(certificateImageView.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 3))
        }
        
        pointsImageView.snp.makeConstraints { (maker) in
            maker.leading.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
            maker.top.equalTo(projectNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
        pointsLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(pointsImageView.snp.trailing).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1))
             maker.top.equalTo(projectNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 40))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
        }
        
        gpaLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(containerView).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.width.equalTo(achievement.gpa.widthOfString(usingFont: gpaLabel.font) + "gpa".localized().widthOfString(usingFont: gpaLabel.font))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.top.equalTo(projectNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
        }
        
        gpaImageView.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(gpaLabel.snp.leading).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 1) * -1)
            maker.top.equalTo(projectNameLabel.snp.bottom).offset(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 1))
            maker.width.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 2))
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
    }
    
    public func populateData() {
        projectNameLabel.text = achievement.projectTitle
        if let _ = achievement.certificate {
            do {
                let request = try URLRequest(url: URL(string: "\(CommonConstants.BASE_URL)certificate/download/\(achievement.certificate.uuid!)/\(achievement.certificate.v1.img.key!)")!, method: .get, headers: ["X-AUTH-TOKEN" : Defaults[.token]!, "CERTIFICATE-VERSION" : "v1"])
                certificateImageView.af_setImage(withURLRequest: request)
            } catch {
                
            }
        }
        
        pointsLabel.text = "\(achievement.points!) " + "points".localized()
        
        gpaLabel.text = "gpa".localized() + " \(achievement.gpa!)"
    }
}
