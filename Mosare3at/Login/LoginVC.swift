//
//  LoginVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 9/30/18.
//  Copyright © 2018 Hesham Donia. All rights reserved.
//

import UIKit
import Toast_Swift
import SwiftyUserDefaults
import Alamofire

class LoginVC: BaseVC {

    var loginLayout: LoginLayout!
    var presenter: LoginPresenter!
    var user: User!
    // to return object of loginVC
    static func buildVC() -> LoginVC {
        return LoginVC()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginLayout = LoginLayout(superview: self.view, loginDelegate: self)
        loginLayout.setupViews()
        
        presenter = Injector.provideLoginPresenter()
        presenter.setView(view: self)
    }

}

extension LoginVC: LoginDelegate {
    func goToRegisterScreen() {
        print("goToRegisterScreen")
        self.navigator.navigateToRegisterationVC()
    }
    
    func login() {
        if (self.loginLayout.emailField.text?.isEmpty)! || (self.loginLayout.passwordField.text?.isEmpty)!{
            self.view.makeToast("emptyFieldsError".localized())
        } else if !(self.loginLayout.emailField.text?.isEmail)! {
            self.view.makeToast("validEmail".localized())
        } else {
           self.presenter.login(email: self.loginLayout.emailField.text!, password: self.loginLayout.passwordField.text!)
        }
    }
    
    func goToResetPasswordScreen() {
        navigator.navigateToForgetPassword()
    }
    
    func goToTutorial() {
        navigator.navigateToTutorial()
    }
    
    func goToProgramScreen() {
        navigator.navigateToMainScreen()
    }
    
    func goToWaitingScreen() {
        navigator.naviagateToWaitingScreen()
    }
    
    func retry() {
        
    }
    
    
}

extension LoginVC : LoginView {
    func loginSuccess(user: User) {
        
        if !user.roles.contains(CommonConstants.USER_ROLE) {
            self.view.makeToast("dontHavePermission".localized())
        } else {
            Defaults[.user] = user.convertToDictionary()
            Defaults[.token] = user.token
            Defaults[.isLoggedIn] = true
            self.user = user
            getSubscriptions(userId: user.id, token: user.token)
           
        }
    }
    
    func loginFailed(errorMessage: String) {
        self.view.makeToast(errorMessage)
    }
}

extension LoginVC {
    public func getSubscriptions(userId: Int, token: String) {
        let headers = ["X-AUTH-TOKEN" : token]
        let parameters = ["user" : "/users/\(userId)"]
        UiHelpers.showLoader()
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "subscriptions")!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        let subscriptionResponse = SubscribtionsResponse.getInstance(dictionary: json)
                        let subscription = subscriptionResponse.hydraMember.get(at: 0)!
                        Defaults[.subscriptionId] = subscription.id
                        Defaults[.subscription] = subscription.convertToDictionary()
                        Defaults[.currentWeek] = subscription.week.convertToDictionary()
                        let programID = Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").get(at: Subscribtion.getInstance(dictionary: Defaults[.subscription]!).program.requestId.components(separatedBy: "/").count - 1)
                        self.getProgramDetails(programId: Int(programID!)!)
                    }
                }
            }
        }
    }
    
    public func getProgramDetails(programId: Int) {
        let headers = ["X-AUTH-TOKEN" : Defaults[.token]!]
        
        Alamofire.request(URL(string: CommonConstants.BASE_URL + "programs/\(programId)/")!, method: .get, parameters: nil, encoding: URLEncoding.default, headers: headers).responseJSON { (response) in
            UiHelpers.hideLoader()
            if response.result.isSuccess {
                if let json = response.result.value as? Dictionary<String,AnyObject> {
                    if response.response?.statusCode == 200 ||  response.response?.statusCode == 201 || response.response?.statusCode == 204 {
                        UiHelpers.hideLoader()
                        let programDetails = ProgramDetails.getInstance(dictionary: json)
                        Defaults[.currentProgramStartDate] = programDetails.startDate
                        let finalCurrentProgramStartDate = Defaults[.currentProgramStartDate]?.components(separatedBy: "+").get(at: 0)
                        if AppDelegate.isCurrentProgramStarted(programStartDate: finalCurrentProgramStartDate!) {
                            self.view.makeToast("loginSuccess".localized())
                            if self.user.takeTutorial {
                                self.goToProgramScreen()
                            } else {
                                self.goToTutorial()
                            }
                        } else {
                            self.goToWaitingScreen()
                        }
                    }
                }
            }
        }
    }
}
