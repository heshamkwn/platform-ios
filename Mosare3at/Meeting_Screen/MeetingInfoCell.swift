//
//  MeetingInfoCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/21/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift

class MeetingInfoCell: UITableViewCell {

    static let identifier = "MeetingInfoCell"
    var superView: UIView!
    
    var title: String!

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        if Localize.currentLanguage() == "en" {
            label.textAlignment = .left
        } else {
            label.textAlignment = .right
        }
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    public func setupViews() {
        superView = self.contentView
        
        superView.addSubviews([titleLabel])
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.edges.equalTo(superView)
        }
    }
    
    public func populateData() {
        titleLabel.text = title
    }
    
}
