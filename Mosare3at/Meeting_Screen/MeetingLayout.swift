//
//  MeetingLayout.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/21/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation
import UIKit

public protocol MeetingLayoutDelegate: BaseLayoutDelegate {
    func goBack()
}

public class MeetingLayout: BaseLayout {
    
    var meetingLayoutDelegate: MeetingLayoutDelegate!
    
    var topView: TopView = TopView()
    
    init(superview: UIView, delegate: MeetingLayoutDelegate) {
        super.init(superview: superview, delegate: delegate)
        self.meetingLayoutDelegate = delegate
    }
    
    lazy var meetingTableView: UITableView = {
        let tableView = UITableView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.white
        tableView.isScrollEnabled = true
        tableView.register(MeetingCell.self, forCellReuseIdentifier: MeetingCell.identifier)
        return tableView
    }()
    
    public func setupViews() {
        let views = [topView, meetingTableView]
        
        self.superview.addSubviews(views)
        
        self.topView.snp.makeConstraints { maker in
            maker.leading.equalTo(superview.snp.leading)
            maker.trailing.equalTo(superview.snp.trailing)
            maker.top.equalTo(superview)
            
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 9))
        }
        
        setupTopView()
        
        self.meetingTableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(topView.snp.bottom)
            maker.leading.trailing.bottom.equalTo(superview)
        }
        
    }
    
    func setupTopView() {
        self.topView.setupViews(screenTitle: "")
        self.topView.backImageView.image = UIImage(named: "close")
        self.topView.screenTitleLabel.isHidden = false
        self.topView.delegate = self
    }
    
}

extension MeetingLayout: TopViewDelegate {
    public func goBack() {
        self.meetingLayoutDelegate.goBack()
    }
    
    public func goToNotifications() {
        
    }
}


