//
//  MeetingStandard.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/21/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class MeetingStandard {
    var id: Int!
    var title: String!
    
    public init(id: Int, title: String) {
        self.id = id
        self.title = title
    }
}


/*
 StandardModel standardModel1 = new StandardModel(1, "علي الفريق تسجيل الاجتماع من بدايتة وحتي نهايته");
 StandardModel standardModel2 = new StandardModel(2, "علي جميع أعضاء الفريق الحضور قبل وقت الاجتماع بخمس دقائق لحل أى مشكلة تقنية كي لا يأخذ حلها من وقت الاجتماع");
 StandardModel standardModel3 = new StandardModel(3, "علي جميع أعضاء الفريق الاطلاع علي جدول الاجتماع الأول لمعرفة المواضيع المطلوب تغطيتها");
 StandardModel standardModel4 = new StandardModel(4, "علي جميع أعضاء الفريق وضع القوانين الخاصة بهم فيما يخص اجتماعاتهم الأسبوعيه");
 StandardModel standardModel5 = new StandardModel(5, "تحميل برامج اخراج وتعديل الفيديوهات واجراء التجارب عليها");

 
 */
