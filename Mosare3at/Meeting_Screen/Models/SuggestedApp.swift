//
//  SuggestedApp.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/21/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import Foundation

public class SuggestedApp {
    var id: Int!
    var title: String!
    var image: String!
    
    public init(id: Int, title: String, image: String?) {
        self.id = id
        self.title = title
        if let _ = image {
            self.image = image!
        }
    }
}

/*
 
 suggestedAppsAdapter.addItem(new SuggestedAppsModel(1, "zoom", ""));
 suggestedAppsAdapter.addItem(new SuggestedAppsModel(2, "slack", ""));
 suggestedAppsAdapter.addItem(new SuggestedAppsModel(4, "Rown.com", ""));
 suggestedAppsAdapter.addItem(new SuggestedAppsModel(5, "Mrefernce.com", ""));
 */
