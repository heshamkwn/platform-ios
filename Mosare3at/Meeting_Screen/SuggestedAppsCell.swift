//
//  SuggestedAppsCell.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/21/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import Localize_Swift

class SuggestedAppsCell: UICollectionViewCell {

    static let identifier = "SuggestedAppsCell"
    var superView: UIView!

    var suggestedApp: SuggestedApp!
    
    lazy var appImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.image = UIImage(named: "zoom_ic")
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.backgroundColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.font = AppFont.font(type: .Regular, size: 14)
        return label
    }()
    
    public func setupViews() {
        let views = [appImageView, titleLabel]
        
        superView = self.contentView
        
        superView.addSubviews(views)
        
        appImageView.snp.makeConstraints { (maker) in
            maker.centerX.top.equalTo(superView)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 6))
            maker.width.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_WIDTH, relativeView: nil, percentage: 10))
        }
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.leading.trailing.equalTo(superView)
            maker.top.equalTo(appImageView.snp.bottom)
            maker.height.equalTo(UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 4))
        }
        
    }
    
    public func populateData() {
        titleLabel.text = suggestedApp.title
    }
}
