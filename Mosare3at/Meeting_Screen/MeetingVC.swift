//
//  MeetingVC.swift
//  Mosare3at
//
//  Created by Hesham Donia on 2/21/19.
//  Copyright © 2019 Hesham Donia. All rights reserved.
//

import UIKit
import Alamofire
import JGProgressHUD
class MeetingVC: BaseVC {

    
    var layout: MeetingLayout!
    var suggestedApps = [SuggestedApp]()
    var meetingStandard = [MeetingStandard]()
    var descriptions: [Description]!
    
    public class func buildVC(describtion: [Description]) -> MeetingVC {
        let vc = MeetingVC()
        vc.descriptions = describtion
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareMeetingStandard()
        
        prepareSuggestedApps()
        
        layout = MeetingLayout(superview: self.view, delegate: self)
        layout.setupViews()
        
        layout.meetingTableView.dataSource = self
        layout.meetingTableView.delegate = self
        layout.meetingTableView.reloadData()
    }
    
    func prepareMeetingStandard() {
        meetingStandard.append(MeetingStandard(id: 1, title: "علي الفريق تسجيل الاجتماع من بدايتة وحتي نهايته"))
        meetingStandard.append(MeetingStandard(id: 2, title: "علي جميع أعضاء الفريق الحضور قبل وقت الاجتماع بخمس دقائق لحل أى مشكلة تقنية كي لا يأخذ حلها من وقت الاجتماع"))
        meetingStandard.append(MeetingStandard(id: 3, title: "علي جميع أعضاء الفريق الاطلاع علي جدول الاجتماع الأول لمعرفة المواضيع المطلوب تغطيتها"))
        meetingStandard.append(MeetingStandard(id: 4, title: "علي جميع أعضاء الفريق وضع القوانين الخاصة بهم فيما يخص اجتماعاتهم الأسبوعيه"))
        meetingStandard.append(MeetingStandard(id: 5, title: "تحميل برامج اخراج وتعديل الفيديوهات واجراء التجارب عليها"))
    }
    
    func prepareSuggestedApps() {
        suggestedApps.append(SuggestedApp(id: 1, title: "zoom", image: nil))
        suggestedApps.append(SuggestedApp(id: 2, title: "slack", image: nil))
        suggestedApps.append(SuggestedApp(id: 4, title: "Rown.com", image: nil))
        suggestedApps.append(SuggestedApp(id: 5, title: "Mrefernce.com", image: nil))
    }

}

extension MeetingVC: MeetingLayoutDelegate {
    func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func retry() {
        
    }
}

extension MeetingVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MeetingCell.identifier, for: indexPath) as! MeetingCell
        cell.delegate = self
        cell.selectionStyle = .none
        cell.meetingStandards = self.meetingStandard
        cell.suggestedApps = self.suggestedApps
        cell.descriptions = self.descriptions
        cell.setupViews()
        cell.populateData()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UiHelpers.getLengthAccordingTo(relation: .SCREEN_HEIGHT, relativeView: nil, percentage: 110)
    }
}

extension MeetingVC: MeetingCellDelegate {
    func openMeetingTable(describtion: [Description]) {
        self.navigator.navigateToMeetingTableVC(describtion: describtion)
    }
    
    func downloadMeetingDraft() {
        let fileURL = URL(string: "http://accelerate.om:8080/media/download/meeting_minutes.docx")

        let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
        let hud = JGProgressHUD(style: .dark)
        hud.textLabel.text = "downloading".localized()
        hud.show(in: self.view)
        
        Alamofire.download(
            fileURL!,
            method: .get,
            parameters: nil,
            encoding: JSONEncoding.default,
            headers: nil,
            to: destination).downloadProgress(closure: { (progress) in
                print("\(progress)")
            }).response(completionHandler: { (DefaultDownloadResponse) in
                hud.indicatorView = JGProgressHUDSuccessIndicatorView()
                hud.textLabel.text = "downloadSuccess".localized()
                hud.dismiss(afterDelay: 2.0)
            })

    }
}
